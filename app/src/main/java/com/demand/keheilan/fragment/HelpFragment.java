package com.demand.keheilan.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.demand.keheilan.R;
import com.demand.keheilan.activity.GeneralInquiry;

import static com.demand.keheilan.util.MyAppConstants.GENERAL_INQUIRY;
import static com.demand.keheilan.util.MyAppConstants.GENERAL_INQUIRY_TAB;
import static com.demand.keheilan.util.MyAppConstants.HELP_TAB;
import static com.demand.keheilan.util.MyAppConstants.HELP_WITH_ORDER;
import static com.demand.keheilan.util.MyAppConstants.HELP_WITH_ORDER_TAB;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelpFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelpFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "HelpFragment";
    private TextView tv_heading;
    private ImageView iv_general_inquiry, iv_help_order;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HelpFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HelpFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelpFragment newInstance(String param1, String param2) {
        HelpFragment fragment = new HelpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        initView(view);
        tv_heading.setText(getString(R.string.help));
        return view;
    }

    private void initView(View view) {
        tv_heading = view.findViewById(R.id.tv_heading);
        iv_general_inquiry = view.findViewById(R.id.iv_general_inquiry);
        iv_help_order = view.findViewById(R.id.iv_help_order);

        iv_general_inquiry.setOnClickListener(this);
        iv_help_order.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_general_inquiry) {
            openCommonScreen(GENERAL_INQUIRY_TAB);
        } else if (id == R.id.iv_help_order) {
            openCommonScreen(HELP_WITH_ORDER_TAB);
        }
    }

    private void openCommonScreen(String tabClicked) {
        Intent intent = new Intent(getActivity(), GeneralInquiry.class);
        intent.putExtra(HELP_TAB, tabClicked);
        startActivity(intent);
    }
}