package com.demand.keheilan.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.demand.keheilan.BuildConfig;
import com.demand.keheilan.R;
import com.demand.keheilan.activity.BookServiceActivity;
import com.demand.keheilan.activity.ChooseLanguageActivity;
import com.demand.keheilan.activity.HomeActivity;
import com.demand.keheilan.activity.LoginPhoneActivity;
import com.demand.keheilan.activity.ProviderRegistrationActivity;
import com.demand.keheilan.activity.ShareActivity;
import com.demand.keheilan.activity.WebViewActivity;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.ProfilePicResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.FilePath;
import com.demand.keheilan.util.InternetCheck;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.demand.keheilan.util.MyAppConstants.ABOUT_US_TAB;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.CLICKED_ON_TAB;
import static com.demand.keheilan.util.MyAppConstants.MORE_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.PRIVACY_POLICY_TAB;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TERMS_AND_CONDITIONS_TAB;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MoreFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "MoreFragment";
    private Button btn_login;
    private TextView tv_changeLang, tv_logout, tv_about, tv_privacy, tv_terms, tv_register, tv_userName, tv_userPhone, tv_shareApp;
    private ConstraintLayout const_profile, const_fragment_header;
    private CardView card_pic;
    private View view_bg_top;
    private ImageView iv_add_pic, iv_user_pic;
    private String path;
    private static final int STORAGE_REQUEST_CODE = 15;
    private static final int PHOTO_REQ = 5;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MoreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MoreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MoreFragment newInstance(String param1, String param2) {
        MoreFragment fragment = new MoreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called...");
        if (SharedPreferenceWriter.getInstance(getActivity()).getBoolean(SPreferenceKey.IS_GUEST_USER)) {
            const_fragment_header.setVisibility(View.VISIBLE);
            view_bg_top.setVisibility(View.GONE);
            const_profile.setVisibility(View.GONE);
            card_pic.setVisibility(View.GONE);
            iv_add_pic.setVisibility(View.GONE);
        } else {
            const_fragment_header.setVisibility(View.GONE);
            view_bg_top.setVisibility(View.VISIBLE);
            const_profile.setVisibility(View.VISIBLE);
            card_pic.setVisibility(View.VISIBLE);
            iv_add_pic.setVisibility(View.VISIBLE);

            setUserData();
            serviceUserPic();
        }
    }

    private void setUserData() {
        String fullName = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.FULL_NAME);
        String phoneNumber = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.PHONE_NUMBER);
        String countryCode = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.COUNTRY_CODE);
        tv_userName.setText(fullName);
        tv_userPhone.setText("+" + countryCode + phoneNumber);

    }

    private void initView(View view) {
        btn_login = view.findViewById(R.id.btn_login);
        tv_changeLang = view.findViewById(R.id.tv_changeLang);
        tv_logout = view.findViewById(R.id.tv_logout);
        tv_about = view.findViewById(R.id.tv_about);
        tv_privacy = view.findViewById(R.id.tv_privacy);
        tv_terms = view.findViewById(R.id.tv_terms);
        tv_register = view.findViewById(R.id.tv_register);
        const_profile = view.findViewById(R.id.const_profile);
        tv_userName = view.findViewById(R.id.tv_userName);
        tv_userPhone = view.findViewById(R.id.tv_userPhone);
        card_pic = view.findViewById(R.id.card_pic);
        const_fragment_header = view.findViewById(R.id.const_fragment_header);
        view_bg_top = view.findViewById(R.id.view_bg_top);
        iv_add_pic = view.findViewById(R.id.iv_add_pic);
        iv_user_pic = view.findViewById(R.id.iv_user_pic);
        tv_shareApp = view.findViewById(R.id.tv_shareApp);

        tv_changeLang.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        tv_about.setOnClickListener(this);
        tv_privacy.setOnClickListener(this);
        tv_terms.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        iv_add_pic.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        tv_shareApp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_changeLang) {
            Intent intent = new Intent(getActivity(), ChooseLanguageActivity.class);
            intent.putExtra(CAME_FROM_SCREEN, MORE_SCREEN);
            startActivity(intent);
        } else if (id == R.id.tv_logout) {
            showLogoutDialog();
        } else if (id == R.id.tv_about) {
            openWebViewActivity(ABOUT_US_TAB);
        } else if (id == R.id.tv_privacy) {
            openWebViewActivity(PRIVACY_POLICY_TAB);
        } else if (id == R.id.tv_terms) {
            openWebViewActivity(TERMS_AND_CONDITIONS_TAB);
        } else if (id == R.id.tv_register) {
            if (CommonUtil.isUserLoggedIn(getActivity()))
                startActivity(new Intent(getActivity(), ProviderRegistrationActivity.class));
            else
                CommonUtil.showLoginAlert(getActivity(), (CommonUtil.LogoutUser) getActivity());
        } else if (id == R.id.iv_add_pic) {
            if (checkPermissions(CommonUtil.getPermissionList())) {
                takePhotoIntent();
            }
        } else if (id == R.id.btn_login) {
            logoutUser();
        } else if (id == R.id.tv_shareApp) {
            startActivity(new Intent(getActivity(), ShareActivity.class));
        }
    }

    private boolean checkPermissions(List<String> permissions) {
        List<String> permissionsNotGranted = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), permission) != PackageManager.PERMISSION_GRANTED)
                permissionsNotGranted.add(permission);
        }
        if (!permissionsNotGranted.isEmpty()) {
            requestPermissions(permissionsNotGranted.toArray(new String[permissionsNotGranted.size()]), STORAGE_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_REQUEST_CODE: {
                Log.d(TAG, "onRequestPermissionsResult: called.....");
                boolean anyPermissionDenied = false;
                boolean neverAskAgainSelected = false;
                // Check if any permission asked has been denied
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        anyPermissionDenied = true;
                        //check if user select "never ask again" when denying any permission
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[i])) {
                            neverAskAgainSelected = true;
                        }
                    }
                }
                if (!anyPermissionDenied) {
                    Log.d(TAG, "onRequestPermissionsResult: Every permissions granted...");
                    // All Permissions asked were granted! Yey!
                    // DO YOUR STUFF

                    takePhotoIntent();
                } else {
                    // the user has just denied one or all of the permissions
                    // use this message to explain why he needs to grant these permissions in order to proceed
                    if (neverAskAgainSelected) {
                        Log.d(TAG, "onRequestPermissionsResult: never ask again selected...");
                        //This message is displayed after the user has checked never ask again checkbox.
                        shouldShowSettings(iv_add_pic);

                    } else {
                        Log.d(TAG, "onRequestPermissionsResult: denied selected......");
                        //This message is displayed while the user hasn't checked never ask again checkbox.
                        CommonUtil.showSnackBar(getActivity(), "Give Permissions to Upload Image");
                    }
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void shouldShowSettings(View v) {
        Snackbar.make(v, getString(R.string.grant_permission), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.grant_access), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, " snackbar onClick: starts");
                        //user has permanenty denied the permissions so take them to the settings
                        Log.d(TAG, "onClick: user has permanently denied the permission ...");
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        Log.d(TAG, " snackbar onClick: uri is = " + uri.toString());
                        intent.setData(uri);
                        getActivity().startActivity(intent);
                        Log.d(TAG, " snackbar onClick: ends");
                    }
                }).show();
    }

    private void takePhotoIntent() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File imageFile = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir);
            path = imageFile.getAbsolutePath();
            Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getContext().getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", imageFile);
            Intent intent = new CommonUtil().getPickIntent(getContext().getApplicationContext(), imageFileUri);
            startActivityForResult(intent, PHOTO_REQ);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
//        File file = new File(capture_dir);
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        path = capture_dir + System.currentTimeMillis() + ".jpg";
//        Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getActivity().getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", new File(path));
//        Intent intent = new CommonUtil().getPickIntent(getActivity(), imageFileUri);
//        startActivityForResult(intent, PHOTO_REQ);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PHOTO_REQ:
                    if (data != null) {
                        if (data.getData() != null)
                            path = FilePath.getPath(getActivity(), Uri.parse(data.getDataString()));
                    }
                    compressAndSetImage(path);
                    break;
            }
        }
    }

    private void compressAndSetImage(String imagePath) {
        Log.d(TAG, "compressAndSetImage: GIVEN_PATH==>" + imagePath);
//        Bitmap bm = BitmapFactory.decodeFile(imagePath);
//        Bitmap myBitmap = CommonUtil.setImageOrientation(bm, imagePath);
//        String finalpath = compressImage(myBitmap, myBitmap.getWidth(), myBitmap.getHeight());
//        Log.d(TAG, "compressAndSetImage: FINAL_IMAGE_PATH==>" + finalpath);
        serviceUpdateProfilePic(imagePath);
    }

    private String compressImage(Bitmap finalBitmap, int width, int height) {
        Log.d(TAG, "compressImage: starts.....");
        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
        File myDir = new File(capture_dir);
        myDir.mkdirs();
        File file = new File(myDir, System.currentTimeMillis() + ".jpg");
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);//	compressing to 50%
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    private void serviceUpdateProfilePic(String imagePath) {
        {
            if (new InternetCheck(getActivity()).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.TOKEN);
                DialogPopup dialog = new DialogPopup(getActivity());
                dialog.showLoadingDialog(getActivity(), "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = null;
                try {
                    call = api_service.updateProfilePic(TOKEN_KEY + token, getProfilePic(imagePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialog.dismissLoadingDialog();
                            Toast.makeText(getActivity(), getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(getActivity(), (CommonUtil.LogoutUser) getActivity());
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialog.dismissLoadingDialog();
                                    SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.PROFILE_PIC, imagePath);
                                    Glide.with(getActivity()).load(imagePath).
                                            placeholder(R.drawable.profile_placeholder).
                                            error(R.drawable.profile_placeholder).
                                            into(iv_user_pic);

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialog.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + response.message());
                                    // CommonUtils.showSnackBar(LoginPhoneActivity.this, "" + server_response.getMessage());
                                } else {
                                    dialog.dismissLoadingDialog();
                                    //Toast.makeText(LoginPhoneActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(getActivity(), getString(R.string.internet_lost));
            }
        }
    }

    private MultipartBody.Part getProfilePic(String currentImagePath) throws IOException {
        MultipartBody.Part part = null;
        if ((currentImagePath != null) && (!currentImagePath.contains("http")) && (currentImagePath.length() > 0)) {
            File file = new File(currentImagePath);
            RequestBody body = RequestBody.create(MediaType.parse("" + "*/*"), file);
            part = MultipartBody.Part.createFormData("profile_pic", file.getName(), body);
        }
        return part;
    }


    private void openWebViewActivity(String tabClicked) {
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        intent.putExtra(CLICKED_ON_TAB, tabClicked);
        startActivity(intent);
    }

    private void logoutUser() {
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.USER_ID, "");
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.TOKEN, "");
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.FULL_NAME, "");
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.PHONE_NUMBER, "");
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.COUNTRY_CODE, "");
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.PROFILE_PIC, "");
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.USER_HOME_ADDRESS, "");
        SharedPreferenceWriter.getInstance(getActivity()).writeBooleanValue(SPreferenceKey.IS_USER_LOGIN, false);

        Intent intent = new Intent(getActivity(), LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
    }

    private void showLogoutDialog() {
        Dialog logoutDialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        logoutDialog.setContentView(R.layout.layout_logout);
        logoutDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        logoutDialog.setCanceledOnTouchOutside(true);

        ImageView iv_close = logoutDialog.findViewById(R.id.iv_close);
        Button btn_cancel = logoutDialog.findViewById(R.id.btn_cancel);
        TextView tv_logout = logoutDialog.findViewById(R.id.tv_logout);

        View.OnClickListener dialogClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                if (id == R.id.tv_logout) {
                    logoutDialog.dismiss();
                    logoutUser();
                } else if (id == R.id.iv_close) {
                    logoutDialog.dismiss();
                } else if (id == R.id.btn_cancel) {
                    logoutDialog.dismiss();
                }
            }
        };

        iv_close.setOnClickListener(dialogClickListener);
        btn_cancel.setOnClickListener(dialogClickListener);
        tv_logout.setOnClickListener(dialogClickListener);

        logoutDialog.show();
    }

    private void serviceUserPic() {
        {
            if (new InternetCheck(getActivity()).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.TOKEN);
                DialogPopup dialog = new DialogPopup(getActivity());
                dialog.showLoadingDialog(getActivity(), "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ProfilePicResponse> call = api_service.getProfilePic(TOKEN_KEY + token);
                call.enqueue(new Callback<ProfilePicResponse>() {
                    @Override
                    public void onResponse(Call<ProfilePicResponse> call, Response<ProfilePicResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialog.dismissLoadingDialog();
                            Toast.makeText(getActivity(), getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(getActivity(), (CommonUtil.LogoutUser) getActivity());
                        } else {
                            if (response.isSuccessful()) {
                                ProfilePicResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialog.dismissLoadingDialog();
                                    if (server_response.getProfile_pic() != null && server_response.getProfile_pic().length() > 0) {
                                        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.PROFILE_PIC, server_response.getProfile_pic());
                                        Glide.with(getActivity()).load(server_response.getProfile_pic()).
                                                placeholder(R.drawable.profile_placeholder).
                                                error(R.drawable.profile_placeholder).
                                                into(iv_user_pic);
                                    }

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialog.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + response.message());
                                } else {
                                    dialog.dismissLoadingDialog();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfilePicResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(getActivity(), getString(R.string.internet_lost));
            }
        }
    }
}