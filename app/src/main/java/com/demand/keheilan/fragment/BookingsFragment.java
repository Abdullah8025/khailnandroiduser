package com.demand.keheilan.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.activity.BookServiceActivity;
import com.demand.keheilan.activity.HomeActivity;
import com.demand.keheilan.activity.OtpVerificationActivity;
import com.demand.keheilan.adapter.InProcessServiceAdapter;
import com.demand.keheilan.adapter.PastServiceAdapter;
import com.demand.keheilan.adapter.UpcomingServiceAdapter;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.BookedServices;
import com.demand.keheilan.responsemodel.BookedServicesResponse;
import com.demand.keheilan.responsemodel.LoginResponse;
import com.demand.keheilan.responsemodel.PastService;
import com.demand.keheilan.responsemodel.PastServicesResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.DEVICE_TYPE;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ARABIC;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ENGLISH;
import static com.demand.keheilan.util.MyAppConstants.OPEN_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.PAST_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;
import static com.demand.keheilan.util.MyAppConstants.UPCOMING_SERVICES;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BookingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingsFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "BookingsFragment";
    private TextView tv_heading, tv_noData;
    private RecyclerView rv_order;
    private Button btn_in_process, btn_upcoming, btn_past;

    private List<BookedServices> mUpcomingServicesList;
    private List<BookedServices> mInProcessServicesList;
    private List<PastService> mPastServiceList;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public BookingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookingsFragment newInstance(String param1, String param2) {
        BookingsFragment fragment = new BookingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bookings, container, false);
        initView(view);
        tv_heading.setText(getString(R.string.my_bookings));
        initializeList();
        initializeAdapter();

        Bundle extraData = getArguments();
        if (extraData != null) {
            Log.d(TAG, "onCreateView: EXTRA DATA FOUND = " + extraData.getString(OPEN_SERVICES));
            if (extraData.getString(OPEN_SERVICES).equals(UPCOMING_SERVICES)) {
                refreshUpComingServices();
            } else if (extraData.getString(OPEN_SERVICES).equals(IN_PROCESS_SERVICES)) {
                refreshProcessServices();
            } else if (extraData.getString(OPEN_SERVICES).equals(PAST_SERVICES)) {
                refreshPastList();
            }else {
                refreshProcessServices();
            }

        } else {
            Log.d(TAG, "onCreateView: EXTRA DATA NULL..");
            refreshProcessServices();
        }

        return view;
    }

    private void initializeList() {
        mUpcomingServicesList = new ArrayList<>();
        mInProcessServicesList = new ArrayList<>();
        mPastServiceList = new ArrayList<>();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        Log.d(TAG, "onAttach: called..");
        super.onAttach(context);
        Activity activity = getActivity();
        if (!(activity instanceof PastServiceAdapter.OnPastServiceClicked)) {
            throw new ClassCastException(activity.getClass().getSimpleName() +
                    "must implement PastServiceAdapter.OnPastServiceClicked inteface");
        }
    }

    private void initializeAdapter() {
        rv_order.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initView(View view) {
        tv_heading = view.findViewById(R.id.tv_heading);
        rv_order = view.findViewById(R.id.rv_order);
        btn_in_process = view.findViewById(R.id.btn_in_process);
        btn_upcoming = view.findViewById(R.id.btn_upcoming);
        btn_past = view.findViewById(R.id.btn_past);
        tv_noData = view.findViewById(R.id.tv_noData);

        btn_in_process.setOnClickListener(this);
        btn_upcoming.setOnClickListener(this);
        btn_past.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_in_process) {
            setButtonBackground(true, false, false);
            serviceBookings(IN_PROCESS_SERVICES);
        } else if (id == R.id.btn_upcoming) {
            setButtonBackground(false, true, false);
            serviceBookings(UPCOMING_SERVICES);
        } else if (id == R.id.btn_past) {
            setButtonBackground(false, false, true);
            servicePastServices();
        }
    }

    private void setPastAdapter() {
        rv_order.setAdapter(new PastServiceAdapter(getActivity(), mPastServiceList, (PastServiceAdapter.OnPastServiceClicked) getActivity()));
    }

    private void setUpcomingAdapter() {
        rv_order.setAdapter(new UpcomingServiceAdapter(getActivity(), mUpcomingServicesList, (UpcomingServiceAdapter.OnBookingClicked) getActivity()));
    }

    private void setInProcessAdapter() {
        rv_order.setAdapter(new InProcessServiceAdapter(getActivity(), mInProcessServicesList, (UpcomingServiceAdapter.OnBookingClicked) getActivity()));
    }

    private void setButtonBackground(boolean isInProcessSelected, boolean isUpcomingSelected, boolean isPastSelected) {
        String langCode = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.LANG_CODE);
        if (langCode.equals(LANGUAGE_CODE_ENGLISH)) {
            if (isInProcessSelected) {
                btn_in_process.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.left_corner_blue));
                btn_in_process.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            } else {
                btn_in_process.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.left_corner));
                btn_in_process.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
            }

            if (isPastSelected) {
                btn_past.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.right_corner_blue));
                btn_past.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            } else {
                btn_past.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.right_corner));
                btn_past.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
            }
        }
        if (langCode.equals(LANGUAGE_CODE_ARABIC)) {
            if (isInProcessSelected) {
                btn_in_process.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.right_corner_blue));
                btn_in_process.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            } else {
                btn_in_process.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.right_corner));
                btn_in_process.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
            }

            if (isPastSelected) {
                btn_past.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.left_corner_blue));
                btn_past.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            } else {
                btn_past.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.left_corner));
                btn_past.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
            }
        }


        if (isUpcomingSelected) {
            btn_upcoming.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.bg_blue));
            btn_upcoming.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        } else {
            btn_upcoming.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.bg_white));
            btn_upcoming.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        }


    }

    private void serviceBookings(String serviceType) {
        {
            if (new InternetCheck(getActivity()).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(getActivity());
                dialogPopup.showLoadingDialog(getActivity(), "");
                String token = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<BookedServicesResponse> call;
                if (serviceType.equals(UPCOMING_SERVICES))
                    call = api_service.upcomingBookings(TOKEN_KEY + token);
                else if (serviceType.equals(IN_PROCESS_SERVICES))
                    call = api_service.inProcessServices(TOKEN_KEY + token);
                else
                    throw new IllegalArgumentException(TAG + "Must be of type upcoming or in process");
                call.enqueue(new Callback<BookedServicesResponse>() {
                    @Override
                    public void onResponse(Call<BookedServicesResponse> call, Response<BookedServicesResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(getActivity(), getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(getActivity(), (CommonUtil.LogoutUser) getActivity());
                        } else {
                            if (response.isSuccessful()) {
                                BookedServicesResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    if (serviceType.equals(UPCOMING_SERVICES)) {
                                        ArrayList<BookedServices> upcomingServices = (ArrayList<BookedServices>) server_response.getData();
                                        mUpcomingServicesList.clear();
                                        if (upcomingServices != null && upcomingServices.size() > 0) {
                                            tv_noData.setVisibility(View.GONE);
                                            mUpcomingServicesList.addAll(upcomingServices);
                                        } else {
                                            tv_noData.setVisibility(View.VISIBLE);
                                        }
                                        setUpcomingAdapter();
                                    }

                                    if (serviceType.equals(IN_PROCESS_SERVICES)) {
                                        ArrayList<BookedServices> inProcessServices = (ArrayList<BookedServices>) server_response.getData();
                                        mInProcessServicesList.clear();
                                        if (inProcessServices != null && inProcessServices.size() > 0) {
                                            tv_noData.setVisibility(View.GONE);
                                            mInProcessServicesList.addAll(inProcessServices);
                                        } else {
                                            tv_noData.setVisibility(View.VISIBLE);
                                        }
                                        setInProcessAdapter();
                                    }


                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<BookedServicesResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(getActivity(), getString(R.string.internet_lost));
            }
        }
    }

    private void servicePastServices() {
        {
            if (new InternetCheck(getActivity()).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(getActivity());
                dialogPopup.showLoadingDialog(getActivity(), "");
                String token = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PastServicesResponse> call = api_service.pastServices(TOKEN_KEY + token);
                call.enqueue(new Callback<PastServicesResponse>() {
                    @Override
                    public void onResponse(Call<PastServicesResponse> call, Response<PastServicesResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(getActivity(), getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(getActivity(), (CommonUtil.LogoutUser) getActivity());
                        } else {
                            if (response.isSuccessful()) {
                                PastServicesResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();

                                    ArrayList<PastService> pastServices = (ArrayList<PastService>) server_response.getData();
                                    mPastServiceList.clear();
                                    if (pastServices != null && pastServices.size() > 0) {
                                        tv_noData.setVisibility(View.GONE);
                                        mPastServiceList.addAll(pastServices);
                                    } else {
                                        tv_noData.setVisibility(View.VISIBLE);
                                    }
                                    setPastAdapter();

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<PastServicesResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(getActivity(), getString(R.string.internet_lost));
            }
        }
    }

    public void refreshProcessServices() {
        setButtonBackground(true, false, false);
        serviceBookings(IN_PROCESS_SERVICES);
    }

    public void refreshUpComingServices() {
        setButtonBackground(false, true, false);
        serviceBookings(UPCOMING_SERVICES);
    }

    public void refreshPastList() {
        setButtonBackground(false, false, true);
        servicePastServices();
    }
}