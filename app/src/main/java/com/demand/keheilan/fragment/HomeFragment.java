package com.demand.keheilan.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.activity.AllCategoriesActivity;
import com.demand.keheilan.activity.HomeActivity;
import com.demand.keheilan.activity.NotificationsActivity;
import com.demand.keheilan.activity.RecentSearchesActivity;
import com.demand.keheilan.adapter.HomeCategoryAdapter;
import com.demand.keheilan.adapter.AllServicesAdapter;
import com.demand.keheilan.responsemodel.CategoriesBean;
import com.demand.keheilan.responsemodel.HomeApiResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.GPSService;
import com.demand.keheilan.util.InternetCheck;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import kotlin.jvm.internal.SpreadBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.ALL_CATEGORY_LIST;
import static com.demand.keheilan.util.MyAppConstants.ALL_SERVICES_LIST;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.HOME_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.MY_LIST;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_LAYOUT;
import static com.demand.keheilan.util.MyAppConstants.SHOW_LIST;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;
import static com.demand.keheilan.util.MyAppConstants.TOP_SERVICES_LIST;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, GPSService.GetLocationUpdate {
    private static final String TAG = "HomeFragment";
    private RecyclerView rv_all_services;
    private TextView edt_search, tv_viewAllCategory, tv_homeAddress, tv_userName;
    private ImageView iv_notif;
    private List<CategoriesBean> mAllCategoryList;
    private List<HomeApiResponse.ServicesBean> mAllServicesList;
    private Geocoder mGeocoder;
    private String mLatitude, mLongitude;
    //private List<HomeApiResponse.TopServicesBean> mTopServicesList;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        if (!(activity instanceof AllServicesAdapter.OnAllServiceClick)) {
            throw new ClassCastException(activity.getClass().getSimpleName() + " must implement TopServicesAdapter.OnTopServiceClick interface or " +
                    "AllServiceAdapter.OnAllServiceClick interface  ");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);
        mAllCategoryList = new ArrayList<>();
        // mTopServicesList = new ArrayList<>();
        mAllServicesList = new ArrayList<>();
        // setUpTopServicesAdapter();
        setUpAllServicesAdapter();
        // setUpAllCategoryAdapter();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called...");
        String userName = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.FULL_NAME);
        if (SharedPreferenceWriter.getInstance(getActivity()).getBoolean(SPreferenceKey.IS_GUEST_USER))
            tv_userName.setText(getString(R.string.guest_name));
        else
            tv_userName.setText(getString(R.string.user_name_home, userName));

        checkHomeAddress();
        serviceHomeList();

    }

    private void checkHomeAddress() {
        String homeAddress = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.USER_HOME_ADDRESS);
        if (CommonUtil.blankValidation(homeAddress)) {
            Log.d(TAG, "checkHomeAddress: HOME ADDRESS FOUND");
            tv_homeAddress.setText(homeAddress);
        } else {
            Log.d(TAG, "checkHomeAddress: HOME ADDRESS NOT FOUND");
            getHomeAddress();
        }

    }

    public void getHomeAddress() {
        mLatitude = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.LATITUDE);
        mLongitude = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.LONGITUDE);
        mGeocoder = new Geocoder(getActivity(), Locale.getDefault());

        if (mLatitude != null && mLongitude != null) {
            GetAddress getAddress = new GetAddress();
            getAddress.execute();
        } else {
            new GPSService(getActivity(), this);
        }
    }

    @Override
    public void getLocationUpdate(Double latitude, Double longitude) {
        Log.d(TAG, "getLocationUpdate: called...");
        mLatitude = String.valueOf(latitude);
        mLongitude = String.valueOf(longitude);
        GetAddress getAddress = new GetAddress();
        getAddress.execute();

    }
//    private void setUpAllCategoryAdapter() {
//        rv_category.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
//        rv_category.setAdapter(new HomeCategoryAdapter(getActivity(), mAllCategoryList, (HomeCategoryAdapter.OnCategoryClick) getActivity()));
//    }

    private void setUpAllServicesAdapter() {
        rv_all_services.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rv_all_services.setAdapter(new AllServicesAdapter(getActivity(), mAllServicesList, (AllServicesAdapter.OnAllServiceClick) getActivity()));

        // setUpAllCategoryAdapter();
    }

//    private void setUpTopServicesAdapter() {
//        rv_top_services.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
//        rv_top_services.setAdapter(new TopServicesAdapter(getActivity(), mTopServicesList, (TopServicesAdapter.OnTopServiceClick) getActivity()));
//
//        setUpAllServicesAdapter();
//
//    }

    private void initView(View view) {
        // rv_top_services = view.findViewById(R.id.rv_top_services);
        rv_all_services = view.findViewById(R.id.rv_all_services);
        // rv_category = view.findViewById(R.id.rv_category);
        edt_search = view.findViewById(R.id.edt_search);
        // tv_view_topServices = view.findViewById(R.id.tv_view_topServices);
        tv_viewAllCategory = view.findViewById(R.id.tv_viewAllCategory);
        // tv_view_AllCategory = view.findViewById(R.id.tv_view_AllCategory);
        iv_notif = view.findViewById(R.id.iv_notif);
        tv_homeAddress = view.findViewById(R.id.tv_homeAddress);
        tv_userName = view.findViewById(R.id.tv_userName);

        edt_search.setOnClickListener(this);
        // tv_view_topServices.setOnClickListener(this);
        tv_viewAllCategory.setOnClickListener(this);
        //  tv_view_AllCategory.setOnClickListener(this);
        iv_notif.setOnClickListener(this);
        tv_homeAddress.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent = null;
        if (id == R.id.edt_search) {
            intent = new Intent(getActivity(), RecentSearchesActivity.class);
            intent.putExtra(CAME_FROM_SCREEN, HOME_SCREEN);
        }
//        else if (id == R.id.tv_view_topServices) {
//            intent = new Intent(getActivity(), AllCategoriesActivity.class);
//            intent.putExtra(SHOW_LIST, TOP_SERVICES_LIST);
//            intent.putExtra(MY_LIST, (Serializable) mTopServicesList);
//        }
//        else if (id == R.id.tv_viewAllCategory) {
//            intent = new Intent(getActivity(), AllCategoriesActivity.class);
//            intent.putExtra(SHOW_LIST, ALL_SERVICES_LIST);
//            intent.putExtra(MY_LIST, (Serializable) mAllServicesList);
//        }
        else if (id == R.id.tv_viewAllCategory) {
            intent = new Intent(getActivity(), AllCategoriesActivity.class);
            intent.putExtra(SHOW_LIST, ALL_CATEGORY_LIST);
            intent.putExtra(MY_LIST, (Serializable) mAllCategoryList);
        } else if (id == R.id.iv_notif) {
            intent = new Intent(getActivity(), NotificationsActivity.class);
        } else if (id == R.id.tv_homeAddress) {
            ((HomeActivity) Objects.requireNonNull(getActivity())).getMapAddress();
            return;
        }
        startActivity(intent);
    }

    private void serviceHomeList() {
        {
            if (new InternetCheck(getActivity()).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(getActivity());
                dialogPopup.showLoadingDialog(getActivity(), "");
                String token = SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<HomeApiResponse> call = api_service.homeList(TOKEN_KEY + token);
                call.enqueue(new Callback<HomeApiResponse>() {
                    @Override
                    public void onResponse(Call<HomeApiResponse> call, Response<HomeApiResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(getActivity(), getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(getActivity(), (CommonUtil.LogoutUser) getActivity());
                        } else {
                            if (response.isSuccessful()) {
                                HomeApiResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    //ArrayList<HomeApiResponse.TopServicesBean> topServices = (ArrayList<HomeApiResponse.TopServicesBean>) server_response.getTop_services();
                                    ArrayList<HomeApiResponse.ServicesBean> allServices = (ArrayList<HomeApiResponse.ServicesBean>) server_response.getServices();
                                    ArrayList<CategoriesBean> allCategory = (ArrayList<CategoriesBean>) server_response.getCategories();

//                                    if (topServices != null && topServices.size() > 0) {
//                                        mTopServicesList.clear();
//                                        mTopServicesList.addAll(topServices);
//                                    }
                                    if (allServices != null && allServices.size() > 0) {
                                        mAllServicesList.clear();
                                        mAllServicesList.addAll(allServices);
                                    }
                                    if (allCategory != null && allCategory.size() > 0) {
                                        mAllCategoryList.clear();
                                        mAllCategoryList.addAll(allCategory);
                                    }
                                    setUpAllServicesAdapter();

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(getActivity(), getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<HomeApiResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(getActivity(), getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(getActivity(), getString(R.string.internet_lost));
            }
        }
    }

    private class GetAddress extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "onPostExecute: HOME ADDRESS = " + s);
            setHomeAddress(s);

        }

        @Override
        protected String doInBackground(Void... voids) {
            String selectedAddress = "";
            if(!mLatitude.isEmpty())
            {
                try {
                    List<Address> addresses = mGeocoder.getFromLocation(Double.parseDouble(mLatitude), Double.parseDouble(mLongitude), 1);
                    if (addresses != null && addresses.size() > 0) {
                        selectedAddress = addresses.get(0).getAddressLine(0);
                    } else {
                        selectedAddress = "";
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    if (e.getMessage().equalsIgnoreCase("grpc failed")) {
                        CommonUtil.showSnackBar(getActivity(), getString(R.string.enable_gps));
                    }


                }
            }


            return selectedAddress;
        }
    }

    public void setHomeAddress(String address) {
        Log.d(TAG, "setHomeAddress: called..ADDRESS = " + address);
        tv_homeAddress.setText(address);
        SharedPreferenceWriter.getInstance(getActivity()).writeStringValue(SPreferenceKey.USER_HOME_ADDRESS, address);

    }
}