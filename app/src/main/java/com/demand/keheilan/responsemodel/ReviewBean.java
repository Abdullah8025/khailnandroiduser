package com.demand.keheilan.responsemodel;

public class ReviewBean {
    private String user_name;
    private String user_image;
    private float rating;
    private String review;
    private String created_at;

    public String getUser_name() {
        return user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public float getRating() {
        return rating;
    }

    public String getReview() {
        return review;
    }

    public String getCreated_at() {
        return created_at;
    }
}
