package com.demand.keheilan.responsemodel;

import java.util.List;

public class BookedServicesResponse {
    private int status;
    private List<BookedServices> data;

    public int getStatus() {
        return status;
    }

    public List<BookedServices> getData() {
        return data;
    }
}
