package com.demand.keheilan.responsemodel;

import java.util.List;

public class SubCategoryServices {

    /**
     * data : [{"id":1,"category_id":1,"sub_category_id":1,"service_name":"qwerty","field_1":"","field_2":"","field_3":"","field_4":"","base_price":"","image_1":"media/1615457320954-front_3Lu1sWU.png","image_2":"media/1615457322652-back_YlWLPio.png"}]
     * status : 200
     */

    private int status;
    private List<ServicesBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ServicesBean> getData() {
        return data;
    }

    public void setData(List<ServicesBean> data) {
        this.data = data;
    }

}
