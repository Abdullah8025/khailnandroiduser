package com.demand.keheilan.responsemodel;

import java.io.Serializable;
import java.util.List;

public class HomeApiResponse {

    /**
     * services : [{"id":1,"category_id":1,"sub_category_id":1,"service_name":"qwerty","field_1":"","field_2":"","field_3":"","image_1":"media/1615457320954-front_3Lu1sWU.png","image_2":"media/1615457322652-back_YlWLPio.png"},{"id":2,"category_id":3,"sub_category_id":3,"service_name":"asdfd","field_1":"","field_2":"","field_3":"","image_1":"media/image_2021_02_11T08_29_02_450Z_UbflVtF.png","image_2":"media/image_2021_02_11T08_29_02_450Z_IJ1fsBR.png"},{"id":3,"category_id":2,"sub_category_id":2,"service_name":"nbvnbmbpooi","field_1":"","field_2":"","field_3":"","image_1":"media/1615457320954-front_SlaRHc0.png","image_2":"media/1615457320954-front_qUvF9yz.png"}]
     * top_services : [{"id":1,"service_name":"qwerty","image_1":"/media/media/1615457320954-front_3Lu1sWU.png","image_2":"/media/media/1615457322652-back_YlWLPio.png"}]
     * categories : [{"id":1,"category_name":"Food and Beverages","category_image":""},{"id":2,"category_name":"Sports","category_image":""},{"id":3,"category_name":"Information Technology","category_image":"media/1615457322652-back_9u1IdvG.png"}]
     * status : 200
     */

    private int status;
    private List<ServicesBean> services;
    private List<TopServicesBean> top_services;
    private List<CategoriesBean> categories;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ServicesBean> getServices() {
        return services;
    }

    public void setServices(List<ServicesBean> services) {
        this.services = services;
    }

    public List<TopServicesBean> getTop_services() {
        return top_services;
    }

    public void setTop_services(List<TopServicesBean> top_services) {
        this.top_services = top_services;
    }

    public List<CategoriesBean> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoriesBean> categories) {
        this.categories = categories;
    }

    public static class ServicesBean implements Serializable {
        public static final long serialVersionUID = 1L;
        /**
         * id : 1
         * category_id : 1
         * sub_category_id : 1
         * service_name : qwerty
         * field_1 :
         * field_2 :
         * field_3 :
         * image_1 : media/1615457320954-front_3Lu1sWU.png
         * image_2 : media/1615457322652-back_YlWLPio.png
         */

        private String id;
        private int category_id;
        private int sub_category_id;
        private String service_name;
        private String field_1;
        private String field_2;
        private String field_3;
        private String image_1;
        private String image_2;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(int sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public String getService_name() {
            return service_name;
        }

        public void setService_name(String service_name) {
            this.service_name = service_name;
        }

        public String getField_1() {
            return field_1;
        }

        public void setField_1(String field_1) {
            this.field_1 = field_1;
        }

        public String getField_2() {
            return field_2;
        }

        public void setField_2(String field_2) {
            this.field_2 = field_2;
        }

        public String getField_3() {
            return field_3;
        }

        public void setField_3(String field_3) {
            this.field_3 = field_3;
        }

        public String getImage_1() {
            return image_1;
        }

        public void setImage_1(String image_1) {
            this.image_1 = image_1;
        }

        public String getImage_2() {
            return image_2;
        }

        public void setImage_2(String image_2) {
            this.image_2 = image_2;
        }

        @Override
        public String toString() {
            return "ServicesBean{" +
                    "id=" + id +
                    ", category_id=" + category_id +
                    ", sub_category_id=" + sub_category_id +
                    ", service_name='" + service_name + '\'' +
                    ", field_1='" + field_1 + '\'' +
                    ", field_2='" + field_2 + '\'' +
                    ", field_3='" + field_3 + '\'' +
                    ", image_1='" + image_1 + '\'' +
                    ", image_2='" + image_2 + '\'' +
                    '}';
        }
    }

    public static class TopServicesBean implements Serializable {
        public static final long serialVersionUID = 2L;
        /**
         * id : 1
         * service_name : qwerty
         * image_1 : /media/media/1615457320954-front_3Lu1sWU.png
         * image_2 : /media/media/1615457322652-back_YlWLPio.png
         */

        private String id;
        private String service_name;
        private String image_1;
        private String image_2;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getService_name() {
            return service_name;
        }

        public void setService_name(String service_name) {
            this.service_name = service_name;
        }

        public String getImage_1() {
            return image_1;
        }

        public void setImage_1(String image_1) {
            this.image_1 = image_1;
        }

        public String getImage_2() {
            return image_2;
        }

        public void setImage_2(String image_2) {
            this.image_2 = image_2;
        }

        @Override
        public String toString() {
            return "TopServicesBean{" +
                    "id=" + id +
                    ", service_name='" + service_name + '\'' +
                    ", image_1='" + image_1 + '\'' +
                    ", image_2='" + image_2 + '\'' +
                    '}';
        }
    }


}
