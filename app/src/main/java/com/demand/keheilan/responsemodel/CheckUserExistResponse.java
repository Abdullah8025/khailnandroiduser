package com.demand.keheilan.responsemodel;

public class CheckUserExistResponse {

    private boolean message;
    private int status;

    public boolean isMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }
}
