package com.demand.keheilan.responsemodel;

public class AboutUsResponse {

    /**
     * content : Dummy about us
     * content_arabic : Dummy about us in arabic
     * status : 200
     */

    private String content;
    private String content_arabic;
    private int status;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent_arabic() {
        return content_arabic;
    }

    public void setContent_arabic(String content_arabic) {
        this.content_arabic = content_arabic;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
