package com.demand.keheilan.responsemodel;

public class ServicesBean {
    /**
     * id : 1
     * category_id : 1
     * sub_category_id : 1
     * service_name : qwerty
     * field_1 :
     * field_2 :
     * field_3 :
     * field_4 :
     * base_price :
     * image_1 : media/1615457320954-front_3Lu1sWU.png
     * image_2 : media/1615457322652-back_YlWLPio.png
     */

    private String id;
    private int category_id;
    private int sub_category_id;
    private String service_name;
    private String field_1;
    private String field_2;
    private String field_3;
    private String field_4;
    private String base_price;
    private String image_1;
    private String image_2;
    private float average_ratings;
    private String reviews;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(int sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getField_1() {
        return field_1;
    }

    public void setField_1(String field_1) {
        this.field_1 = field_1;
    }

    public String getField_2() {
        return field_2;
    }

    public void setField_2(String field_2) {
        this.field_2 = field_2;
    }

    public String getField_3() {
        return field_3;
    }

    public void setField_3(String field_3) {
        this.field_3 = field_3;
    }

    public String getField_4() {
        return field_4;
    }

    public void setField_4(String field_4) {
        this.field_4 = field_4;
    }

    public String getBase_price() {
        return base_price;
    }

    public void setBase_price(String base_price) {
        this.base_price = base_price;
    }

    public String getImage_1() {
        return image_1;
    }

    public void setImage_1(String image_1) {
        this.image_1 = image_1;
    }

    public String getImage_2() {
        return image_2;
    }

    public void setImage_2(String image_2) {
        this.image_2 = image_2;
    }

    public float getAverage_ratings() {
        return average_ratings;
    }

    public String getReviews() {
        return reviews;
    }
}
