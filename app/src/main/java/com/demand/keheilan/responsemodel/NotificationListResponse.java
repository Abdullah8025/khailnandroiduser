package com.demand.keheilan.responsemodel;

import java.util.List;

public class NotificationListResponse {
    private int status;
    private List<NotificationBean>data;

    public int getStatus() {
        return status;
    }

    public List<NotificationBean> getData() {
        return data;
    }

    public static  class NotificationBean{
        private int id;
        private String title;
        private String body;
        private String created_at;

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getBody() {
            return body;
        }

        public String getCreated_at() {
            return created_at;
        }
    }
}
