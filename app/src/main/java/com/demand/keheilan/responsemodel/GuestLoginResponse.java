package com.demand.keheilan.responsemodel;

public class GuestLoginResponse {
    private int status;
    private String token;

    public int getStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }
}
