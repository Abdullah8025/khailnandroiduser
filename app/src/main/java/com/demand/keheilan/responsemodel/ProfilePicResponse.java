package com.demand.keheilan.responsemodel;

public class ProfilePicResponse {
    private String message;
    private String profile_pic;
    private int status;

    public String getMessage() {
        return message;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public int getStatus() {
        return status;
    }
}
