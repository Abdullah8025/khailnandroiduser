package com.demand.keheilan.responsemodel;

public class CustomerTaxResponse {
    private String data;
    private int status;

    public String getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }
}
