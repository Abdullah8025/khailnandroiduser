package com.demand.keheilan.responsemodel;

public class SignUpResponse {

    /**
     * message : User created successfully
     * id : 5
     * token : 56df9a8ca243675b2777cbded717196b4c194db6
     * full_name : Harshit Test User
     * country_code : 91
     * phone_number : 1231231234
     * status : 200
     */

    private String message;
    private String id;
    private String token;
    private String full_name;
    private String country_code;
    private String phone_number;
    private int status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
