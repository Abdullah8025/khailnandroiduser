package com.demand.keheilan.responsemodel;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    /**
     * token : d72f3caba3349d2f798c08f9fa767e9f6f693202
     * id : 2
     * country_code : 91
     * phone_number : 8882320570
     * status : 200
     * lat : 435467676
     * long : 2345346456
     */

    private String token;
    private String id;
    private String country_code;
    private String phone_number;
    private String full_name;
    private int status;
    private String lat;
    private String lng;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getFull_name() {
        return full_name;
    }
}
