package com.demand.keheilan.responsemodel;

import java.util.List;

public class QueryServicesResponse {
    private int status;
    private List<ServicesBean> data;

    public int getStatus() {
        return status;
    }

    public List<ServicesBean> getServices() {
        return data;
    }
}
