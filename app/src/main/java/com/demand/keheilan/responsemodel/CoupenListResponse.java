package com.demand.keheilan.responsemodel;

import java.util.List;

public class CoupenListResponse {
    private int status;
    private List<Coupen> data;

    public int getStatus() {
        return status;
    }

    public List<Coupen> getData() {
        return data;
    }

    public static class Coupen{
        private String id;
        private String coupon_code;
        private String percent;

        public String getId() {
            return id;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public String getPercent() {
            return percent;
        }
    }
}
