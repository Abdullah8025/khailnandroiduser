package com.demand.keheilan.responsemodel;

public class BookingDetailResponse {
    private String id;
    private String service_id;
    private String service_name;
    private double quote;
    private String service_provider_id;
    private String booking_date;
    private String booking_time;
    private String address;
    private String default_address;
    private String booking_status;
    private String sub_total;
    private String fees;
    private String discount;
    private String total;
    private String requirement;
    private String image_1;
    private String image_2;
    private String booked_at;
    private String promocode_name;
    private String additional_fees;
    private boolean promocode_status;
    private int status;

    public String getId() {
        return id;
    }

    public String getService_id() {
        return service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public double getQuote() {
        return quote;
    }

    public String getService_provider_id() {
        return service_provider_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public String getBooking_time() {
        return booking_time;
    }

    public String getAddress() {
        return address;
    }

    public String getDefault_address() {
        return default_address;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public String getSub_total() {
        return sub_total;
    }

    public String getFees() {
        return fees;
    }

    public String getDiscount() {
        return discount;
    }

    public String getTotal() {
        return total;
    }

    public String getRequirement() {
        return requirement;
    }

    public String getImage_1() {
        return image_1;
    }

    public String getImage_2() {
        return image_2;
    }

    public String getBooked_at() {
        return booked_at;
    }

    public String getPromocode_name() {
        return promocode_name;
    }

    public boolean isPromocode_status() {
        return promocode_status;
    }

    public int getStatus() {
        return status;
    }

    public String getAdditional_fees() {
        return additional_fees;
    }
}
