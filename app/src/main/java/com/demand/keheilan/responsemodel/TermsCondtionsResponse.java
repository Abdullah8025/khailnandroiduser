package com.demand.keheilan.responsemodel;

public class TermsCondtionsResponse {

    /**
     * terms : Dummy terms and condition.
     * terms_arabic : Dummy terms and condition arabic
     * status : 200
     */

    private String terms;
    private String terms_arabic;
    private int status;

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getTerms_arabic() {
        return terms_arabic;
    }

    public void setTerms_arabic(String terms_arabic) {
        this.terms_arabic = terms_arabic;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
