package com.demand.keheilan.responsemodel;

public class ServiceBookingResponse {
    private String message;
    private String booking;
    private int status;

    public String getMessage() {
        return message;
    }

    public String getBooking() {
        return booking;
    }

    public int getStatus() {
        return status;
    }
}
