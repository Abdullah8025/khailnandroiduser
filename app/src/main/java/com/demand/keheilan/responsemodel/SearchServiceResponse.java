package com.demand.keheilan.responsemodel;

import java.util.List;

public class SearchServiceResponse {

    /**
     * services : [{"id":1,"category_id":1,"sub_category_id":1,"service_name":"qwerty","field_1":"","field_2":"","field_3":"","field_4":"","base_price":"","image_1":"media/1615457320954-front_3Lu1sWU.png","image_2":"media/1615457322652-back_YlWLPio.png"}]
     * service_providers : [{"id":1,"full_name":"Amit Kumar Choudhary","email":"binitchoudhary1603@gmail.com","password":"Test@123","confirm_password":"Test@123","profile_pic":"media/image_2021_02_11T08_29_02_450Z_Vz2G12e.png","country_code":91,"phone_number":6661234567,"category_id":1,"sub_category_id":1,"services_id":1,"address":"","created_at":"2021-03-19T07:48:48.841360Z"},{"id":2,"full_name":"Abhishek Kumar Choudhary","email":"binitchoudhary160000003@gmail.com","password":"Test@123","confirm_password":"Test@123","profile_pic":"media/image_2021_02_11T08_29_02_450Z_m9oujrS.png","country_code":91,"phone_number":8881234567,"category_id":1,"sub_category_id":1,"services_id":1,"address":"cn hfj ytru","created_at":"2021-03-19T08:00:16.288553Z"}]
     * categories : [{"id":1,"category_name":"Food and Beverages","category_image":""}]
     * status : 200
     */

    private int status;
    private List<SearchServiceBean> services;
    private List<ServiceProvidersBean> service_providers;
    private List<CategoriesBean> categories;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<SearchServiceBean> getServices() {
        return services;
    }

    public void setServices(List<SearchServiceBean> services) {
        this.services = services;
    }

    public List<ServiceProvidersBean> getService_providers() {
        return service_providers;
    }

    public void setService_providers(List<ServiceProvidersBean> service_providers) {
        this.service_providers = service_providers;
    }

    public List<CategoriesBean> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoriesBean> categories) {
        this.categories = categories;
    }

    public static class ServiceProvidersBean {
        /**
         * id : 1
         * full_name : Amit Kumar Choudhary
         * email : binitchoudhary1603@gmail.com
         * password : Test@123
         * confirm_password : Test@123
         * profile_pic : media/image_2021_02_11T08_29_02_450Z_Vz2G12e.png
         * country_code : 91
         * phone_number : 6661234567
         * category_id : 1
         * sub_category_id : 1
         * services_id : 1
         * address :
         * created_at : 2021-03-19T07:48:48.841360Z
         */

        private int id;
        private String full_name;
        private String email;
        private String password;
        private String confirm_password;
        private String profile_pic;
        private int country_code;
        private long phone_number;
        private int category_id;
        private int sub_category_id;
        private int services_id;
        private String address;
        private String created_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirm_password() {
            return confirm_password;
        }

        public void setConfirm_password(String confirm_password) {
            this.confirm_password = confirm_password;
        }

        public String getProfile_pic() {
            return profile_pic;
        }

        public void setProfile_pic(String profile_pic) {
            this.profile_pic = profile_pic;
        }

        public int getCountry_code() {
            return country_code;
        }

        public void setCountry_code(int country_code) {
            this.country_code = country_code;
        }

        public long getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(long phone_number) {
            this.phone_number = phone_number;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public int getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(int sub_category_id) {
            this.sub_category_id = sub_category_id;
        }

        public int getServices_id() {
            return services_id;
        }

        public void setServices_id(int services_id) {
            this.services_id = services_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
