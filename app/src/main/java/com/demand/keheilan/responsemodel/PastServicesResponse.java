package com.demand.keheilan.responsemodel;

import java.util.List;

public class PastServicesResponse {
    private int status;
    private List<PastService> data;

    public int getStatus() {
        return status;
    }

    public List<PastService> getData() {
        return data;
    }
}
