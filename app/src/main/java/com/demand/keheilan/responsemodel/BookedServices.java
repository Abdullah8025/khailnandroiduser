package com.demand.keheilan.responsemodel;

public class BookedServices {
    private String id;
    private String service_name;
    private String image_1;
    private String image_2;
    private String price;
    private String date;
    private String time;
    private String address;
    private String booking_status;
    private String base_price;
    private String tax;

    public String getId() {
        return id;
    }

    public String getService_name() {
        return service_name;
    }

    public String getImage_1() {
        return image_1;
    }

    public String getImage_2() {
        return image_2;
    }

    public String getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getAddress() {
        return address;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public String getBase_price() {
        return base_price;
    }

    public String getTax() {
        return tax;
    }
}
