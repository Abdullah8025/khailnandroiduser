package com.demand.keheilan.responsemodel;

import java.io.Serializable;

public class CategoriesBean implements Serializable {
    public static final long serialVersionUID = 3L;
    /**
     * id : 1
     * category_name : Food and Beverages
     * category_image :
     */

    private String id;
    private String category_name;
    private String category_image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }

    @Override
    public String toString() {
        return "CategoriesBean{" +
                "id=" + id +
                ", category_name='" + category_name + '\'' +
                ", category_image='" + category_image + '\'' +
                '}';
    }
}
