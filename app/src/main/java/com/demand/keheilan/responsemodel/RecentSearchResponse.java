package com.demand.keheilan.responsemodel;

import java.util.List;

public class RecentSearchResponse {

    /**
     * data : [{"id":12,"user_id":7,"searched_value":"ac"}]
     * status : 200
     */

    private int status;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 12
         * user_id : 7
         * searched_value : ac
         */

        private int id;
        private int user_id;
        private String searched_value;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getSearched_value() {
            return searched_value;
        }

        public void setSearched_value(String searched_value) {
            this.searched_value = searched_value;
        }
    }
}
