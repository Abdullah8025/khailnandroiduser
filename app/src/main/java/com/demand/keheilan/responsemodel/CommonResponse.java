package com.demand.keheilan.responsemodel;

public class CommonResponse {
    private String message;
    private int status;

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }
}
