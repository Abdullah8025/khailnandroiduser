package com.demand.keheilan.responsemodel;

public class PrivacyPolicyResponse {

    /**
     * policy : dummy privacy policy.
     * policy_arabic : dummy privacy policy in arabic
     * status : 200
     */

    private String policy;
    private String policy_arabic;
    private int status;

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getPolicy_arabic() {
        return policy_arabic;
    }

    public void setPolicy_arabic(String policy_arabic) {
        this.policy_arabic = policy_arabic;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
