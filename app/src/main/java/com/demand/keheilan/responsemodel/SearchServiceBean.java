package com.demand.keheilan.responsemodel;

public class SearchServiceBean {

    private String service_id;
    private String service_name;
    private String field_1;
    private String field_2;
    private String field_3;
    private String field_4;
    private String base_price;
    private String image_1;
    private String image_2;
    private String rating_count;
    private float average_rating;

    public String getService_id() {
        return service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public String getField_1() {
        return field_1;
    }

    public String getField_2() {
        return field_2;
    }

    public String getField_3() {
        return field_3;
    }

    public String getField_4() {
        return field_4;
    }

    public String getBase_price() {
        return base_price;
    }

    public String getImage_1() {
        return image_1;
    }

    public String getImage_2() {
        return image_2;
    }

    public String getRating_count() {
        return rating_count;
    }

    public float getAverage_rating() {
        return average_rating;
    }
}
