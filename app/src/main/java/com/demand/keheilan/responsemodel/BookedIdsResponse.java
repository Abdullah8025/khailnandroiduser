package com.demand.keheilan.responsemodel;

import java.util.List;

public class BookedIdsResponse {
    private int status;
    private List<BookingIdBean> data;

    public int getStatus() {
        return status;
    }

    public List<BookingIdBean> getData() {
        return data;
    }

    public static class BookingIdBean{
        private String id;

        public String getId() {
            return id;
        }
    }
}
