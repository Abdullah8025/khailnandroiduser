package com.demand.keheilan.responsemodel;

import java.util.List;

public class SubCategoryResponse {

    /**
     * data : [{"id":1,"category_id":1,"sub_category_name":"Test3","sub_category_image":"media/1615457320954-front_XqIddyk.png"}]
     * status : 200
     */

    private int status;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * category_id : 1
         * sub_category_name : Test3
         * sub_category_image : media/1615457320954-front_XqIddyk.png
         */

        private String id;
        private String category_id;
        private String sub_category_name;
        private String sub_category_image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getSub_category_name() {
            return sub_category_name;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }

        public String getSub_category_image() {
            return sub_category_image;
        }

        public void setSub_category_image(String sub_category_image) {
            this.sub_category_image = sub_category_image;
        }
    }
}
