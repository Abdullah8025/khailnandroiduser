package com.demand.keheilan.responsemodel;

import java.util.List;

public class ReviewsResponse {
    private int status;
    private int rating_count;
    private List<ReviewBean> data;

    public int getStatus() {
        return status;
    }

    public int getRating_count() {
        return rating_count;
    }

    public List<ReviewBean> getData() {
        return data;
    }
}
