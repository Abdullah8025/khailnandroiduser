package com.demand.keheilan.adapter;

import android.content.Context;
import android.graphics.Color;
import android.media.session.MediaController;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demand.keheilan.R;
import com.demand.keheilan.model.ServiceTimeModel;

import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ServiceTimeAdapter extends RecyclerView.Adapter<ServiceTimeAdapter.ViewHolder> {
    private static final String TAG = "ServiceTimeAdapter";
    private Context mContext;
    private List<ServiceTimeModel> mTimingList;
    private OnTimingClicked mOnTimingClicked;


    public interface OnTimingClicked {
        void onTimingClicked(int position);
    }

    public ServiceTimeAdapter(Context mContext, List<ServiceTimeModel> mTimingList, OnTimingClicked timingClickedListener) {
        this.mContext = mContext;
        this.mTimingList = mTimingList;
        this.mOnTimingClicked = timingClickedListener;
    }

    @NonNull
    @Override
    public ServiceTimeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_service_timing, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceTimeAdapter.ViewHolder holder, int position) {

        ServiceTimeModel timing = mTimingList.get(position);
        holder.tv_time.setText(timing.getTiming());
        if (timing.isDateEnabled())
            holder.tv_time.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        else
            holder.tv_time.setTextColor(ContextCompat.getColor(mContext, R.color.color_light_grey));

        holder.checkbox_time.setChecked(timing.isChecked());

        holder.checkbox_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timing.isDateEnabled()) {
                    if (mOnTimingClicked != null)
                        mOnTimingClicked.onTimingClicked(position);

                    refreshList(position);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.time_slot_not_available), Toast.LENGTH_SHORT).show();
                    unSelectPosition(position);
                }
            }
        });


    }

    private void unSelectPosition(int pos) {
        mTimingList.get(pos).setChecked(false);
        notifyDataSetChanged();
    }

    private void refreshList(int position) {
        for (int i = 0; i < mTimingList.size(); i++) {
            if (i == position)
                mTimingList.get(i).setChecked(true);
            else
                mTimingList.get(i).setChecked(false);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mTimingList != null ? mTimingList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_time;
        private CheckBox checkbox_time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_time = itemView.findViewById(R.id.tv_time);
            checkbox_time = itemView.findViewById(R.id.checkbox_time);
        }
    }
}
