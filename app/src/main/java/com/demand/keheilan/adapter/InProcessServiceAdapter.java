package com.demand.keheilan.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.activity.AcceptServiceActivity;
import com.demand.keheilan.responsemodel.BookedServices;

import java.util.List;

import static com.demand.keheilan.util.MyAppConstants.BOOKING_STATUS_IN_PROCESS;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_STATUS_PENDING;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_ITEM;

public class InProcessServiceAdapter extends RecyclerView.Adapter<InProcessServiceAdapter.ViewHolder> {
    private Context mContext;
    private List<BookedServices> mInProcessServicesList;
    private UpcomingServiceAdapter.OnBookingClicked mOnBookingClicked;

    public InProcessServiceAdapter(Context mContext, List<BookedServices> mInProcessServicesList, UpcomingServiceAdapter.OnBookingClicked mOnBookingClicked) {
        this.mContext = mContext;
        this.mInProcessServicesList = mInProcessServicesList;
        this.mOnBookingClicked = mOnBookingClicked;
    }

    @NonNull
    @Override
    public InProcessServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_inprocess, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InProcessServiceAdapter.ViewHolder holder, int position) {
        BookedServices service = mInProcessServicesList.get(position);
        holder.tv_priceType.setText(mContext.getString(R.string.offered_price));
        Glide.with(mContext)
                .load(service.getImage_1())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.iv_pic);

        holder.tv_serviceName.setText(service.getService_name());
        holder.tv_timing.setText(service.getTime());
        holder.tv_location.setText(service.getAddress());
        holder.tv_date.setText(service.getDate());
        holder.tv_price.setText(mContext.getString(R.string.num_amount, service.getPrice()));
        holder.tv_status.setText(mContext.getString(R.string.booking_status_accepted));
        holder.tv_customerTax.setText(mContext.getString(R.string.num_amount,service.getTax()));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnBookingClicked != null)
                    mOnBookingClicked.onInProcessServiceClicked(service.getId(),service.getBase_price());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mInProcessServicesList != null ? mInProcessServicesList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_pic;
        private TextView tv_serviceName, tv_timing, tv_location, tv_date, tv_price, tv_status, tv_priceType,tv_customerTax;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_pic = itemView.findViewById(R.id.iv_pic);
            tv_serviceName = itemView.findViewById(R.id.tv_serviceName);
            tv_timing = itemView.findViewById(R.id.tv_timing);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_priceType = itemView.findViewById(R.id.tv_priceType);
            tv_customerTax =itemView.findViewById(R.id.tv_customerTax);
        }
    }
}
