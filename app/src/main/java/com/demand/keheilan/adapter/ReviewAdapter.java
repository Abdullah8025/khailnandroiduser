package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.ReviewBean;
import com.demand.keheilan.util.CommonUtil;

import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    private Context mContext;
    private List<ReviewBean> mReviewList;

    public ReviewAdapter(Context mContext, List<ReviewBean> reviewList) {
        this.mContext = mContext;
        this.mReviewList = reviewList;
    }

    @NonNull
    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_review, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapter.ViewHolder holder, int position) {
        ReviewBean review = mReviewList.get(position);
        Glide.with(mContext).load(review.getUser_image())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.iv_userPic);
        holder.tv_userName.setText(review.getUser_name());
        holder.ratingBar_review.setRating(review.getRating());
        holder.tv_review.setText(review.getReview());
        if (CommonUtil.blankValidation(review.getCreated_at())) {
            if (review.getCreated_at().contains("T")) {
                String[] parts = review.getCreated_at().split("T");
                holder.tv_date.setText(parts[0]);
            }
        }else {
            holder.tv_date.setText("");
        }


    }

    @Override
    public int getItemCount() {
        return mReviewList != null ? mReviewList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_userPic;
        private TextView tv_userName, tv_review, tv_date;
        private RatingBar ratingBar_review;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_userPic = itemView.findViewById(R.id.iv_userPic);
            tv_userName = itemView.findViewById(R.id.tv_userName);
            tv_review = itemView.findViewById(R.id.tv_review);
            tv_date = itemView.findViewById(R.id.tv_date);
            ratingBar_review = itemView.findViewById(R.id.ratingBar_review);

        }
    }
}
