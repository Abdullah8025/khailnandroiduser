package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.RecentSearchResponse;

import java.util.List;

public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.ViewHolder> {
    private Context mContext;
    private List<RecentSearchResponse.DataBean> mRecentSearchList;
    private OnRecentSearchClicked mOnRecentSearchClicked;

    public interface OnRecentSearchClicked {
        void onRecentSearchClicked(String text);
    }

    public RecentSearchAdapter(Context mContext, List<RecentSearchResponse.DataBean> mRecentSearchList, OnRecentSearchClicked recentSearchClickedListener) {
        this.mContext = mContext;
        this.mRecentSearchList = mRecentSearchList;
        this.mOnRecentSearchClicked = recentSearchClickedListener;
    }

    @NonNull
    @Override
    public RecentSearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_recent_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentSearchAdapter.ViewHolder holder, int position) {
        RecentSearchResponse.DataBean recentSearch = mRecentSearchList.get(position);
        holder.tv_search.setText(recentSearch.getSearched_value());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnRecentSearchClicked != null)
                    mOnRecentSearchClicked.onRecentSearchClicked(recentSearch.getSearched_value());
            }
        });


    }

    @Override
    public int getItemCount() {
        return mRecentSearchList != null ? mRecentSearchList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_search;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_search = itemView.findViewById(R.id.tv_search);
        }
    }
}
