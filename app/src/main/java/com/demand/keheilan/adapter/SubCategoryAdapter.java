package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.CategoriesBean;
import com.demand.keheilan.responsemodel.SubCategoryResponse;

import java.util.List;


public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {
    private Context mContext;
    private List<SubCategoryResponse.DataBean> mSubCategoryList;
    private OnSubCategoryClick mOnSubCategoryClick;

    public interface OnSubCategoryClick{
        void onSubCategoryClick(String subCategoryId);
    }

    public SubCategoryAdapter(Context mContext, List<SubCategoryResponse.DataBean> mSubCategoryList, OnSubCategoryClick mOnSubCategoryClick) {
        this.mContext = mContext;
        this.mSubCategoryList = mSubCategoryList;
        this.mOnSubCategoryClick = mOnSubCategoryClick;
    }

    @NonNull
    @Override
    public SubCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_all_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryAdapter.ViewHolder holder, int position) {
        SubCategoryResponse.DataBean subCategory = mSubCategoryList.get(position);
        Glide.with(mContext).load( subCategory.getSub_category_image()).
                placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(holder.iv_service);

        holder.tv_service_title.setText(subCategory.getSub_category_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnSubCategoryClick != null) {
                    mOnSubCategoryClick.onSubCategoryClick(subCategory.getId());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mSubCategoryList != null ? mSubCategoryList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_service;
        private TextView tv_service_title;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_service = itemView.findViewById(R.id.iv_service);
            tv_service_title = itemView.findViewById(R.id.tv_service_title);
        }
    }
}
