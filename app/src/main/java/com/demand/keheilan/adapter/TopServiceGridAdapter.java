package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.HomeApiResponse;

import java.util.List;


public class TopServiceGridAdapter extends RecyclerView.Adapter<TopServiceGridAdapter.ViewHolder> {
    private Context mContext;
    private List<HomeApiResponse.TopServicesBean> mTopServicesList;
    private OnTopServiceGridClick mOnTopServiceGridClick;

    public interface OnTopServiceGridClick {
        void onTopServiceGridClicked(HomeApiResponse.TopServicesBean topServicesBean);
    }

    public TopServiceGridAdapter(Context mContext, List<HomeApiResponse.TopServicesBean> mTopServicesList, OnTopServiceGridClick mOnTopServiceGridClick) {
        this.mContext = mContext;
        this.mTopServicesList = mTopServicesList;
        this.mOnTopServiceGridClick = mOnTopServiceGridClick;
    }

    @NonNull
    @Override
    public TopServiceGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_all_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopServiceGridAdapter.ViewHolder holder, int position) {
        HomeApiResponse.TopServicesBean topService = mTopServicesList.get(position);
        Glide.with(mContext).load( topService.getImage_1()).
                placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(holder.iv_service);

        holder.tv_service_title.setText(topService.getService_name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnTopServiceGridClick != null) {
                    mOnTopServiceGridClick.onTopServiceGridClicked(topService);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTopServicesList != null ? mTopServicesList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_service;
        private TextView tv_service_title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_service = itemView.findViewById(R.id.iv_service);
            tv_service_title = itemView.findViewById(R.id.tv_service_title);
        }
    }
}
