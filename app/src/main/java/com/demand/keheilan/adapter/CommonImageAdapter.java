package com.demand.keheilan.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demand.keheilan.R;
import com.demand.keheilan.util.ImageGlider;

import java.util.List;

public class CommonImageAdapter extends RecyclerView.Adapter<CommonImageAdapter.MyViewHolder> {

    private Context context;
    private List<String> list;
    private onImageClick listner;

    public CommonImageAdapter(Context context, List<String> list, onImageClick listner) {
        this.context = context;
        this.list = list;
        this.listner = listner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adpter_custom_image, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ImageGlider.setRoundImage(context, holder.ivPhoto, holder.progressBar, "" + Uri.parse(list.get(position)));
    }


    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ProgressBar progressBar;
        ImageView ivPhoto;
        ImageView ivRemove;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
            ivPhoto = itemView.findViewById(R.id.ivPhoto);
            ivRemove = itemView.findViewById(R.id.ivRemove);
            ivRemove.setOnClickListener(this);
//            ivPhoto.setOnClickListener(this::onClick);
        }

        @Override
        public void onClick(View v) {
            int id=v.getId();
            if (id == R.id.ivRemove){
                if (listner != null) {
                    listner.onRemove(getAdapterPosition());
                }
            }
        }


    }

    public interface onImageClick {
        void onRemove(int pos);
        void onShowLargeImage(ImageView imageView, String path);
    }
}
