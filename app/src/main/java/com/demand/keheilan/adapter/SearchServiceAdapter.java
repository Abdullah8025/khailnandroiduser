package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.SearchServiceBean;
import com.demand.keheilan.responsemodel.SearchServiceResponse;
import com.demand.keheilan.responsemodel.ServicesBean;

import java.util.List;

public class SearchServiceAdapter extends RecyclerView.Adapter<SearchServiceAdapter.ViewHolder> {
    private Context mContext;
    private List<SearchServiceBean> mServicesList;
    private OnViewServiceClicked mOnViewServiceClicked;

    public interface OnViewServiceClicked {
        void onViewServiceClicked(String serviceId);

        void onAddServiceClicked(String serviceId,String basePrice);
    }

    public SearchServiceAdapter(Context mContext, List<SearchServiceBean> mServicesList, OnViewServiceClicked mOnViewServiceClicked) {
        this.mContext = mContext;
        this.mServicesList = mServicesList;
        this.mOnViewServiceClicked = mOnViewServiceClicked;
    }

    @NonNull
    @Override
    public SearchServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchServiceAdapter.ViewHolder holder, int position) {

        SearchServiceBean service = mServicesList.get(position);
        Glide.with(mContext).load(service.getImage_1())
                .error(R.drawable.profile_placeholder)
                .placeholder(R.drawable.profile_placeholder)
                .into(holder.iv_serviceImage);

        holder.tv_serviceName.setText(service.getService_name());
        holder.tv_amount.setText(mContext.getString(R.string.num_amount,service.getBase_price()));
        holder.tv_review.setText(mContext.getString(R.string.num_reviews,service.getRating_count()));
        holder.ratingBar.setRating(service.getAverage_rating());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnViewServiceClicked != null) {
                    mOnViewServiceClicked.onViewServiceClicked(service.getService_id());
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mServicesList != null ? mServicesList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Button btn_add;
        private ImageView iv_serviceImage;
        private TextView tv_serviceName, tv_review, tv_amount;
        private RatingBar ratingBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btn_add = itemView.findViewById(R.id.btn_add);
            iv_serviceImage = itemView.findViewById(R.id.iv_serviceImage);
            tv_serviceName = itemView.findViewById(R.id.tv_serviceName);
            tv_review = itemView.findViewById(R.id.tv_review);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            ratingBar = itemView.findViewById(R.id.ratingBar);

            btn_add.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.btn_add) {
                if (mOnViewServiceClicked != null){
                    mOnViewServiceClicked.onAddServiceClicked(mServicesList.get(getAdapterPosition()).getService_id(),mServicesList.get(getAdapterPosition()).getBase_price());
                }

            }
        }
    }
}
