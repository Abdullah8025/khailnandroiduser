package com.demand.keheilan.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.activity.AcceptServiceActivity;
import com.demand.keheilan.responsemodel.BookedServices;

import java.util.List;

import static com.demand.keheilan.util.MyAppConstants.BASE_PRICE;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_STATUS_PENDING;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.UPCOMING_ITEM;

public class UpcomingServiceAdapter extends RecyclerView.Adapter<UpcomingServiceAdapter.ViewHolder> {
    private Context mContext;
    private List<BookedServices> mServicesList;
    private OnBookingClicked mOnBookingClicked;

    public interface OnBookingClicked {
        void onUpcomingBookingClicked(String bookingId, String basePrice);
        void onInProcessServiceClicked(String bookingId,String basePrice);
    }

    public UpcomingServiceAdapter(Context mContext, List<BookedServices> mServicesList, OnBookingClicked bookingClickedListener) {
        this.mContext = mContext;
        this.mServicesList = mServicesList;
        this.mOnBookingClicked = bookingClickedListener;
    }

    @NonNull
    @Override
    public UpcomingServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_inprocess, parent, false);
        return new UpcomingServiceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UpcomingServiceAdapter.ViewHolder holder, int position) {
        BookedServices service = mServicesList.get(position);
        holder.tv_priceType.setVisibility(View.GONE);
        holder.tv_price.setVisibility(View.GONE);
        holder.view5.setVisibility(View.GONE);

        holder.tv_priceType.setText(mContext.getString(R.string.base_price));
        Glide.with(mContext)
                .load(service.getImage_1())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.iv_pic);

        holder.tv_serviceName.setText(service.getService_name());
        holder.tv_timing.setText(service.getTime());
        holder.tv_location.setText(service.getAddress());
        holder.tv_date.setText(service.getDate());
        holder.tv_price.setText(mContext.getString(R.string.num_amount, service.getBase_price()));
        holder.tv_status.setText(mContext.getString(R.string.booking_status_pending));
        holder.tv_customerTax.setText(mContext.getString(R.string.num_amount,service.getTax()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnBookingClicked != null){
                    mOnBookingClicked.onUpcomingBookingClicked(service.getId(),service.getBase_price());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mServicesList != null ? mServicesList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_pic;
        private TextView tv_serviceName, tv_timing, tv_location, tv_date, tv_price, tv_status, tv_priceType,tv_customerTax;
        private View view5;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_pic = itemView.findViewById(R.id.iv_pic);
            tv_serviceName = itemView.findViewById(R.id.tv_serviceName);
            tv_timing = itemView.findViewById(R.id.tv_timing);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_priceType = itemView.findViewById(R.id.tv_priceType);
            tv_customerTax =itemView.findViewById(R.id.tv_customerTax);
            view5=itemView.findViewById(R.id.view5);
        }
    }
}
