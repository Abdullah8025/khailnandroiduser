package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.HomeApiResponse;

import java.util.List;


public class TopServicesAdapter extends RecyclerView.Adapter<TopServicesAdapter.ViewHolder> {
    private Context mContext;
    private List<HomeApiResponse.TopServicesBean> mTopServicesList;
    private OnTopServiceClick mOnTopServiceClick;


    public interface OnTopServiceClick {
        void onTopServiceClick(HomeApiResponse.TopServicesBean topServicesBean);
    }


    public TopServicesAdapter(Context mContext, List<HomeApiResponse.TopServicesBean> mTopServicesList, OnTopServiceClick onTopServiceClick) {
        this.mContext = mContext;
        this.mTopServicesList = mTopServicesList;
        this.mOnTopServiceClick = onTopServiceClick;
    }

    @NonNull
    @Override
    public TopServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_service, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopServicesAdapter.ViewHolder holder, int position) {
        HomeApiResponse.TopServicesBean topService = mTopServicesList.get(position);

        Glide.with(mContext).load( topService.getImage_1()).
                placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(holder.iv_service);

        holder.tv_service_title.setText(topService.getService_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnTopServiceClick != null) {
                    mOnTopServiceClick.onTopServiceClick(topService);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mTopServicesList != null ? mTopServicesList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_service;
        private TextView tv_service_title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_service = itemView.findViewById(R.id.iv_service);
            tv_service_title = itemView.findViewById(R.id.tv_service_title);


        }
    }

}
