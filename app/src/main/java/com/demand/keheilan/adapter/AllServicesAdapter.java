package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.HomeApiResponse;

import java.util.List;


public class AllServicesAdapter extends RecyclerView.Adapter<AllServicesAdapter.ViewHolder> {
    private Context mContext;
    private List<HomeApiResponse.ServicesBean> mAllServicesList;
    private OnAllServiceClick mOnAllServiceClick;

    public interface OnAllServiceClick{
        void onAllServiceClick(HomeApiResponse.ServicesBean allservice);
    }

    public AllServicesAdapter(Context mContext, List<HomeApiResponse.ServicesBean> mAllServicesList, OnAllServiceClick mOnAllServiceClick) {
        this.mContext = mContext;
        this.mAllServicesList = mAllServicesList;
        this.mOnAllServiceClick = mOnAllServiceClick;
    }

    @NonNull
    @Override
    public AllServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_service, parent, false);
        return new AllServicesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllServicesAdapter.ViewHolder holder, int position) {
        HomeApiResponse.ServicesBean allService = mAllServicesList.get(position);
        Glide.with(mContext).load( allService.getImage_1()).
                placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(holder.iv_service);

        holder.tv_service_title.setText(allService.getService_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnAllServiceClick != null){
                    mOnAllServiceClick.onAllServiceClick(allService);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mAllServicesList != null ? mAllServicesList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_service;
        private TextView tv_service_title;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_service = itemView.findViewById(R.id.iv_service);
            tv_service_title = itemView.findViewById(R.id.tv_service_title);
        }
    }
}
