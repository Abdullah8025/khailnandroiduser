package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.CoupenListResponse;

import java.util.List;

public class CouponListAdapter extends RecyclerView.Adapter<CouponListAdapter.ViewHolder> {
    private Context mContext;
    private List<CoupenListResponse.Coupen> mCouponsList;
    private OnCouponClicked mOnCouponClicked;

    public interface OnCouponClicked {
        void onCouponSelected(String couponId, String couponCode);
    }

    public CouponListAdapter(Context mContext, List<CoupenListResponse.Coupen> mCouponsList, OnCouponClicked mOnCouponClicked) {
        this.mContext = mContext;
        this.mCouponsList = mCouponsList;
        this.mOnCouponClicked = mOnCouponClicked;
    }

    @NonNull
    @Override
    public CouponListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_coupon_code, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponListAdapter.ViewHolder holder, int position) {
        CoupenListResponse.Coupen coupon = mCouponsList.get(position);
        holder.tv_percent.setText(mContext.getString(R.string.take_sar_percent_off, coupon.getPercent()));
        holder.tv_coupon_name.setText(coupon.getCoupon_code());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCouponClicked != null)
                    mOnCouponClicked.onCouponSelected(coupon.getId(), coupon.getCoupon_code());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCouponsList != null ? mCouponsList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_percent, tv_coupon_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_percent = itemView.findViewById(R.id.tv_percent);
            tv_coupon_name = itemView.findViewById(R.id.tv_coupon_name);
        }
    }
}
