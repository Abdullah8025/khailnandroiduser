package com.demand.keheilan.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.activity.AcceptServiceActivity;
import com.demand.keheilan.responsemodel.PastService;

import java.util.List;

import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_ITEM;
import static com.demand.keheilan.util.MyAppConstants.PAST_ITEM;

public class PastServiceAdapter extends RecyclerView.Adapter<PastServiceAdapter.ViewHolder> {
    private static final String TAG = "PastServiceAdapter";
    private Context mContext;
    private List<PastService> mPastServiceList;
    private OnPastServiceClicked mOnPastServiceClicked;

    public interface OnPastServiceClicked {
        void onRatingClicked(String serviceId);

        void onRebookingClicked(String serviceId, String basePrice);

        void onPastServiceClicked(String bookingId, String price);
    }

    public PastServiceAdapter(Context mContext, List<PastService> pastServices, OnPastServiceClicked onPastServiceClicked) {
        this.mContext = mContext;
        this.mOnPastServiceClicked = onPastServiceClicked;
        this.mPastServiceList = pastServices;
    }

    @NonNull
    @Override
    public PastServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_past_booking, parent, false);
        return new PastServiceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PastServiceAdapter.ViewHolder holder, int position) {
        PastService service = mPastServiceList.get(position);
        Glide.with(mContext)
                .load(service.getImage_1())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.iv_pic);
        holder.tv_serviceName.setText(service.getService_name());
        holder.tv_amount.setText(mContext.getString(R.string.num_amount, service.getPrice()));
        holder.tv_dateTime.setText(service.getDate());
        if (service.getRating_status()) {
            holder.ratingBar_service.setVisibility(View.VISIBLE);
            holder.btn_rating.setVisibility(View.GONE);
            holder.ratingBar_service.setRating(service.getRating());
        } else {
            holder.ratingBar_service.setVisibility(View.GONE);
            holder.btn_rating.setVisibility(View.VISIBLE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnPastServiceClicked != null)
                    mOnPastServiceClicked.onPastServiceClicked(service.getId(), service.getBase_price());

            }
        });
    }

    @Override
    public int getItemCount() {
        return mPastServiceList != null ? mPastServiceList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Button btn_rating, btn_rebooking;
        private ImageView iv_pic;
        private TextView tv_serviceName, tv_amount, tv_dateTime;
        private RatingBar ratingBar_service;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            btn_rating = itemView.findViewById(R.id.btn_rating);
            btn_rebooking = itemView.findViewById(R.id.btn_rebooking);
            iv_pic = itemView.findViewById(R.id.iv_pic);
            tv_serviceName = itemView.findViewById(R.id.tv_serviceName);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_dateTime = itemView.findViewById(R.id.tv_dateTime);
            ratingBar_service = itemView.findViewById(R.id.ratingBar_service);

            btn_rating.setOnClickListener(this);
            btn_rebooking.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.btn_rating) {
                if (mOnPastServiceClicked != null)
                    mOnPastServiceClicked.onRatingClicked(mPastServiceList.get(getAdapterPosition()).getId());
            } else if (id == R.id.btn_rebooking) {
                if (mOnPastServiceClicked != null) {
                    mOnPastServiceClicked.onRebookingClicked(mPastServiceList.get(getAdapterPosition()).getService_id(),
                            mPastServiceList.get(getAdapterPosition()).getBase_price());
                }
            }
        }
    }
}
