package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.CategoriesBean;
import com.demand.keheilan.responsemodel.HomeApiResponse;

import java.util.List;


public class AllCategoryGridAdapter extends RecyclerView.Adapter<AllCategoryGridAdapter.ViewHolder> {
    private Context mContext;
    private List<CategoriesBean> mAllCategoryList;
    private OnCategoryGridClick mOnCategoryGridClick;

    public interface OnCategoryGridClick {
        void onCategoryGridClick(CategoriesBean categoriesBean);
    }

    public AllCategoryGridAdapter(Context mContext, List<CategoriesBean> mAllCategoryList, OnCategoryGridClick mOnCategoryGridClick) {
        this.mContext = mContext;
        this.mAllCategoryList = mAllCategoryList;
        this.mOnCategoryGridClick = mOnCategoryGridClick;
    }

    @NonNull
    @Override
    public AllCategoryGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_all_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllCategoryGridAdapter.ViewHolder holder, int position) {
        CategoriesBean category = mAllCategoryList.get(position);
        Glide.with(mContext).load( category.getCategory_image()).
                placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(holder.iv_service);

        holder.tv_service_title.setText(category.getCategory_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCategoryGridClick != null) {
                    mOnCategoryGridClick.onCategoryGridClick(category);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAllCategoryList != null ? mAllCategoryList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_service;
        private TextView tv_service_title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_service = itemView.findViewById(R.id.iv_service);
            tv_service_title = itemView.findViewById(R.id.tv_service_title);
        }
    }
}
