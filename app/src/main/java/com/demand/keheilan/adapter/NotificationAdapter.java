package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.NotificationListResponse;
import com.demand.keheilan.util.CommonUtil;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private static final String TAG = "NotificationAdapter";
    private Context mContext;
    private List<NotificationListResponse.NotificationBean> mNotificationList;

    public NotificationAdapter(Context mContext, List<NotificationListResponse.NotificationBean> mNotificationList) {
        this.mContext = mContext;
        this.mNotificationList = mNotificationList;
    }

    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.ViewHolder holder, int position) {

        NotificationListResponse.NotificationBean notification = mNotificationList.get(position);
        holder.tv_notiTitle.setText(notification.getTitle());
        holder.tv_notiMessage.setText(notification.getBody());

        if (CommonUtil.blankValidation(notification.getCreated_at())) {
            if (notification.getCreated_at().contains("T")) {
                String[] parts = notification.getCreated_at().split("T");
                holder.tv_date.setText(parts[0]);
            }
        } else {
            holder.tv_date.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return mNotificationList != null ? mNotificationList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_notiTitle, tv_notiMessage, tv_date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_notiTitle = itemView.findViewById(R.id.tv_notiTitle);
            tv_notiMessage = itemView.findViewById(R.id.tv_notiMessage);
            tv_date = itemView.findViewById(R.id.tv_date);
        }
    }
}
