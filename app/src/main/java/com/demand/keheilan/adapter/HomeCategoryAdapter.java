package com.demand.keheilan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.CategoriesBean;
import com.demand.keheilan.responsemodel.HomeApiResponse;

import java.util.List;


public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.ViewHolder> {
    private Context mContext;
    private List<CategoriesBean> mAllCategoryList;
    private OnCategoryClick mOnCategoryClick;

    public interface OnCategoryClick {
        void onCategoryClick(String categoryId, String categoryName);
    }


    public HomeCategoryAdapter(Context mContext, List<CategoriesBean> mAllCategoryList, OnCategoryClick onCategoryClick) {
        this.mContext = mContext;
        this.mAllCategoryList = mAllCategoryList;
        this.mOnCategoryClick = onCategoryClick;
    }


    @NonNull
    @Override
    public HomeCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_search_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeCategoryAdapter.ViewHolder holder, int position) {

        CategoriesBean category = mAllCategoryList.get(position);
        Glide.with(mContext).load(category.getCategory_image()).
                placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(holder.iv_service);

        holder.tv_service_title.setText(category.getCategory_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCategoryClick != null) {
                    mOnCategoryClick.onCategoryClick(category.getId(), category.getCategory_name());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mAllCategoryList != null ? mAllCategoryList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_service;
        private TextView tv_service_title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_service = itemView.findViewById(R.id.iv_service);
            tv_service_title = itemView.findViewById(R.id.tv_service_title);
        }
    }
}
