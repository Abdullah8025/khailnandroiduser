package com.demand.keheilan.model;

public class ServiceModel {
    private int serviceImage;
    private String Title;

    public ServiceModel(int serviceImage, String title) {
        this.serviceImage = serviceImage;
        Title = title;
    }

    public int getServiceImage() {
        return serviceImage;
    }

    public String getTitle() {
        return Title;
    }
}
