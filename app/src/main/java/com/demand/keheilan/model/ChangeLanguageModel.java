package com.demand.keheilan.model;

public class ChangeLanguageModel {
    private String language;

    public ChangeLanguageModel(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

}