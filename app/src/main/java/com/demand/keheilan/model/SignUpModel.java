package com.demand.keheilan.model;

import android.util.Log;

public class SignUpModel {
    private static final String TAG = "SignUpModel";
    private static SignUpModel mSignUpModel;
    private String phoneNumber, langCode,countryCode;

    private SignUpModel(){

    }

    public static SignUpModel getInstance(){
        if (mSignUpModel==null){
            Log.d(TAG, "getInstance: new Instance created...");
            mSignUpModel=new SignUpModel();
        }
        Log.d(TAG, "getInstance: same instance used...");
        return mSignUpModel;
    }
    public static void nullModel(){
        mSignUpModel=null;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "SignUpModel{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", langCode='" + langCode + '\'' +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}
