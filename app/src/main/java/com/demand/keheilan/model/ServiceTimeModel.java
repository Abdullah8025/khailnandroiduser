package com.demand.keheilan.model;

public class ServiceTimeModel {
    private String timing;
    private boolean isChecked;
    private boolean isDateEnabled;


    public ServiceTimeModel(String timing, boolean isChecked, boolean isDateEnabled) {
        this.timing = timing;
        this.isChecked = isChecked;
        this.isDateEnabled = isDateEnabled;

    }

    public String getTiming() {
        return timing;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isDateEnabled() {
        return isDateEnabled;
    }

    public void setDateEnabled(boolean dateEnabled) {
        isDateEnabled = dateEnabled;
    }
}
