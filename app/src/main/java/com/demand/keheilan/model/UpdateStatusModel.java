package com.demand.keheilan.model;

public class UpdateStatusModel {
    private String id;
    private String status;

    public UpdateStatusModel(String id, String status) {
        this.id = id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }
}
