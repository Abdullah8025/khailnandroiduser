package com.demand.keheilan.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demand.keheilan.R;


public class DialogPopup {
    public static ProgressDialog progressDialog;
    private static DialogPopup instance;
    static Context context;

    public synchronized static DialogPopup getInstance() {

        if (instance == null) {
            instance = new DialogPopup(context);
        }
        return instance;
    }

    public DialogPopup(Context context) {
        this.context = context;
    }


    /**
     * Displays custom loading dialog
     *
     * @param context application context
     * @param message string message to show in dialog
     */
    public void showLoadingDialog(Context context, String message) {
        try {
            if (isDialogShowing()) {
                dismissLoadingDialog();
            }

            if (context instanceof Activity) {
                Activity activity = (Activity) context;
                if (activity.isFinishing()) {
                    return;
                }

            }
            progressDialog = new ProgressDialog(context, R.style.progressDialogTheme);
            progressDialog.show();
            WindowManager.LayoutParams layoutParams = progressDialog.getWindow().getAttributes();
            layoutParams.dimAmount = 0.5f;
            //progressDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.setCancelable(false);
            progressDialog.setContentView(R.layout.dialog_loading_box);

            RelativeLayout frameLayout = (RelativeLayout) progressDialog.findViewById(R.id.dlgProgress);

            ((ProgressBar) progressDialog.findViewById(R.id.progress_wheel)).setProgress(10);
            TextView messageText = (TextView) progressDialog.findViewById(R.id.tvProgress);
            //  messageText.setText(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isDialogShowing() {
        try {
            if (progressDialog == null) {
                return false;
            } else {
                return
                        progressDialog.isShowing();
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Dismisses above loading dialog
     */
    public void dismissLoadingDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            Log.e("e", "=" + e);
        }
    }

}
