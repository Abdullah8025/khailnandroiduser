package com.demand.keheilan.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class GPSService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String TAG =GPSService.class.getSimpleName();
    public static Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 6000000;
    private static int FATEST_INTERVAL = 6000000;
    private static int DISPLACEMENT = 100000;
    private Context context;
    private boolean isSavedCalled = false;
    GetLocationUpdate mListener;

     public interface GetLocationUpdate{
        public void getLocationUpdate(Double latitude, Double longitude);
    }




    public GPSService(Context context , GetLocationUpdate listener) {
        Log.d(TAG, "GPSService: constructor called...");
        this.context = context;
        this.mListener=listener;
        buildGoogleApiClient();
        createLocationRequest();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }

    }

    /**
     * Method to display the location
     */
    private void saveLocation() {
        if (Build.VERSION.SDK_INT < 23) {
            if (mLastLocation == null) {
                try {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                } catch (SecurityException ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            } else {
                if (mLastLocation == null) {
                    try {
                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    } catch (SecurityException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        if (mLastLocation != null) {
            SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.LATITUDE, ""+mLastLocation.getLatitude());
            SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.LONGITUDE, ""+mLastLocation.getLongitude());

            Log.d(TAG, "saveLocation:  latitude is=="+mLastLocation.getLatitude()+" longitude is=="+mLastLocation.getLongitude());
            if (mListener!=null){
                mListener.getLocationUpdate(mLastLocation.getLatitude(),mLastLocation.getLongitude());
            }
        }
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

    }


    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        if (Build.VERSION.SDK_INT < 23) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (SecurityException ex) {
                ex.printStackTrace();
            }
        } else {
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            } else {
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                } catch (SecurityException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {
        //  saveLocation();
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (!isSavedCalled) {
            isSavedCalled = true;
            saveLocation();
        }

        stopLocationUpdates();
    }

}
