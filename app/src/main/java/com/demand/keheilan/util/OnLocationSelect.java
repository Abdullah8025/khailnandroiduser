package com.demand.keheilan.util;

public interface OnLocationSelect {
  void onLocationSelect(String lat, String longi, String city, String state, String country, String postal);
}
