package com.demand.keheilan.util;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.demand.keheilan.R;
import com.demand.keheilan.activity.ChooseLanguageActivity;
import com.demand.keheilan.activity.HomeActivity;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class CommonUtil {
    private static final String TAG = "CommonUtil";
    private static LogoutUser mLogoutUser;
    public interface OnOptionsSheetItemSelected{

    }

    public interface LogoutUser {
        void logoutAndFinish();
    }

    public static void showSnackBar(Context context, String msg) {
        Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setMinimumHeight(10);
        snackBarView.setBackgroundColor(Color.parseColor("#FF000000"));
        TextView tv = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        tv.setTextSize(13);
        tv.setTextColor(Color.parseColor("#FFFFFF"));
        snackbar.show();
    }

    public static void hideKeyBoard(Context context, View view) {
        try {
            InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean blankValidation(String str) {
        if (str != null && str.trim().length() > 0)
            return true;
        else
            return false;
    }

    public static boolean checkGPS(Context context) {
        try {
            if (context != null) {
                LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (!gps_enabled && !network_enabled) {
                    return false;
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static ArrayList<String> getPermissionList() {
        ArrayList<String> myPermissions = new ArrayList<>();
        myPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        myPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        myPermissions.add(Manifest.permission.CAMERA);
        return myPermissions;
    }

    public Intent getPickIntent(Context context, Uri cameraOutputUri) {
        final List<Intent> intents = new ArrayList<Intent>();

        if (true) {
//            intents.add(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI));
//            Intent(Intent.ACTION_GET_CONTENT).also { intent ->
//                    intent.type = "image/*"
            Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            galleryIntent.setType("image/*");
            intents.add(galleryIntent);

        }

        if (true) {
            // setCameraIntents(context, intents, cameraOutputUri);
            dispatchTakePictureIntent(context, intents, cameraOutputUri);
        }

        if (intents.isEmpty()) return null;
        Intent result = Intent.createChooser(intents.remove(0), null);
        if (!intents.isEmpty()) {
            result.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(new Parcelable[]{}));
        }
        return result;


    }

    public void setCameraIntents(Context context, List<Intent> cameraIntents, Uri output) {
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
            intent.putExtra("uri", output);
            cameraIntents.add(intent);
        }
    }

    private void dispatchTakePictureIntent(Context context, List<Intent> cameraIntents, Uri output) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {

            // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, output);

            final List<ResolveInfo> listCam = context.getPackageManager().queryIntentActivities(takePictureIntent, 0);
            for (ResolveInfo res : listCam) {
                final String packageName = res.activityInfo.packageName;
                final Intent intent = new Intent(takePictureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                intent.putExtra("uri", output);
                intent.putExtra(MyAppConstants.CAMERA_INTENT,"isCamera");
                cameraIntents.add(intent);
            }

        }
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static Bitmap setImageOrientation(Bitmap bitmap, String currentImagePath) {
        if (bitmap != null) {
            try {
                ExifInterface ei = null;
                ei = new ExifInterface(currentImagePath);
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                switch (orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotateImage(bitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        bitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:

                    default:
                        break;
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return bitmap;
        } else throw new IllegalArgumentException("BITMAP PASSED WAS NULL...");
    }

    public static void setSpinner(Context context, List<String> data, Spinner spinner) {
        Log.d(TAG, "setSpinner: Called===");
        ArrayAdapter myArrayAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, data) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0)
                    return false;
                else
                    return true;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                // return super.getDropDownView(position, convertView, parent);
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(ContextCompat.getColor(context, R.color.color_light_grey));
                } else {
                    tv.setTextColor(ContextCompat.getColor(context, R.color.black));
                }
                return view;
            }

        };
        spinner.setAdapter(myArrayAdapter);
        spinner.performClick();
    }

    public static void showLoginAlert(Context context, LogoutUser logoutListener) {
        Dialog logoutDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logoutDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        logoutDialog.setContentView(R.layout.layout_login_alert);
        logoutDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        logoutDialog.setCanceledOnTouchOutside(true);

        Button btn_cancel = logoutDialog.findViewById(R.id.btn_cancel);
        Button btn_login = logoutDialog.findViewById(R.id.btn_login);

        View.OnClickListener dialogClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                if (id == R.id.btn_login) {
                    logoutDialog.dismiss();
                    clearSavedData(context, logoutListener);
                } else if (id == R.id.btn_cancel) {
                    logoutDialog.dismiss();
                }
            }
        };

        btn_cancel.setOnClickListener(dialogClickListener);
        btn_login.setOnClickListener(dialogClickListener);

        logoutDialog.show();
    }

    public static void clearSavedData(Context context, LogoutUser logoutUser) {
        SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.USER_ID, "");
        SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.TOKEN, "");
        SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.FULL_NAME, "");
        SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.PHONE_NUMBER, "");
        SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.COUNTRY_CODE, "");
        SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.PROFILE_PIC, "");
        SharedPreferenceWriter.getInstance(context).writeStringValue(SPreferenceKey.USER_HOME_ADDRESS, "");
        SharedPreferenceWriter.getInstance(context).writeBooleanValue(SPreferenceKey.IS_USER_LOGIN, false);

        mLogoutUser = (LogoutUser) logoutUser;

        if (mLogoutUser != null)
            mLogoutUser.logoutAndFinish();
    }

    public static boolean isUserLoggedIn(Context context) {
        if (SharedPreferenceWriter.getInstance(context).getBoolean(SPreferenceKey.IS_GUEST_USER)) {
            return false;
        }
        return true;
    }

    public static void startActivity(Context context, Class<?> className) {
        Intent intent = new Intent(context, className);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

}
