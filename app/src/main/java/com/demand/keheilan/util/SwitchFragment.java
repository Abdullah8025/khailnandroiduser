package com.demand.keheilan.util;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.demand.keheilan.R;

public class SwitchFragment {
    private static final String TAG = "SwitchFragment";
    private static SetHeader mSetHeaderListener;

    public interface SetHeader {
        public void setHeader(String fragmentTag);
    }

    public static void changeFragment(FragmentManager fragmentManager, Fragment fragment, String fragmentTag, boolean saveInBackstack, boolean animate) {
        String backStateName = fragment.getClass().getName();
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            if (animate) {
                transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_left);
            }
            transaction.replace(R.id.replaceFrame, fragment, fragmentTag);
            if (saveInBackstack) {
                transaction.addToBackStack(backStateName);
            }

            transaction.commit();
            //transaction.commitAllowingStateLoss();

        } catch (IllegalStateException e) {
            Log.e("e", e.toString());
        }
    }

    public static void replaceFragment(FragmentManager fragmentManager, Fragment fragment, String fragmentTag, boolean saveInBackStack, boolean animate, SetHeader listener) {
        String backStateName = fragment.getClass().getName();
        if (listener != null) {
            SwitchFragment.mSetHeaderListener = listener;
            try {
                //Log.d(TAG, "replaceFragment: back stack entry count is BEFORE popBackStackimmediate==="+fragmentManager.getBackStackEntryCount());
                boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
                //  Log.d(TAG, "replaceFragment: back stack entry count is AFTER popBackStackimmediate==="+fragmentManager.getBackStackEntryCount());
                if (!fragmentPopped) {
                    //  Log.d(TAG, "replaceFragment: not found in backstack");
                    SwitchFragment.changeFragment(fragmentManager, fragment, fragmentTag, saveInBackStack, animate);
                    mSetHeaderListener.setHeader(fragmentTag);
                } else {
                    Log.d(TAG, "replaceFragment: found in backstack");
                    mSetHeaderListener.setHeader(fragmentTag);
                }
            } catch (IllegalStateException e) {
                Log.e("e", e.toString());
            }
        } else throw new IllegalArgumentException("SetHeader context found null");

    }


}
