package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.ViewPagerAdapter;
import com.demand.keheilan.fragment.OnBoardingOneFragment;
import com.demand.keheilan.fragment.OnBoardingSecondFragment;
import com.demand.keheilan.fragment.OnBoardingThirdFragment;

import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.ON_BOARDING_THIRD;

public class OnBoardingSlidePagerActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "OnBoardingSlidePagerAct";
    private ViewPager pager;
    private TextView  tv_tab2, tv_tab3, tv_skip;
    Button btn_get_started;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding_slide_pager);
        initView();
        addTabs(pager);
    }


    private void initView() {
        pager = findViewById(R.id.pager);
//        tv_tab1 = findViewById(R.id.tv_tab1);
        tv_tab2 = findViewById(R.id.tv_tab2);
        tv_tab3 = findViewById(R.id.tv_tab3);
        tv_skip = findViewById(R.id.tv_skip);
        btn_get_started = findViewById(R.id.btn_get_started);

        tv_skip.setOnClickListener(this);
        btn_get_started.setOnClickListener(this);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedPage(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });

    }

    private void selectedPage(int position) {
//        if (position == 0) {
//            tv_tab1.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_yellow_round_corner));
//            tv_tab2.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_light_primary_round_corner));
//            tv_tab3.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_light_primary_round_corner));
//            tv_skip.setVisibility(View.VISIBLE);
//            btn_get_started.setVisibility(View.GONE);
//        }
        if (position == 0) {
           // tv_tab1.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_light_primary_round_corner));
            tv_tab2.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_yellow_round_corner));
            tv_tab3.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_light_primary_round_corner));
            tv_skip.setVisibility(View.VISIBLE);
            btn_get_started.setVisibility(View.GONE);
        }
        if (position == 1) {
          //  tv_tab1.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_light_primary_round_corner));
            tv_tab2.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_light_primary_round_corner));
            tv_tab3.setBackground(ContextCompat.getDrawable(OnBoardingSlidePagerActivity.this, R.drawable.bg_yellow_round_corner));
            tv_skip.setVisibility(View.GONE);
            btn_get_started.setVisibility(View.VISIBLE);
        }
    }

    private void addTabs(ViewPager pager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
       // viewPagerAdapter.addFragment(new OnBoardingOneFragment());
        viewPagerAdapter.addFragment(new OnBoardingSecondFragment());
        viewPagerAdapter.addFragment(new OnBoardingThirdFragment());
        pager.setAdapter(viewPagerAdapter);

    }

    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_skip) {
            pager.setCurrentItem(1);
        } else if (id == R.id.btn_get_started) {
            Intent intent = new Intent(OnBoardingSlidePagerActivity.this, ChooseLanguageActivity.class);
            intent.putExtra(CAME_FROM_SCREEN, ON_BOARDING_THIRD);
            startActivity(intent);
        }
    }
}