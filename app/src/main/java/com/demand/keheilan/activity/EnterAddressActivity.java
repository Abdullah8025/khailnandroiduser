package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.demand.keheilan.R;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.GPSService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.demand.keheilan.util.MyAppConstants.BOOKING_PLACE_CITY;
import static com.demand.keheilan.util.MyAppConstants.DEFAULT_ADDRESS;
import static com.demand.keheilan.util.MyAppConstants.SELECTED_ADDRESS;

public class EnterAddressActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, GPSService.GetLocationUpdate {
    private static final String TAG = "EnterAddressActivity";
    private EditText edt_completeAddress, edt_building, edt_city, edt_landmark;
    private CheckBox checkbox_defaultAddress;
    private Button btn_save;
    private ImageView iv_back;
    private TextView tv_searchAddress;

    private String mAddress, mLatitude, mLongitude,mCityName;
    private GoogleMap mMap;
    Geocoder geocoder;
    private GetAddress getAddress;
    Marker marker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_address);
        initView();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    private void initView() {
        edt_completeAddress = findViewById(R.id.edt_completeAddress);
        edt_building = findViewById(R.id.edt_building);
        edt_city = findViewById(R.id.edt_city);
        edt_landmark = findViewById(R.id.edt_landmark);
        checkbox_defaultAddress = findViewById(R.id.checkbox_defaultAddress);
        btn_save = findViewById(R.id.btn_save);
        iv_back = findViewById(R.id.iv_back);
        tv_searchAddress = findViewById(R.id.tv_searchAddress);

        btn_save.setOnClickListener(this);
        iv_back.setOnClickListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        geocoder = new Geocoder(EnterAddressActivity.this, Locale.getDefault());

        mLatitude = SharedPreferenceWriter.getInstance(EnterAddressActivity.this).getString(SPreferenceKey.LATITUDE);
        mLongitude = SharedPreferenceWriter.getInstance(EnterAddressActivity.this).getString(SPreferenceKey.LONGITUDE);
        LatLng currentLocation = new LatLng(Double.parseDouble(mLatitude), Double.parseDouble(mLongitude));

        if (mLatitude != null && mLongitude != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15.0f));
            marker = mMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.address_pointer)));
            getAddress = new GetAddress();
            getAddress.execute();
        } else {
            new GPSService(EnterAddressActivity.this, this);
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_save) {
            if (validateAddress()) {
                makeCompleteAddress();
                setResultAndFinish(true);
            }
        } else if (id == R.id.iv_back) {
            setResultAndFinish(false);
        }
    }

    private void makeCompleteAddress() {
        String completeAddress = edt_completeAddress.getText().toString().trim();
        String building = edt_building.getText().toString().trim();
        String city = edt_city.getText().toString().trim();
        String landmark = edt_landmark.getText().toString();
        mCityName =city;

//        mAddress = completeAddress + ", " + "\n"
//                + "Building- " + building + ", " + "\n"
//                + "City- " + city + ", " + "\n"
//                + "Landmark- " + landmark;

        mAddress = completeAddress + ", " + "Building- " + building + ", " + "City- " + city + ", " + "Landmark- " + landmark;

    }

    private void setResultAndFinish(boolean saveAddress) {
        Intent returnIntent = new Intent();
        if (saveAddress) {
            returnIntent.putExtra(DEFAULT_ADDRESS, checkbox_defaultAddress.isChecked());
            returnIntent.putExtra(SELECTED_ADDRESS, mAddress);
            returnIntent.putExtra(BOOKING_PLACE_CITY,mCityName);
            setResult(RESULT_OK, returnIntent);
        } else {
            setResult(RESULT_CANCELED,returnIntent);
           // returnIntent.putExtra(SELECTED_ADDRESS, "");
        }

        finish();
    }

    private boolean validateAddress() {
        if (!CommonUtil.blankValidation(edt_completeAddress.getText().toString())) {
            CommonUtil.showSnackBar(EnterAddressActivity.this, getString(R.string.validate_complete_address));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_building.getText().toString())) {
            CommonUtil.showSnackBar(EnterAddressActivity.this, getString(R.string.validate_appartment));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_city.getText().toString())) {
            CommonUtil.showSnackBar(EnterAddressActivity.this, getString(R.string.validate_city));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_landmark.getText().toString())) {
            CommonUtil.showSnackBar(EnterAddressActivity.this, getString(R.string.validate_landmark));
            return false;
        }

        return true;
    }

    private class GetAddress extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "onPostExecute: SELECTED ADDRESS = " + s);
            tv_searchAddress.setText(s);
        }

        @Override
        protected String doInBackground(Void... voids) {
            String selectedAddress = "";

            try {
                List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(mLatitude), Double.parseDouble(mLongitude), 1);
                if (addresses != null && addresses.size() > 0) {
                    selectedAddress = addresses.get(0).getAddressLine(0);
                } else {
                    selectedAddress = "";
                }


            } catch (IOException e) {
                e.printStackTrace();
                if (e.getMessage().equalsIgnoreCase("grpc failed")) {
                    CommonUtil.showSnackBar(EnterAddressActivity.this, getString(R.string.enable_gps));
                }


            }
            return selectedAddress;
        }
    }

    @Override
    public void getLocationUpdate(Double latitude, Double longitude) {
        Log.d(TAG, "getLocationUpdate: called...");
        mLatitude = String.valueOf(latitude);
        mLongitude = String.valueOf(longitude);
        LatLng currentLocation = new LatLng(Double.parseDouble(mLatitude), Double.parseDouble(mLongitude));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15.0f));
        marker = mMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.address_pointer)));
        getAddress = new GetAddress();
        getAddress.execute();
    }
}