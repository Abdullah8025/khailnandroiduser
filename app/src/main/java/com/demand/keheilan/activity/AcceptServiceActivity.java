package com.demand.keheilan.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.model.UpdateStatusModel;
import com.demand.keheilan.responsemodel.BookedServices;
import com.demand.keheilan.responsemodel.BookedServicesResponse;
import com.demand.keheilan.responsemodel.BookingDetailResponse;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.CoupenListResponse;
import com.demand.keheilan.responsemodel.CustomerTaxResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.BASE_PRICE;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_ID;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_STATUS_COMPLETED;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_STATUS_IN_PROCESS;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_STATUS_PENDING;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.HELP_TAB;
import static com.demand.keheilan.util.MyAppConstants.HELP_WITH_ORDER_TAB;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_DETAIL;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_ITEM;
import static com.demand.keheilan.util.MyAppConstants.PAST_DETAIL;
import static com.demand.keheilan.util.MyAppConstants.PAST_ITEM;
import static com.demand.keheilan.util.MyAppConstants.PROMO_APPLY;
import static com.demand.keheilan.util.MyAppConstants.PROMO_REMOVE;
import static com.demand.keheilan.util.MyAppConstants.RESULT;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ACCEPTED;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_REJECTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_ACCEPTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_REJECTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;
import static com.demand.keheilan.util.MyAppConstants.TOTAL_PRICE;
import static com.demand.keheilan.util.MyAppConstants.UPCOMING_DETAIL;
import static com.demand.keheilan.util.MyAppConstants.UPCOMING_ITEM;

public class AcceptServiceActivity extends AppCompatActivity implements View.OnClickListener, CommonUtil.LogoutUser {
    private static final String TAG = "AcceptServiceActivity";
    private ImageView iv_back, iv_servicePic;
    private Button btn_accept;
    private TextView tv_reject, tv_serviceName, tv_time, tv_location, tv_date, tv_priceTitle, tv_priceTop, tv_bookingId, tv_status, tv_promoName, tv_Apply,
            tv_subTotal, tv_Fees, tv_discount, tv_payAmount, tv_offeredPrice, tv_needHelp, tv_additionalFees, tv_customerTax,tv_topCustomerTax;
    private ConstraintLayout const_accept, const_needHelp, const_promo, const_finalAmount, const_offeredPrice, const_additionalFees,const_topCustomerTax;

    private String mBookingId, mBasePrice, mCouponId;
    private ArrayList<String> promoCodesNames;
    private ArrayList<CoupenListResponse.Coupen> mCouponsList;
    public static final int REQUEST_CODE_COUPON = 8;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_service);
        initView();
        promoCodesNames = new ArrayList<>();
        mCouponsList = new ArrayList<>();
        Intent extraData = getIntent();
        if (extraData != null) {
            if (extraData.hasExtra(CAME_FROM_SCREEN)) {
                Log.d(TAG, "onCreate: CAME_FROM=> " + extraData.getStringExtra(CAME_FROM_SCREEN));
                if (extraData.getStringExtra(CAME_FROM_SCREEN).equals(IN_PROCESS_ITEM)) {
                    mBookingId = extraData.getStringExtra(BOOKING_ID);
                    mBasePrice = extraData.getStringExtra(BASE_PRICE);
                    Log.d(TAG, "onCreate: INPROCESS BOOKING ID = " + mBookingId + " BASE PRICE = " + mBasePrice);
                    if (CommonUtil.blankValidation(mBookingId)) {
                        serviceBookingDetail(mBookingId, IN_PROCESS_DETAIL);
                    }
                }
                if (extraData.getStringExtra(CAME_FROM_SCREEN).equals(UPCOMING_ITEM)) {
                    mBookingId = extraData.getStringExtra(BOOKING_ID);
                    mBasePrice = extraData.getStringExtra(BASE_PRICE);
                    Log.d(TAG, "onCreate:UPCOMING BOOKING ID = " + mBookingId + " BASE PRICE = " + mBasePrice);
                    if (CommonUtil.blankValidation(mBookingId)) {
                        serviceBookingDetail(mBookingId, UPCOMING_DETAIL);
                    }
                }
                if (extraData.getStringExtra(CAME_FROM_SCREEN).equals(PAST_ITEM)) {
                    mBookingId = extraData.getStringExtra(BOOKING_ID);
                    mBasePrice = extraData.getStringExtra(TOTAL_PRICE);
                    Log.d(TAG, "onCreate:PAST BOOKING ID = " + mBookingId + " BASE PRICE = " + mBasePrice);
                    if (CommonUtil.blankValidation(mBookingId)) {
                        serviceBookingDetail(mBookingId, PAST_DETAIL);
                    }
                }
            }
        }
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        btn_accept = findViewById(R.id.btn_accept);
        tv_reject = findViewById(R.id.tv_reject);
        const_accept = findViewById(R.id.const_accept);
        const_needHelp = findViewById(R.id.const_needHelp);
        const_promo = findViewById(R.id.const_promo);
        iv_servicePic = findViewById(R.id.iv_servicePic);
        tv_serviceName = findViewById(R.id.tv_serviceName);
        tv_time = findViewById(R.id.tv_time);
        tv_location = findViewById(R.id.tv_location);
        tv_date = findViewById(R.id.tv_date);
        tv_priceTitle = findViewById(R.id.tv_priceTitle);
        tv_priceTop = findViewById(R.id.tv_priceTop);
        tv_bookingId = findViewById(R.id.tv_bookingId);
        tv_status = findViewById(R.id.tv_status);
        tv_promoName = findViewById(R.id.tv_promoName);
        tv_Apply = findViewById(R.id.tv_Apply);
        const_finalAmount = findViewById(R.id.const_finalAmount);
        tv_subTotal = findViewById(R.id.tv_subTotal);
        tv_Fees = findViewById(R.id.tv_Fees);
        tv_discount = findViewById(R.id.tv_discount);
        tv_payAmount = findViewById(R.id.tv_payAmount);
        tv_offeredPrice = findViewById(R.id.tv_offeredPrice);
        const_offeredPrice = findViewById(R.id.const_offeredPrice);
        tv_needHelp = findViewById(R.id.tv_needHelp);
        const_additionalFees = findViewById(R.id.const_additionalFees);
        tv_additionalFees = findViewById(R.id.tv_additionalFees);
        tv_customerTax = findViewById(R.id.tv_customerTax);
        const_topCustomerTax =findViewById(R.id.const_topCustomerTax);
        tv_topCustomerTax =findViewById(R.id.tv_topCustomerTax);

        iv_back.setOnClickListener(this);
        btn_accept.setOnClickListener(this);
        tv_reject.setOnClickListener(this);
        tv_Apply.setOnClickListener(this);
        tv_needHelp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.tv_reject) {
            serviceUpdateServiceStatus(mBookingId, STATUS_REJECTED);
        } else if (id == R.id.btn_accept) {
            serviceUpdateServiceStatus(mBookingId, STATUS_ACCEPTED);
        } else if (id == R.id.tv_Apply) {
            if (tv_Apply.getText().toString().equalsIgnoreCase(PROMO_REMOVE)) {
                serviceApplyRemoveCoupon(PROMO_REMOVE, null);
            } else {
                openCouponListScreen();
            }
        } else if (id == R.id.tv_needHelp) {
            openHelpScreen();
        }
    }

    private void openCouponListScreen() {
        Intent intent = new Intent(AcceptServiceActivity.this, CoupenListActivity.class);
        startActivityForResult(intent, REQUEST_CODE_COUPON);
    }

    private void openHelpScreen() {
        Intent intent = new Intent(AcceptServiceActivity.this, GeneralInquiry.class);
        intent.putExtra(HELP_TAB, HELP_WITH_ORDER_TAB);
        startActivity(intent);
    }


    private void serviceBookingDetail(String bookingId, String bookingDetailType) {
        {
            if (new InternetCheck(AcceptServiceActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(AcceptServiceActivity.this);
                dialogPopup.showLoadingDialog(AcceptServiceActivity.this, "");
                String token = SharedPreferenceWriter.getInstance(AcceptServiceActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<BookingDetailResponse> call = api_service.bookingDetail(TOKEN_KEY + token, bookingId);
                call.enqueue(new Callback<BookingDetailResponse>() {
                    @Override
                    public void onResponse(Call<BookingDetailResponse> call, Response<BookingDetailResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(AcceptServiceActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(AcceptServiceActivity.this, AcceptServiceActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                BookingDetailResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();

                                    setBookingData(server_response, bookingDetailType);

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<BookingDetailResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceCustomerTax() {
        {
            if (new InternetCheck(AcceptServiceActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(AcceptServiceActivity.this);
                dialogPopup.showLoadingDialog(AcceptServiceActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CustomerTaxResponse> call = api_service.getCustomerTax();
                call.enqueue(new Callback<CustomerTaxResponse>() {
                    @Override
                    public void onResponse(Call<CustomerTaxResponse> call, Response<CustomerTaxResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(AcceptServiceActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(AcceptServiceActivity.this, AcceptServiceActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CustomerTaxResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();

                                    if (CommonUtil.blankValidation(server_response.getData())){
                                        tv_customerTax.setText(getString(R.string.num_amount, server_response.getData()));
                                        tv_topCustomerTax.setText(getString(R.string.num_amount, server_response.getData()));
                                    }



                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CustomerTaxResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void setBookingData(BookingDetailResponse server_response, String detailType) {
        Glide.with(AcceptServiceActivity.this)
                .load(server_response.getImage_1())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(iv_servicePic);

        tv_serviceName.setText(server_response.getService_name());
        tv_time.setText(server_response.getBooking_time());
        tv_location.setText(server_response.getAddress());
        tv_date.setText(server_response.getBooking_date());
        tv_bookingId.setText("#" + server_response.getId());
        tv_priceTop.setText(getString(R.string.num_amount, mBasePrice));
        tv_offeredPrice.setText(getString(R.string.num_amount, String.valueOf(server_response.getQuote())));
        tv_additionalFees.setText(getString(R.string.num_amount, server_response.getAdditional_fees()));
        if (detailType.equals(UPCOMING_DETAIL)) {
            tv_status.setText(getString(R.string.booking_status_pending));
            const_needHelp.setVisibility(View.GONE);
            if (server_response.getQuote() > 0.0) {
                Log.d(TAG, "setBookingData: OFFERED PRICE FOUND");
                const_finalAmount.setVisibility(View.VISIBLE);
                const_accept.setVisibility(View.VISIBLE);
                const_offeredPrice.setVisibility(View.VISIBLE);
                const_promo.setVisibility(View.VISIBLE);
                const_additionalFees.setVisibility(View.GONE);
                const_topCustomerTax.setVisibility(View.GONE);
                tv_promoName.setText(server_response.getPromocode_name());

                if (server_response.isPromocode_status()) {
                    tv_Apply.setText(getString(R.string.remove));
                } else {
                    tv_Apply.setText(getString(R.string.apply));
                }
            } else {
                Log.d(TAG, "setBookingData: OFFERED PRICE NOT FOUND");
                const_finalAmount.setVisibility(View.GONE);
                const_accept.setVisibility(View.GONE);
                const_offeredPrice.setVisibility(View.GONE);
                const_promo.setVisibility(View.GONE);
                const_topCustomerTax.setVisibility(View.VISIBLE);

            }

        }
        if (detailType.equals(IN_PROCESS_DETAIL)) {
            tv_status.setText(getString(R.string.booking_status_accepted));
            const_finalAmount.setVisibility(View.VISIBLE);
            const_promo.setVisibility(View.GONE);
            const_needHelp.setVisibility(View.VISIBLE);
            const_accept.setVisibility(View.GONE);
            const_offeredPrice.setVisibility(View.VISIBLE);
            const_additionalFees.setVisibility(View.GONE);
            const_topCustomerTax.setVisibility(View.GONE);
        }
        if (detailType.equals(PAST_DETAIL)) {
            tv_status.setText(getString(R.string.booking_status_completed));
            const_finalAmount.setVisibility(View.VISIBLE);
            const_promo.setVisibility(View.GONE);
            const_needHelp.setVisibility(View.VISIBLE);
            const_accept.setVisibility(View.GONE);
            const_offeredPrice.setVisibility(View.VISIBLE);
            const_additionalFees.setVisibility(View.VISIBLE);
            const_topCustomerTax.setVisibility(View.GONE);
        }

        if (CommonUtil.blankValidation(server_response.getSub_total()))
            tv_subTotal.setText(getString(R.string.num_amount, server_response.getSub_total()));
        if (CommonUtil.blankValidation(server_response.getFees()))
            tv_Fees.setText(getString(R.string.num_amount, server_response.getFees()));
        if (CommonUtil.blankValidation(server_response.getDiscount()))
            tv_discount.setText(getString(R.string.num_amount, server_response.getDiscount()));
        if (CommonUtil.blankValidation(server_response.getTotal()))
            tv_payAmount.setText(getString(R.string.num_amount, server_response.getTotal()));

        serviceCustomerTax();
    }

    private void serviceUpdateServiceStatus(String bookingId, String status) {
        {
            if (new InternetCheck(AcceptServiceActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(AcceptServiceActivity.this);
                dialogPopup.showLoadingDialog(AcceptServiceActivity.this, "");
                String token = SharedPreferenceWriter.getInstance(AcceptServiceActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = api_service.updateServiceStatus(TOKEN_KEY + token, bookingId, status);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(AcceptServiceActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(AcceptServiceActivity.this, AcceptServiceActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    if (status.equals(STATUS_ACCEPTED))
                                        setResultAndFinish(SERVICE_ACCEPTED);
                                    if (status.equals(STATUS_REJECTED))
                                        setResultAndFinish(SERVICE_REJECTED);
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_COUPON) {
            if (resultCode == RESULT_OK) {
                String couponId = data.getStringExtra(RESULT);
                Log.d(TAG, "onActivityResult: COUPON ID = " + data.getStringExtra(RESULT));
                serviceApplyRemoveCoupon(PROMO_APPLY, couponId);
            }
        }

    }

    private void serviceApplyRemoveCoupon(String couponStatus, String couponId) {
        {
            if (new InternetCheck(AcceptServiceActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(AcceptServiceActivity.this);
                dialogPopup.showLoadingDialog(AcceptServiceActivity.this, "");
                String token = SharedPreferenceWriter.getInstance(AcceptServiceActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = null;
                if (couponStatus.equalsIgnoreCase(PROMO_APPLY))
                    call = api_service.applyCoupon(TOKEN_KEY + token, couponId, mBookingId);
                else if (couponStatus.equalsIgnoreCase(PROMO_REMOVE))
                    call = api_service.removeCoupon(TOKEN_KEY + token, mBookingId);
                else
                    throw new IllegalArgumentException(TAG + "Only Apply or Remove Coupon Applicable");
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(AcceptServiceActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(AcceptServiceActivity.this, AcceptServiceActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    serviceBookingDetail(mBookingId, UPCOMING_DETAIL);
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(AcceptServiceActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void setCoupenData() {
        if (promoCodesNames.size() > 1) {
            const_promo.setVisibility(View.VISIBLE);
        } else {
            const_promo.setVisibility(View.GONE);
        }
    }

    private void setResultAndFinish(String serviceStatus) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(RESULT, serviceStatus);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called....");
        Intent intent = new Intent(AcceptServiceActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}