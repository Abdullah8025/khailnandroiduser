package com.demand.keheilan.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.LoginResponse;
import com.demand.keheilan.responsemodel.SignUpResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.GPSService;
import com.demand.keheilan.util.InternetCheck;
import com.demand.keheilan.util.LocaleHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.DEVICE_TYPE;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ARABIC;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ENGLISH;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;

public class LoginProfileActivity extends AppCompatActivity implements View.OnClickListener, GPSService.GetLocationUpdate {
    private static final String TAG = "LoginProfileActivity";
    private ProgressBar progressBar_profile, progressBar_login;
    private ImageView imageView5;
    private TextView tv_profile;
    private Button btn_continue;
    private EditText edt_fullName, edt_referralCode;
    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            progressBar_profile.setProgress((Integer) msg.obj);
            if (100 == (Integer) msg.obj) {
                imageView5.setImageDrawable(ContextCompat.getDrawable(LoginProfileActivity.this, R.drawable.bg_circle_yellow));
                tv_profile.setTextColor(ContextCompat.getColor(LoginProfileActivity.this, R.color.black));

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_profile);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: SIGNUP MODEL " + SignUpModel.getInstance().toString());
        showProgress();
    }

    private void showProgress() {
        Thread progressThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= 100; i++) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Message message = Message.obtain();
                    message.obj = i;
                    mHandler.sendMessage(message);
                }
            }
        });
        progressThread.start();

    }

    private void initView() {
        progressBar_profile = findViewById(R.id.progressBar_profile);
        progressBar_login = findViewById(R.id.progressBar_login);
        imageView5 = findViewById(R.id.imageView5);
        tv_profile = findViewById(R.id.tv_profile);
        btn_continue = findViewById(R.id.btn_continue);
        edt_fullName = findViewById(R.id.edt_fullName);
        edt_referralCode = findViewById(R.id.edt_referralCode);


        btn_continue.setOnClickListener(this);
        progressBar_login.setProgress(100);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_continue) {
            if (validateData()) {
                String latitude = SharedPreferenceWriter.getInstance(LoginProfileActivity.this).getString(SPreferenceKey.LATITUDE);
                String longitude = SharedPreferenceWriter.getInstance(LoginProfileActivity.this).getString(SPreferenceKey.LONGITUDE);
                if (latitude != null && longitude != null) {
                    serviceSignUp(latitude, longitude);
                } else {
                    new GPSService(LoginProfileActivity.this, this);
                }
            }
        }
    }

    private boolean validateData() {
        if (!CommonUtil.blankValidation(edt_fullName.getText().toString())) {
            CommonUtil.showSnackBar(LoginProfileActivity.this, getString(R.string.validate_full_name));
            return false;
        }
        return true;
    }

    private void serviceSignUp(String latitude, String longitude) {
        {
            if (new InternetCheck(this).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(LoginProfileActivity.this);
                dialogPopup.showLoadingDialog(LoginProfileActivity.this, "");
                String deviceToken = SharedPreferenceWriter.getInstance(LoginProfileActivity.this).getString(SPreferenceKey.DEVICE_TOKEN);
                String langCode = SharedPreferenceWriter.getInstance(LoginProfileActivity.this).getString(SPreferenceKey.LANG_CODE);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SignUpResponse> call = api_service.signUp(edt_fullName.getText().toString().trim(), SignUpModel.getInstance().getCountryCode(),
                        SignUpModel.getInstance().getPhoneNumber(), edt_referralCode.getText().toString().trim(), deviceToken,
                        langCode, DEVICE_TYPE, latitude, longitude);
                call.enqueue(new Callback<SignUpResponse>() {
                    @Override
                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                        if (response.isSuccessful()) {
                            SignUpResponse server_response = response.body();
                            if (server_response.getStatus() == STATUS_SUCCESS) {
                                dialogPopup.dismissLoadingDialog();
                                SignUpModel.nullModel();
                                startActivity(new Intent(LoginProfileActivity.this, HomeActivity.class));
                                saveData(server_response);

                            } else if (server_response.getStatus() == STATUS_FAILURE) {
                                dialogPopup.dismissLoadingDialog();
                                Log.d(TAG, "onResponse: " + server_response.getMessage());
                                CommonUtil.showSnackBar(LoginProfileActivity.this,server_response.getMessage());
                            } else {
                                dialogPopup.dismissLoadingDialog();
                                Toast.makeText(LoginProfileActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(LoginProfileActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(LoginProfileActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void saveData(SignUpResponse server_response) {
        SharedPreferenceWriter.getInstance(LoginProfileActivity.this).writeStringValue(SPreferenceKey.USER_ID, server_response.getId());
        SharedPreferenceWriter.getInstance(LoginProfileActivity.this).writeStringValue(SPreferenceKey.FULL_NAME, server_response.getFull_name());
        SharedPreferenceWriter.getInstance(LoginProfileActivity.this).writeStringValue(SPreferenceKey.TOKEN, server_response.getToken());
        SharedPreferenceWriter.getInstance(LoginProfileActivity.this).writeStringValue(SPreferenceKey.COUNTRY_CODE, server_response.getCountry_code());
        SharedPreferenceWriter.getInstance(LoginProfileActivity.this).writeStringValue(SPreferenceKey.PHONE_NUMBER, server_response.getPhone_number());
        SharedPreferenceWriter.getInstance(LoginProfileActivity.this).writeBooleanValue(SPreferenceKey.IS_USER_LOGIN, true);
        SharedPreferenceWriter.getInstance(LoginProfileActivity.this).writeBooleanValue(SPreferenceKey.IS_GUEST_USER, false);

    }


    @Override
    public void getLocationUpdate(Double latitude, Double longitude) {
        Log.d(TAG, "getLocationUpdate: called..");
        if (latitude != null && longitude != null)
            serviceSignUp(String.valueOf(latitude), String.valueOf(longitude));
    }
}