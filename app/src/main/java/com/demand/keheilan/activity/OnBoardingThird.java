package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;

import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.ON_BOARDING_THIRD;

public class OnBoardingThird extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "OnBoardingThird";
    private Button btn_get_started;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding_third);
        initView();
    }

    private void initView() {
        btn_get_started = findViewById(R.id.btn_get_started);
        btn_get_started.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_get_started) {
            Intent intent = new Intent(OnBoardingThird.this, ChooseLanguageActivity.class);
            intent.putExtra(CAME_FROM_SCREEN, ON_BOARDING_THIRD);
            startActivity(intent);
        }

    }
}