package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.ReviewAdapter;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.ReviewBean;
import com.demand.keheilan.responsemodel.ReviewsResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.SERVICE_ACCEPTED;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ID;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_REJECTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_ACCEPTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_REJECTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class ReviewRatingActivity extends AppCompatActivity implements View.OnClickListener,CommonUtil.LogoutUser{
    private static final String TAG = "ReviewRatingActivity";
    private RecyclerView rv_review;
    private ImageView iv_back;
    private TextView tv_title, tv_noData;

    private List<ReviewBean> mReviewList;
    private String mServiceId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_rating);
        initView();
        tv_title.setText(getString(R.string.review_rating));
        Intent extraData = getIntent();
        if (extraData != null) {
            mServiceId = extraData.getStringExtra(SERVICE_ID);
            Log.d(TAG, "onCreate: SERVICE ID = " + mServiceId);
        }

        mReviewList = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonUtil.blankValidation(mServiceId))
            serviceReviews(mServiceId);
        else
            CommonUtil.showSnackBar(ReviewRatingActivity.this, getString(R.string.validate_service_id));
    }

    private void setReviewAdapter() {
        rv_review.setLayoutManager(new LinearLayoutManager(this));
        rv_review.setAdapter(new ReviewAdapter(this, mReviewList));
    }

    private void initView() {
        rv_review = findViewById(R.id.rv_review);
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_noData = findViewById(R.id.tv_noData);

        iv_back.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        }
    }

    private void serviceReviews(String serviceId) {
        {
            if (new InternetCheck(ReviewRatingActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(ReviewRatingActivity.this);
                dialogPopup.showLoadingDialog(ReviewRatingActivity.this, "");
                String token = SharedPreferenceWriter.getInstance(ReviewRatingActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ReviewsResponse> call = api_service.reviews(TOKEN_KEY + token, serviceId);
                call.enqueue(new Callback<ReviewsResponse>() {
                    @Override
                    public void onResponse(Call<ReviewsResponse> call, Response<ReviewsResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(ReviewRatingActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(ReviewRatingActivity.this, ReviewRatingActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                ReviewsResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<ReviewBean> reviews = (ArrayList<ReviewBean>) server_response.getData();
                                    mReviewList.clear();
                                    if (reviews != null && reviews.size() > 0) {
                                        tv_noData.setVisibility(View.GONE);
                                        mReviewList.addAll(reviews);
                                    }else {
                                        tv_noData.setVisibility(View.VISIBLE);
                                    }
                                    setReviewAdapter();

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(ReviewRatingActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(ReviewRatingActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<ReviewsResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(ReviewRatingActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(ReviewRatingActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called...");
        Intent intent = new Intent(ReviewRatingActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}