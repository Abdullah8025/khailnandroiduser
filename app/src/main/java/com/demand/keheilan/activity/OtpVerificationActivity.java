package com.demand.keheilan.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.CheckUserExistResponse;
import com.demand.keheilan.responsemodel.LoginResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.GPSService;
import com.demand.keheilan.util.InternetCheck;
import com.demand.keheilan.util.LocaleHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.DEVICE_TYPE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;

public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener, GPSService.GetLocationUpdate, CommonUtil.LogoutUser {
    private static final String TAG = "OtpVerificationActivity";
    private ImageView iv_back;
    private Button btn_continue;
    private EditText et_one, et_two, et_three, et_four, et_five, et_six;
    private StringBuilder sb = null;

    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private String mVerificationCode = "", mVerificationId = "";
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        initView();
        mAuth = FirebaseAuth.getInstance();
        addEditTextChangeListeners();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: SIGNUP MODEL " + SignUpModel.getInstance().toString());
        if (SignUpModel.getInstance().getPhoneNumber() != null && SignUpModel.getInstance().getCountryCode() != null) {
            Toast.makeText(this, getString(R.string.please_wait), Toast.LENGTH_SHORT).show();
            fireBaseAction();
        }
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        btn_continue = findViewById(R.id.btn_continue);
        et_one = findViewById(R.id.et_one_otp);
        et_two = findViewById(R.id.et_two);
        et_three = findViewById(R.id.et_three);
        et_four = findViewById(R.id.et_four);
        et_five = findViewById(R.id.et_five);
        et_six = findViewById(R.id.et_six);

        iv_back.setOnClickListener(this);
        btn_continue.setOnClickListener(this);
    }

    private void addEditTextChangeListeners() {
        sb = new StringBuilder();

        et_one.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (sb.length() == 0 & et_one.length() == 1) {
                    sb.append(s);
                    et_one.clearFocus();
                    et_two.requestFocus();
                    et_two.setCursorVisible(true);

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 0) {
                    et_one.requestFocus();
                }
            }
        });
        et_two.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (sb.length() == 1 & et_two.length() == 1) {
                    sb.append(s);
                    et_two.clearFocus();
                    et_three.requestFocus();
                    et_three.setCursorVisible(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 2) {
                    sb.deleteCharAt(1);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 1) {
                    et_one.requestFocus();
                }
            }
        });

        et_three.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (sb.length() == 2 & et_three.length() == 1) {
                    sb.append(s);
                    et_three.clearFocus();
                    et_four.requestFocus();
                    et_four.setCursorVisible(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 3) {
                    sb.deleteCharAt(2);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 2) {
                    et_two.requestFocus();
                }
            }
        });
        et_four.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (sb.length() == 3 & et_four.length() == 1) {
                    sb.append(s);
                    et_four.clearFocus();
                    et_five.requestFocus();
                    et_five.setCursorVisible(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 4) {
                    sb.deleteCharAt(3);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 3) {
                    et_three.requestFocus();
                }
            }
        });

        et_five.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (sb.length() == 4 & et_five.length() == 1) {
                    sb.append(s);
                    et_five.clearFocus();
                    et_six.requestFocus();
                    et_six.setCursorVisible(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 5) {
                    sb.deleteCharAt(4);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 4) {
                    et_four.requestFocus();
                }
            }
        });

        et_six.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (sb.length() == 5 & et_six.length() == 1) {
                    sb.append(s);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 6) {
                    sb.deleteCharAt(5);
                }
            }

            public void afterTextChanged(Editable s) {
                if (sb.length() == 5) {
                    et_five.requestFocus();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.btn_continue) {
            if (sb.toString().length() == 6) {
                verifyVerificationCode(sb.toString());
            } else {
                CommonUtil.showSnackBar(OtpVerificationActivity.this, getString(R.string.valid_otp));
            }
        }

    }

    private void fireBaseAction() {
        Log.d(TAG, "fireBaseAction: starts.....");
        Log.d(TAG, "fireBaseAction: PHONE_NUMEBER ==>" + SignUpModel.getInstance().getCountryCode() + SignUpModel.getInstance().getPhoneNumber());
        StartFireBaseLogin();
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(SignUpModel.getInstance().getCountryCode() + SignUpModel.getInstance().getPhoneNumber())       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallback)          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }

    private void StartFireBaseLogin() {
        Log.d(TAG, "StartFireBaseLogin: starts.....");
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted: starts..." + credential);
                Log.d(TAG, "onVerificationCompleted: OTP is==" + credential.getSmsCode());
                autofillOtp(credential.getSmsCode());
                SignInWithPhone(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                Toast.makeText(OtpVerificationActivity.this, "Number Verification Failed", Toast.LENGTH_SHORT).show();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    CommonUtil.showSnackBar(OtpVerificationActivity.this, getString(R.string.invalid_credential));
                    Log.e(TAG, "onVerificationFailed: " + e.getMessage(), e);
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    CommonUtil.showSnackBar(OtpVerificationActivity.this, getString(R.string.sms_quota_exceeded));
                    Log.e(TAG, "onVerificationFailed: " + e.getMessage(), e);
                }

            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:===" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                Toast.makeText(OtpVerificationActivity.this, "Sending OTP...", Toast.LENGTH_SHORT).show();
            }
        };
    }

    void verifyVerificationCode(String otp) {
        DialogPopup dialog = new DialogPopup(this);
        if (mVerificationId.equals("")) {
            Toast.makeText(OtpVerificationActivity.this, "Incorrect OTP", Toast.LENGTH_SHORT).show();

        } else {
            dialog.showLoadingDialog(this, "");

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
            SignInWithPhone(credential);
        }

    }

    private void SignInWithPhone(PhoneAuthCredential credential) {
        DialogPopup dialog = new DialogPopup(this);
        dialog.dismissLoadingDialog();

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            Toast.makeText(OtpVerificationActivity.this, "Verified OTP", Toast.LENGTH_SHORT).show();
                            serviceCheckNumber();
                            FirebaseUser user = task.getResult().getUser();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(OtpVerificationActivity.this, "Incorrect OTP", Toast.LENGTH_SHORT).show();
                            dialog.dismissLoadingDialog();

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Log.e(TAG, "onComplete: " + task.getException().getMessage(), task.getException());
                            }
                        }
                    }
                });
    }

    private void autofillOtp(String code) {
        try {
            if (code != null && code.length() == 6) {
                et_one.setText(String.valueOf(code.charAt(0)));
                et_two.setText(String.valueOf(code.charAt(1)));
                et_three.setText(String.valueOf(code.charAt(2)));
                et_four.setText(String.valueOf(code.charAt(3)));
                et_five.setText(String.valueOf(code.charAt(4)));
                et_six.setText(String.valueOf(code.charAt(5)));
            }
        } catch (Exception e) {
            Log.e(TAG, "autofillOtp: " + e.getMessage(), e);
        }

    }

    private void serviceCheckNumber() {
        {
            if (new InternetCheck(this).isConnect()) {
                Log.d(TAG, "serviceCheckNumber: LOGIN_CREDENTIAL =>" + SignUpModel.getInstance().getPhoneNumber());
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CheckUserExistResponse> call = api_service.checkPhoneNumber(SignUpModel.getInstance().getCountryCode(), SignUpModel.getInstance().getPhoneNumber());
                call.enqueue(new Callback<CheckUserExistResponse>() {
                    @Override
                    public void onResponse(Call<CheckUserExistResponse> call, Response<CheckUserExistResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            Toast.makeText(OtpVerificationActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(OtpVerificationActivity.this, OtpVerificationActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CheckUserExistResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    if (server_response.isMessage()) {
                                        Log.d(TAG, "onResponse: USER ALREADY EXIST");

                                        // gpsTracker = new GPSTracker(OtpVerificationActivity.this);
                                        String latitude = SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).getString(SPreferenceKey.LATITUDE);
                                        String longitude = SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).getString(SPreferenceKey.LONGITUDE);

                                        if (CommonUtil.blankValidation(latitude) && CommonUtil.blankValidation(latitude))
                                            serviceLogin(latitude, longitude);
                                        else
                                            getLocationParam();
                                    }

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    Log.d(TAG, "onResponse: " + response.message());
                                    if (!server_response.isMessage()) {
                                        Log.d(TAG, "onResponse: USER DOES NOT EXIST");
                                        startActivity(new Intent(OtpVerificationActivity.this, LoginProfileActivity.class));
                                        finish();
                                    }

                                } else {
                                    Toast.makeText(OtpVerificationActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckUserExistResponse> call, Throwable t) {
                        CommonUtil.showSnackBar(OtpVerificationActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(OtpVerificationActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void getLocationParam() {
        new GPSService(OtpVerificationActivity.this, this);
    }

    private void serviceLogin(String lat, String lng) {
        {
            if (new InternetCheck(this).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(OtpVerificationActivity.this);
                dialogPopup.showLoadingDialog(OtpVerificationActivity.this, "");
                String deviceToken = SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).getString(SPreferenceKey.DEVICE_TOKEN);
                String langCode = SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).getString(SPreferenceKey.LANG_CODE);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<LoginResponse> call = api_service.login(SignUpModel.getInstance().getPhoneNumber(), deviceToken, SignUpModel.getInstance().getCountryCode(),
                        langCode, DEVICE_TYPE, lng, lat);
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.isSuccessful()) {
                            LoginResponse server_response = response.body();
                            if (server_response.getStatus() == STATUS_SUCCESS) {
                                dialogPopup.dismissLoadingDialog();
                                SignUpModel.nullModel();
                                saveData(server_response);
                                startActivity(new Intent(OtpVerificationActivity.this, HomeActivity.class));

                            } else if (server_response.getStatus() == STATUS_FAILURE) {
                                dialogPopup.dismissLoadingDialog();
                                Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                            } else {
                                dialogPopup.dismissLoadingDialog();
                                Toast.makeText(OtpVerificationActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(OtpVerificationActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(OtpVerificationActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void saveData(LoginResponse server_response) {
        SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).writeStringValue(SPreferenceKey.USER_ID, server_response.getId());
        SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).writeStringValue(SPreferenceKey.TOKEN, server_response.getToken());
        SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).writeStringValue(SPreferenceKey.COUNTRY_CODE, server_response.getCountry_code());
        SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).writeStringValue(SPreferenceKey.PHONE_NUMBER, server_response.getPhone_number());
        SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).writeStringValue(SPreferenceKey.FULL_NAME, server_response.getFull_name());
        SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).writeBooleanValue(SPreferenceKey.IS_USER_LOGIN, true);
        SharedPreferenceWriter.getInstance(OtpVerificationActivity.this).writeBooleanValue(SPreferenceKey.IS_GUEST_USER, false);
    }


    @Override
    public void getLocationUpdate(Double latitude, Double longitude) {
        Log.d(TAG, "getLocationUpdate: called...");
        if (latitude != null && longitude != null)
            serviceLogin(String.valueOf(latitude), String.valueOf(longitude));
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called....");

        Intent intent = new Intent(OtpVerificationActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}