package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.NotificationAdapter;
import com.demand.keheilan.responsemodel.NotificationListResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class NotificationsActivity extends AppCompatActivity implements View.OnClickListener,CommonUtil.LogoutUser {
    private static final String TAG = "NotificationsActivity";
    private ImageView iv_back;
    private TextView tv_title;
    private RecyclerView rv_notification;

    private List<NotificationListResponse.NotificationBean> mNotificationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        initView();
        mNotificationList = new ArrayList<>();
        tv_title.setText(getString(R.string.notifications));

    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceNotifications();
    }

    private void setNotificationsAdapter() {
        rv_notification.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this));
        rv_notification.setAdapter(new NotificationAdapter(NotificationsActivity.this,mNotificationList));
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        rv_notification = findViewById(R.id.rv_notification);

        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        }
    }

    private void serviceNotifications() {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(NotificationsActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(NotificationsActivity.this);
                dialogPopup.showLoadingDialog(NotificationsActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<NotificationListResponse> call = api_service.notifications(TOKEN_KEY + token);
                call.enqueue(new Callback<NotificationListResponse>() {
                    @Override
                    public void onResponse(Call<NotificationListResponse> call, Response<NotificationListResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(NotificationsActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(NotificationsActivity.this, NotificationsActivity.this);
                        }else {
                            if (response.isSuccessful()) {
                                NotificationListResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<NotificationListResponse.NotificationBean> notifications=(ArrayList<NotificationListResponse.NotificationBean>)server_response.getData();
                                    if (notifications != null && notifications.size() > 0){
                                        mNotificationList.clear();
                                        mNotificationList.addAll(notifications);
                                    }
                                    setNotificationsAdapter();

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(NotificationsActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<NotificationListResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(NotificationsActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(NotificationsActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called....");
        Intent intent = new Intent(NotificationsActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}