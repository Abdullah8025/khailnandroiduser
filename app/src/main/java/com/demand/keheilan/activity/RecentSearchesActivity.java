package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.HomeCategoryAdapter;
import com.demand.keheilan.adapter.RecentSearchAdapter;
import com.demand.keheilan.adapter.SearchServiceAdapter;
import com.demand.keheilan.adapter.ServicesAdapter;
import com.demand.keheilan.responsemodel.CategoriesBean;
import com.demand.keheilan.responsemodel.SearchServiceBean;
import com.demand.keheilan.responsemodel.SearchServiceResponse;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.RecentSearchResponse;
import com.demand.keheilan.responsemodel.ServicesBean;
import com.demand.keheilan.responsemodel.SubCategoryServices;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.ALL_CATEGORY_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.HOME_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_BASE_PRICE;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ID;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.SUBCATEGORY_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.SUB_CATEGORY_ID;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class RecentSearchesActivity extends AppCompatActivity implements View.OnClickListener, SearchServiceAdapter.OnViewServiceClicked,
        CommonUtil.LogoutUser, RecentSearchAdapter.OnRecentSearchClicked {
    private static final String TAG = "RecentSearches";
    private ImageView iv_back, iv_search;
    private TextView tv_title, tv_searchTitle, tv_noData, tv_category;
    private EditText edt_search;
    private RecyclerView rv_search, rv_category;
    private ConstraintLayout const_category;

    private List<RecentSearchResponse.DataBean> mRecentSearchList;
    private List<ServicesBean> mServicesList;
    private List<SearchServiceBean> mSearchedServicesList;
    private List<CategoriesBean> mCategoriesList;
    private boolean showCategorySection = false;
    private String mSubCategoryId, mSearchText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_searches);
        initView();
        initializeList();
        Intent extraData = getIntent();
        if (extraData != null) {
            if (extraData.hasExtra(CAME_FROM_SCREEN)) {
                Log.d(TAG, "onCreate: CAME FROM SCREEN => " + extraData.getStringExtra(CAME_FROM_SCREEN));
                if (extraData.getStringExtra(CAME_FROM_SCREEN).equals(HOME_SCREEN)) {
                    setRecentSearchAdapter();
                    showCategorySection = true;
                    setRecentTitle(true, false);
                }
                if (extraData.getStringExtra(CAME_FROM_SCREEN).equals(SUBCATEGORY_SCREEN)) {
                    if (extraData.hasExtra(SUB_CATEGORY_ID)) {
                        Log.d(TAG, "onCreate: SUB CATEGORY ID => " + extraData.getStringExtra(SUB_CATEGORY_ID));
                        mSubCategoryId = extraData.getStringExtra(SUB_CATEGORY_ID);
                        setServicesAdapter(mServicesList);
                        showCategorySection = false;
                        setRecentTitle(false, false);
                    }

                }
            }
        }


    }

    private void initializeList() {
        mRecentSearchList = new ArrayList<>();
        mServicesList = new ArrayList<>();
        mCategoriesList = new ArrayList<>();
        mSearchedServicesList = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called...");
        if (showCategorySection) {
            serviceGetRecentSearches();
            setRecentTitle(true, false);
        } else {
            if (mSubCategoryId != null && mSubCategoryId.length() > 0) {
                serviceCategoryServices(mSubCategoryId);
            }
        }
        edt_search.setText("");


    }

    private void setRecentTitle(boolean showRecentTitle, boolean showCategoryList) {
        if (showRecentTitle) {
            tv_searchTitle.setText(getString(R.string.recent_searches));
        } else {
            tv_searchTitle.setText(getText(R.string.services));
        }

        if (showCategoryList) {
            const_category.setVisibility(View.VISIBLE);
        } else {
            const_category.setVisibility(View.GONE);
        }

    }

    private void setCategoryAdapter() {
        rv_category.setLayoutManager(new LinearLayoutManager(RecentSearchesActivity.this, RecyclerView.HORIZONTAL, false));
        rv_category.setAdapter(new HomeCategoryAdapter(RecentSearchesActivity.this, mCategoriesList, null));
    }


    private void setRecentSearchAdapter() {
        rv_search.setLayoutManager(new LinearLayoutManager(this));
        rv_search.setAdapter(new RecentSearchAdapter(this, mRecentSearchList, this));
    }

    private void setServiceSearchAdapter(List<SearchServiceBean> serviceList) {
        rv_search.setLayoutManager(new LinearLayoutManager(this));
        rv_search.setAdapter(new SearchServiceAdapter(this, serviceList, this));
    }

    private void setServicesAdapter(List<ServicesBean> servicesList) {
        rv_search.setLayoutManager(new LinearLayoutManager(this));
        rv_search.setAdapter(new ServicesAdapter(this, servicesList, this));
    }


    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        edt_search = findViewById(R.id.edt_search);
        rv_search = findViewById(R.id.rv_search);
        rv_category = findViewById(R.id.rv_category);
        const_category = findViewById(R.id.const_category);
        iv_search = findViewById(R.id.iv_search);
        tv_searchTitle = findViewById(R.id.tv_searchTitle);
        tv_noData = findViewById(R.id.tv_noData);
        tv_category = findViewById(R.id.tv_category);

        iv_back.setOnClickListener(this);
        iv_search.setOnClickListener(this);

        const_category.setVisibility(View.GONE);

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    CommonUtil.hideKeyBoard(RecentSearchesActivity.this, edt_search);
                    if (mSearchText.length() > 0) {
                        serviceSearchService(mSearchText);
                    }
                    return true;
                }
                return false;
            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mSearchText = s.toString().trim();
                if (mSearchText.length() > 0) {
                    iv_search.setVisibility(View.VISIBLE);
                } else {
                    iv_search.setVisibility(View.GONE);
                    tv_searchTitle.setVisibility(View.VISIBLE);
                    tv_noData.setVisibility(View.GONE);
                    CommonUtil.hideKeyBoard(RecentSearchesActivity.this, edt_search);
                    if (showCategorySection)
                        setRecentSearchAdapter();
                    else
                        setServicesAdapter(mServicesList);
                }
                const_category.setVisibility(View.GONE);

            }
        });

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.iv_search) {
            if (CommonUtil.blankValidation(mSearchText)) {
                serviceSearchService(mSearchText);
                CommonUtil.hideKeyBoard(RecentSearchesActivity.this, iv_search);
            } else {
                Toast.makeText(this, getString(R.string.validate_search), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onViewServiceClicked(String serviceId) {
        Log.d(TAG, "onViewServiceClicked: called...");
        openServiceDetail(serviceId);

    }

    @Override
    public void onAddServiceClicked(String serviceId, String basePrice) {
        Log.d(TAG, "onAddServiceClicked: called... SERVICE ID = " + serviceId + " BASE PRICE = " + basePrice);
        if (CommonUtil.isUserLoggedIn(RecentSearchesActivity.this))
            openBookService(serviceId, basePrice);
        else
            CommonUtil.showLoginAlert(RecentSearchesActivity.this, this);
    }

    private void openServiceDetail(String serviceId) {
        Intent intent = new Intent(RecentSearchesActivity.this, ServiceDetailActivity.class);
        intent.putExtra(SERVICE_ID, serviceId);
        startActivity(intent);
    }

    private void openBookService(String serviceId, String basePrice) {
        Intent intent = new Intent(RecentSearchesActivity.this, BookServiceActivity.class);
        intent.putExtra(SERVICE_ID, serviceId);
        intent.putExtra(SERVICE_BASE_PRICE, basePrice);
        startActivity(intent);
    }

    private void serviceGetRecentSearches() {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(RecentSearchesActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(RecentSearchesActivity.this);
                dialogPopup.showLoadingDialog(RecentSearchesActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<RecentSearchResponse> call = api_service.recentSearches(TOKEN_KEY + token);
                call.enqueue(new Callback<RecentSearchResponse>() {
                    @Override
                    public void onResponse(Call<RecentSearchResponse> call, Response<RecentSearchResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(RecentSearchesActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(RecentSearchesActivity.this, RecentSearchesActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                RecentSearchResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<RecentSearchResponse.DataBean> recentSearches = (ArrayList<RecentSearchResponse.DataBean>) server_response.getData();
                                    if (recentSearches != null && recentSearches.size() > 0) {
                                        mRecentSearchList.clear();
                                        mRecentSearchList.addAll(recentSearches);
                                        tv_noData.setVisibility(View.GONE);
                                    } else {
                                        mRecentSearchList.clear();
                                        tv_noData.setVisibility(View.VISIBLE);
                                    }
                                    setRecentSearchAdapter();
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(RecentSearchesActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<RecentSearchResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceSaveSearch(String value) {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(RecentSearchesActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = api_service.saveSearch(TOKEN_KEY + token, value);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            Toast.makeText(RecentSearchesActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(RecentSearchesActivity.this, RecentSearchesActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_FAILURE) {
                                    Toast.makeText(RecentSearchesActivity.this, getString(R.string.save_search_failed), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceSearchService(String service) {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(RecentSearchesActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(RecentSearchesActivity.this);
                dialogPopup.showLoadingDialog(RecentSearchesActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SearchServiceResponse> call = api_service.searchService(TOKEN_KEY + token, service);
                call.enqueue(new Callback<SearchServiceResponse>() {
                    @Override
                    public void onResponse(Call<SearchServiceResponse> call, Response<SearchServiceResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(RecentSearchesActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(RecentSearchesActivity.this, RecentSearchesActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                SearchServiceResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<SearchServiceBean> services = (ArrayList<SearchServiceBean>) server_response.getServices();
                                    ArrayList<CategoriesBean> categories = (ArrayList<CategoriesBean>) server_response.getCategories();
                                    if (services != null && services.size() > 0) {
                                        mSearchedServicesList.clear();
                                        mSearchedServicesList.addAll(services);
                                        tv_noData.setVisibility(View.GONE);
                                        tv_searchTitle.setVisibility(View.VISIBLE);
                                    } else {
                                        mSearchedServicesList.clear();
                                        tv_noData.setVisibility(View.VISIBLE);
                                        tv_searchTitle.setVisibility(View.GONE);
                                    }
                                    if (categories != null && categories.size() > 0) {
                                        mCategoriesList.clear();
                                        mCategoriesList.addAll(categories);
                                        tv_category.setVisibility(View.VISIBLE);
                                    } else {
                                        tv_category.setVisibility(View.GONE);
                                    }
                                    if (showCategorySection) {
                                        setRecentTitle(false, true);
                                        setCategoryAdapter();
                                        setServiceSearchAdapter(mSearchedServicesList);
                                        serviceSaveSearch(service);
                                    } else {
                                        setRecentTitle(false, false);
                                        setServiceSearchAdapter(mSearchedServicesList);
                                    }

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(RecentSearchesActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchServiceResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceCategoryServices(String subCategoryId) {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(RecentSearchesActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(RecentSearchesActivity.this);
                dialogPopup.showLoadingDialog(RecentSearchesActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SubCategoryServices> call = api_service.getServices(TOKEN_KEY + token, subCategoryId);
                call.enqueue(new Callback<SubCategoryServices>() {
                    @Override
                    public void onResponse(Call<SubCategoryServices> call, Response<SubCategoryServices> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(RecentSearchesActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(RecentSearchesActivity.this, RecentSearchesActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                SubCategoryServices server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    setRecentTitle(false, false);
                                    ArrayList<ServicesBean> services = (ArrayList<ServicesBean>) server_response.getData();
                                    if (services != null && services.size() > 0) {
                                        mServicesList.clear();
                                        mServicesList.addAll(services);
                                        tv_noData.setVisibility(View.GONE);
                                    } else {
                                        mServicesList.clear();
                                        tv_noData.setVisibility(View.VISIBLE);
                                    }
                                    setServicesAdapter(mServicesList);

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(RecentSearchesActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<SubCategoryServices> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(RecentSearchesActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called.....");
        Intent intent = new Intent(RecentSearchesActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRecentSearchClicked(String text) {
        Log.d(TAG, "onRecentSearchClicked: called.. SEARCHED VALUE = " + text);
        if (CommonUtil.blankValidation(text)) {
            edt_search.setText(text);
            edt_search.setSelection(edt_search.getText().toString().length());
            serviceSearchService(text);
        }

    }
}