package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.demand.keheilan.R;

public class OnBoardingSecond extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "OnBoardingSecond";
    private TextView tv_skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding_second);
        initView();
    }

    private void initView() {
        tv_skip=findViewById(R.id.tv_skip);

        tv_skip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        if (id==R.id.tv_skip)
            startActivity(new Intent(OnBoardingSecond.this,OnBoardingThird.class));
    }
}