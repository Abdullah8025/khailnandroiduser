package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.AllCategoryGridAdapter;
import com.demand.keheilan.adapter.AllServiceGridAdapter;
import com.demand.keheilan.adapter.TopServiceGridAdapter;
import com.demand.keheilan.responsemodel.CategoriesBean;
import com.demand.keheilan.responsemodel.HomeApiResponse;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.List;

import static com.demand.keheilan.util.MyAppConstants.ALL_CATEGORY_LIST;
import static com.demand.keheilan.util.MyAppConstants.ALL_SERVICES_LIST;
import static com.demand.keheilan.util.MyAppConstants.CATEGORY_ID;
import static com.demand.keheilan.util.MyAppConstants.CATEGORY_NAME;
import static com.demand.keheilan.util.MyAppConstants.MY_LIST;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ID;
import static com.demand.keheilan.util.MyAppConstants.SHOW_LIST;
import static com.demand.keheilan.util.MyAppConstants.TOP_SERVICES_LIST;


public class AllCategoriesActivity extends AppCompatActivity implements View.OnClickListener, TopServiceGridAdapter.OnTopServiceGridClick,
        AllServiceGridAdapter.OnAllServiceGridClick, AllCategoryGridAdapter.OnCategoryGridClick {
    private static final String TAG = "AllCategoriesActivity";
    private RecyclerView rv_allCategory;
    private ImageView iv_back, iv_search;
    private TextView tv_title;
    private EditText edt_search;

    private ArrayList<CategoriesBean> mAllCategoryList;
    private ArrayList<CategoriesBean> mSearchCategoriesList;
    private String mSearchText;
    private SearchCategory searchCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);
        initView();
        mAllCategoryList = new ArrayList<>();
        mSearchCategoriesList = new ArrayList<>();
        setCommonRecyclerProperties();
        Intent extraData = getIntent();
        if (extraData != null) {
            if (extraData.hasExtra(SHOW_LIST)) {
                Log.d(TAG, "onCreate: SHOW LIST => " + extraData.getStringExtra(SHOW_LIST));
                if (extraData.getStringExtra(SHOW_LIST).equals(TOP_SERVICES_LIST)) {
                    tv_title.setText(getString(R.string.top_services));
                    ArrayList<HomeApiResponse.TopServicesBean> topServicesList = (ArrayList<HomeApiResponse.TopServicesBean>) extraData.getSerializableExtra(MY_LIST);
                    if (topServicesList != null && topServicesList.size() > 0) {
                        Log.d(TAG, "onCreate: TOP SERVICE LIST RECEIVED=> " + topServicesList.toString());
                        SetUpTopServicesAdapter(topServicesList);
                    }
                }
                if (extraData.getStringExtra(SHOW_LIST).equals(ALL_SERVICES_LIST)) {
                    tv_title.setText(getString(R.string.all_services));
                    ArrayList<HomeApiResponse.ServicesBean> allServices = (ArrayList<HomeApiResponse.ServicesBean>) extraData.getSerializableExtra(MY_LIST);
                    if (allServices != null && allServices.size() > 0) {
                        Log.d(TAG, "onCreate: ALL SERVICES LIST RECEIVED=> " + allServices.toString());
                        setUpAllServicesAdapter(allServices);
                    }
                }
                if (extraData.getStringExtra(SHOW_LIST).equals(ALL_CATEGORY_LIST)) {
                    tv_title.setText(getString(R.string.all_category));
                    mAllCategoryList = (ArrayList<CategoriesBean>) extraData.getSerializableExtra(MY_LIST);
                    if (mAllCategoryList != null && mAllCategoryList.size() > 0) {
                        Log.d(TAG, "onCreate: ALL CATEGORY LIST RECEIVED=> " + mAllCategoryList.toString());
                        setUpAllCategoryAdapter(mAllCategoryList);
                    }
                }
            }
        }

    }

    private void setCommonRecyclerProperties() {
        rv_allCategory.setLayoutManager(new GridLayoutManager(this, 3));
//        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(AllCategoriesActivity.this, R.dimen.small_item_offset);
//        rv_allCategory.addItemDecoration(itemDecoration);
    }

    private void setUpAllCategoryAdapter(List<CategoriesBean> categoriesList) {
        rv_allCategory.setAdapter(new AllCategoryGridAdapter(AllCategoriesActivity.this, categoriesList, this));

    }

    private void setUpAllServicesAdapter(List<HomeApiResponse.ServicesBean> allServices) {
        rv_allCategory.setAdapter(new AllServiceGridAdapter(AllCategoriesActivity.this, allServices, this));
    }

    private void SetUpTopServicesAdapter(List<HomeApiResponse.TopServicesBean> topServicesList) {
        rv_allCategory.setAdapter(new TopServiceGridAdapter(AllCategoriesActivity.this, topServicesList, this));
    }

    private void initView() {
        rv_allCategory = findViewById(R.id.rv_allCategory);
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        iv_search = findViewById(R.id.iv_search);
        edt_search = findViewById(R.id.edt_search);

        iv_back.setOnClickListener(this);
        iv_search.setOnClickListener(this);

        searchCategory = new SearchCategory();

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    CommonUtil.hideKeyBoard(AllCategoriesActivity.this, edt_search);
                    if (mSearchText.length() > 0) {
                        startSearching();
                    }
                    return true;
                }
                return false;
            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mSearchText = s.toString().trim();
                if (mSearchText.length() == 0) {
                    iv_search.setVisibility(View.GONE);
                    setUpAllCategoryAdapter(mAllCategoryList);
                    CommonUtil.hideKeyBoard(AllCategoriesActivity.this, edt_search);
                } else {
                    iv_search.setVisibility(View.GONE);
                    startSearching();
                }

            }
        });


    }

    private void startSearching() {
        if (searchCategory.getStatus() != AsyncTask.Status.RUNNING) {
            Log.d(TAG, "onCameraIdle: ASYNC Task is Not Running");
            searchCategory = new SearchCategory();
            searchCategory.execute();
        } else {
            Log.d(TAG, "onCameraIdle: ASYNC TASK RUNNING");
            searchCategory.cancel(true);
            searchCategory = new SearchCategory();
            searchCategory.execute();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        }
//        else if (id == R.id.iv_search) {
//            CommonUtil.hideKeyBoard(AllCategoriesActivity.this, iv_search);
//            searchCategory();
//        }
    }


    @Override
    public void onCategoryGridClick(CategoriesBean categoriesBean) {
        Log.d(TAG, "onCategoryGridClick: called  CATEGORY => " + categoriesBean.toString());
        Intent intent = new Intent(AllCategoriesActivity.this, SubCategoryActivity.class);
        intent.putExtra(CATEGORY_ID, categoriesBean.getId());
        intent.putExtra(CATEGORY_NAME, categoriesBean.getCategory_name());
        startActivity(intent);
    }

    @Override
    public void onAllServiceGridClick(HomeApiResponse.ServicesBean allservice) {
        Log.d(TAG, "onAllServiceGridClick: SERVICE=> " + allservice.toString());
        openServiceDetail(allservice.getId());

    }

    @Override
    public void onTopServiceGridClicked(HomeApiResponse.TopServicesBean topServicesBean) {
        Log.d(TAG, "onTopServiceGridClicked: TOP SERVICE=> " + topServicesBean.toString());
        openServiceDetail(topServicesBean.getId());

    }

    private void openServiceDetail(String serviceId) {
        Intent intent = new Intent(AllCategoriesActivity.this, ServiceDetailActivity.class);
        intent.putExtra(SERVICE_ID, serviceId);
        startActivity(intent);
    }

    private class SearchCategory extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSearchCategoriesList.clear();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setUpAllCategoryAdapter(mSearchCategoriesList);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (CategoriesBean category : mAllCategoryList) {
                if (category.getCategory_name().toLowerCase().contains(mSearchText.toLowerCase()))
                    mSearchCategoriesList.add(category);
            }
            return null;
        }
    }
}