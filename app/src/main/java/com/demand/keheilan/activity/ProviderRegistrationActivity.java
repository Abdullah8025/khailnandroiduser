package com.demand.keheilan.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.demand.keheilan.BuildConfig;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.FilePath;
import com.demand.keheilan.util.InternetCheck;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class ProviderRegistrationActivity extends AppCompatActivity implements View.OnClickListener, CommonUtil.LogoutUser {
    private static final String TAG = "ProviderRegistrationAct";
    private ImageView iv_back, iv_providerProfile, iv_addImage;
    private TextView tv_title;
    private CountryCodePicker ccp;
    private EditText edt_phoneNumber, edt_providerName, edt_providerEmail, edt_password;
    private Button btn_done;

    private String mImagePath, mCountryCode;
    private static final int STORAGE_REQUEST_CODE = 17;
    private static final int PROVIDER_PHOTO_REQ = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_registration);
        initView();
        tv_title.setText(getString(R.string.provider_register));
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        iv_providerProfile = findViewById(R.id.iv_providerProfile);
        iv_addImage = findViewById(R.id.iv_addImage);
        ccp = findViewById(R.id.ccp);
        edt_phoneNumber = findViewById(R.id.edt_phoneNumber);
        edt_providerName = findViewById(R.id.edt_providerName);
        edt_providerEmail = findViewById(R.id.edt_providerEmail);
        edt_password = findViewById(R.id.edt_password);
        btn_done = findViewById(R.id.btn_done);

        iv_back.setOnClickListener(this);
        btn_done.setOnClickListener(this);
        iv_addImage.setOnClickListener(this);
        ccp.setAutoDetectedCountry(true);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.iv_addImage) {
            if (checkPermissions(CommonUtil.getPermissionList())) {
                takePhotoIntent();
            }
        } else if (id == R.id.btn_done) {
            CommonUtil.hideKeyBoard(ProviderRegistrationActivity.this, btn_done);
            if (validateProviderRegistration()) {
                serviceRegisterProvider();
            }
        }
    }

    private boolean checkPermissions(List<String> permissions) {
        List<String> permissionsNotGranted = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(ProviderRegistrationActivity.this, permission) != PackageManager.PERMISSION_GRANTED)
                permissionsNotGranted.add(permission);
        }
        if (!permissionsNotGranted.isEmpty()) {
            requestPermissions(permissionsNotGranted.toArray(new String[permissionsNotGranted.size()]), STORAGE_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_REQUEST_CODE: {
                Log.d(TAG, "onRequestPermissionsResult: called.....");
                boolean anyPermissionDenied = false;
                boolean neverAskAgainSelected = false;
                // Check if any permission asked has been denied
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        anyPermissionDenied = true;
                        //check if user select "never ask again" when denying any permission
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(ProviderRegistrationActivity.this, permissions[i])) {
                            neverAskAgainSelected = true;
                        }
                    }
                }
                if (!anyPermissionDenied) {
                    Log.d(TAG, "onRequestPermissionsResult: Every permissions granted...");
                    // All Permissions asked were granted! Yey!
                    // DO YOUR STUFF

                    takePhotoIntent();
                } else {
                    // the user has just denied one or all of the permissions
                    // use this message to explain why he needs to grant these permissions in order to proceed
                    if (neverAskAgainSelected) {
                        Log.d(TAG, "onRequestPermissionsResult: never ask again selected...");
                        //This message is displayed after the user has checked never ask again checkbox.
                        shouldShowSettings(iv_addImage);

                    } else {
                        Log.d(TAG, "onRequestPermissionsResult: denied selected......");
                        //This message is displayed while the user hasn't checked never ask again checkbox.
                        CommonUtil.showSnackBar(ProviderRegistrationActivity.this, "Give Permissions to Upload Image");
                    }
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void shouldShowSettings(View v) {
        Snackbar.make(v, getString(R.string.grant_permission), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.grant_access), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, " snackbar onClick: starts");
                        //user has permanenty denied the permissions so take them to the settings
                        Log.d(TAG, "onClick: user has permanently denied the permission ...");
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        Log.d(TAG, " snackbar onClick: uri is = " + uri.toString());
                        intent.setData(uri);
                        startActivity(intent);
                        Log.d(TAG, " snackbar onClick: ends");
                    }
                }).show();
    }

    private void takePhotoIntent() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File imageFile = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir);
            mImagePath = imageFile.getAbsolutePath();
            Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", imageFile);
            Intent intent = new CommonUtil().getPickIntent(getApplicationContext(), imageFileUri);
            startActivityForResult(intent, PROVIDER_PHOTO_REQ);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
//        File file = new File(capture_dir);
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        mImagePath = capture_dir + System.currentTimeMillis() + ".jpg";
//        Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", new File(mImagePath));
//        Intent intent = new CommonUtil().getPickIntent(ProviderRegistrationActivity.this, imageFileUri);
//        startActivityForResult(intent, PROVIDER_PHOTO_REQ);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PROVIDER_PHOTO_REQ:
                    if (data != null) {
                        if (data.getData() != null)
                            mImagePath = FilePath.getPath(ProviderRegistrationActivity.this, Uri.parse(data.getDataString()));
                    }
                    compressAndSetImage(mImagePath);
                    break;
            }
        }
    }


    private void compressAndSetImage(String imagePath) {
        Log.d(TAG, "compressAndSetImage: GIVEN_PATH==>" + imagePath);
//        Bitmap bm = BitmapFactory.decodeFile(imagePath);
//        Bitmap myBitmap = CommonUtil.setImageOrientation(bm, imagePath);
//        mImagePath = compressImage(myBitmap, myBitmap.getWidth(), myBitmap.getHeight());
//        Log.d(TAG, "compressAndSetImage: FINAL_IMAGE_PATH==>" + mImagePath);
        setProviderImage(imagePath);
    }

    private void setProviderImage(String mImagePath) {
        Glide.with(ProviderRegistrationActivity.this).
                load(mImagePath).
                placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(iv_providerProfile);
    }

    private String compressImage(Bitmap finalBitmap, int width, int height) {
        Log.d(TAG, "compressImage: starts.....");
        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
        File myDir = new File(capture_dir);
        myDir.mkdirs();
        File file = new File(myDir, System.currentTimeMillis() + ".jpg");
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);//	compressing to 50%
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    private boolean validateProviderRegistration() {
        mCountryCode = ccp.getSelectedCountryCodeWithPlus();
        Log.d(TAG, "validateProviderRegistration: COUNTRY CODE = " + mCountryCode);
        if (mImagePath == null || mImagePath.length() == 0) {
            CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.validate_provider_photo));
            return false;
        }
        if (mCountryCode == null || mCountryCode.length() == 0) {
            CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.validate_country_code));
            return false;
        }

        if (!CommonUtil.blankValidation(edt_phoneNumber.getText().toString())) {
            CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.validate_number));
            return false;
        }
        if (edt_phoneNumber.getText().toString().length() < 7 || edt_phoneNumber.getText().toString().length() > 11) {
            CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.validate_number));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_providerName.getText().toString())) {
            CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.validate_provider_name));
            return false;
        }

        if (!CommonUtil.blankValidation(edt_providerEmail.getText().toString())) {
            CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.validate_provider_email));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_password.getText().toString())) {
            CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.validate_password));
            return false;
        }

        return true;
    }


    private void serviceRegisterProvider() {
        {
            if (new InternetCheck(ProviderRegistrationActivity.this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(ProviderRegistrationActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialog = new DialogPopup(ProviderRegistrationActivity.this);
                dialog.showLoadingDialog(ProviderRegistrationActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = null;
                try {
                    call = api_service.registerProvider(TOKEN_KEY + token, getParam(), getProfilePic(mImagePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialog.dismissLoadingDialog();
                            Toast.makeText(ProviderRegistrationActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(ProviderRegistrationActivity.this, ProviderRegistrationActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialog.dismissLoadingDialog();
                                    finish();

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialog.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialog.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(ProviderRegistrationActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private Map<String, RequestBody> getParam() {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("country_code", RequestBody.create(MediaType.parse("text/plain"), mCountryCode));
        map.put("phone_number", RequestBody.create(MediaType.parse("text/plain"), edt_phoneNumber.getText().toString().trim()));
        map.put("email", RequestBody.create(MediaType.parse("text/plain"), edt_providerEmail.getText().toString().trim()));
        map.put("password", RequestBody.create(MediaType.parse("text/plain"), edt_password.getText().toString().trim()));
        map.put("service_provider_name", RequestBody.create(MediaType.parse("text/plain"), edt_providerName.getText().toString().trim()));
        return map;
    }

    private MultipartBody.Part getProfilePic(String currentImagePath) throws IOException {
        MultipartBody.Part part = null;
        if ((currentImagePath != null) && (currentImagePath.length() > 0)) {
            File file = new File(currentImagePath);
            RequestBody body = RequestBody.create(MediaType.parse("" + "*/*"), file);
            part = MultipartBody.Part.createFormData("image", file.getName(), body);
        }
        return part;
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called");
        Intent intent = new Intent(ProviderRegistrationActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}