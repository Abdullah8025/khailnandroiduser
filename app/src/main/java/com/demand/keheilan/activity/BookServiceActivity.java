package com.demand.keheilan.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.BuildConfig;
import com.demand.keheilan.R;
import com.demand.keheilan.adapter.CommonImageAdapter;
import com.demand.keheilan.adapter.ServiceTimeAdapter;
import com.demand.keheilan.model.ServiceTimeModel;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.ServiceBookingResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.FilePath;
import com.demand.keheilan.util.InternetCheck;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.CommonUtil.getPermissionList;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_PLACE_CITY;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_PLACE_LAT;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_PLACE_LONGI;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_STATUS_STARTED;
import static com.demand.keheilan.util.MyAppConstants.CAMERA_INTENT;
import static com.demand.keheilan.util.MyAppConstants.DEFAULT_ADDRESS;
import static com.demand.keheilan.util.MyAppConstants.SELECTED_ADDRESS;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_BASE_PRICE;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ID;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;
import static com.demand.keheilan.util.MyAppConstants.VALUE_FALSE;
import static com.demand.keheilan.util.MyAppConstants.VALUE_TRUE;

public class BookServiceActivity extends AppCompatActivity implements ServiceTimeAdapter.OnTimingClicked, View.OnClickListener,
        CalendarView.OnDateChangeListener, CommonImageAdapter.onImageClick, CommonUtil.LogoutUser {
    private static final String TAG = "BookServiceActivity";
    private Button btn_book;
    private ImageView iv_back, iv_upload;
    private TextView tv_title, tv_address, tv_enterManually;
    private CalendarView mCalendarView;
    private EditText edt_requirement;
    String latit = "";
    String longit = "";

    private Dialog bookingSucessDialog;
    private RecyclerView rv_serviceTiming, rv_serviceImage;
    private List<ServiceTimeModel> mTimingList;
    public static final int ADDRESS_REQUEST_CODE = 3;
    public static final int ENTER_ADDRESS_REQUEST_CODE = 4;
    public static final int STORAGE_REQUEST_CODE = 6;
    private boolean isDefaultAddress = false;
    private List<String> mImageList;
    private static final int PHOTO_REQ = 7;
    private String path, mServiceId, mBasePrice, mTiming, mDate, mNightBooking, mCityName;
    public static final String BOOKING_STATUS = BOOKING_STATUS_STARTED;
    private Date mTodayDate;
    private static final String HAIL_CITY_REGEX = ".*\\bhail\\b.*";
    public static final String HAIL_CITY_ARABIC_REGEX = ".*\\bحَائِل\\b.*";
    public static final String HAIL_CITY_ARABIC_REGEX_2 = ".*\\bحائل\\b.*";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_service);
        initView();
        Intent extraData = getIntent();
        if (extraData != null) {
            if (extraData.hasExtra(SERVICE_ID)) {
                Log.d(TAG, "onCreate: SERVICE_ID = " + extraData.getStringExtra(SERVICE_ID) + "BASE PRICE = " + extraData.getStringExtra(SERVICE_BASE_PRICE));
                mServiceId = extraData.getStringExtra(SERVICE_ID);
                mBasePrice = extraData.getStringExtra(SERVICE_BASE_PRICE);
            }
        } else {
            Log.d(TAG, "onCreate: EXTRA DATA NOT FOUND..");
        }
        mImageList = new ArrayList<>();
        mTimingList = new ArrayList<>();
        setImageAdapter();
        tv_title.setText(getString(R.string.book_service));
        mNightBooking = VALUE_FALSE;

        setDefaultTimingList();
        getTimeSlots(true);
    }

    private void initiateTimingList(boolean[] positions) {
        if (positions.length > 0) {
            for (int i = 0; i < positions.length; i++) {
                if (positions[i]) {
                    mTimingList.get(i).setDateEnabled(true);
                } else {
                    mTimingList.get(i).setDateEnabled(false);
                }
            }
        } else {
            for (int i = 0; i < 4; i++) {
                mTimingList.get(i).setDateEnabled(false);
            }
        }

        setServiceTimeAdapter();

    }

    private void setDefaultTimingList() {
        mTimingList.add(new ServiceTimeModel("08:00 AM - 12:00 PM", false, true));
        mTimingList.add(new ServiceTimeModel("01:00 PM - 05:00 PM", false, true));
        mTimingList.add(new ServiceTimeModel("05:00 PM - 08:00 PM", false, true));
        mTimingList.add(new ServiceTimeModel("08:00 PM - 12:00 AM", false, true));
    }

    private void getTimeSlots(boolean isToday) {
        boolean[] positionEnabled;
        if (isToday) {
            Log.d(TAG, "getTimeSlots: TIME SLOT FOR TODAY..");
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            Log.d(TAG, "getTimeSlots: CURRENT HOUR => " + hour);
            if (hour >= 8 && hour <= 12) {
                positionEnabled = new boolean[]{true, true, true, true};
            } else if (hour >= 13 && hour < 17) {
                positionEnabled = new boolean[]{false, true, true, true};
            } else if (hour >= 17 && hour < 20) {
                positionEnabled = new boolean[]{false, false, true, true};
            } else if (hour >= 20 && hour <= 24) {
                positionEnabled = new boolean[]{false, false, false, true};
            } else {
                Log.d(TAG, "compareTime: no match found...");
                positionEnabled = new boolean[0];
            }
        } else {
            Log.d(TAG, "getTimeSlots: TIME SLOTS FOR OTHER DAY..");
            positionEnabled = new boolean[]{true, true, true, true};
        }


        initiateTimingList(positionEnabled);
    }

    private void setServiceTimeAdapter() {
        rv_serviceTiming.setLayoutManager(new LinearLayoutManager(this));
        rv_serviceTiming.setAdapter(new ServiceTimeAdapter(this, mTimingList, this));
    }

    private void initView() {
        rv_serviceTiming = findViewById(R.id.rv_serviceTiming);
        rv_serviceImage = findViewById(R.id.rv_serviceImage);
        btn_book = findViewById(R.id.btn_book);
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_address = findViewById(R.id.tv_address);
        tv_enterManually = findViewById(R.id.tv_enterManually);
        mCalendarView = findViewById(R.id.calendarView);
        iv_upload = findViewById(R.id.iv_upload);
        edt_requirement = findViewById(R.id.edt_requirement);
        mCalendarView.setMinDate((new Date().getTime()));
        mDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date().getTime());
        Log.d(TAG, "initView: DEFAULT CURRENT DATE SET = " + mDate);

        iv_back.setOnClickListener(this);
        btn_book.setOnClickListener(this);
        tv_address.setOnClickListener(this);
        mCalendarView.setOnDateChangeListener(this);
        tv_enterManually.setOnClickListener(this);
        iv_upload.setOnClickListener(this);

        try {
            mTodayDate = new SimpleDateFormat("yyyy-MM-dd").parse(mDate);
            Log.d(TAG, "initView: TODAY => " + mTodayDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setImageAdapter() {
        rv_serviceImage.setLayoutManager(new LinearLayoutManager(BookServiceActivity.this, RecyclerView.HORIZONTAL, false));
        rv_serviceImage.setAdapter(new CommonImageAdapter(BookServiceActivity.this, mImageList, this));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_book) {
            if (validateBooking()) {
                Log.d(TAG, "onClick: IS NIGHT BOOKING = " + mNightBooking);
                serviceCheckSlot();
            }
        } else if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.tv_address) {
            Intent intent = new Intent(BookServiceActivity.this, AddressPickActivity.class);
            startActivityForResult(intent, ADDRESS_REQUEST_CODE);
        } else if (id == R.id.tv_enterManually) {
            Intent intent = new Intent(BookServiceActivity.this, EnterAddressActivity.class);
            startActivityForResult(intent, ENTER_ADDRESS_REQUEST_CODE);
        } else if (id == R.id.iv_upload) {
            if (checkPermissions(getPermissionList())) {
                if (checkImageLoadSize(mImageList))
                    takePhotoIntent();
            }
        }
    }


//    public void goToLocationFromAddress(String strAddress) {
//        //Create coder with Activity context - this
//        Geocoder coder = new Geocoder(this);
//        List<Address> address;
//
//        try {
//            //Get latLng from String
//            address = coder.getFromLocationName(strAddress, 5);
//
//            //check for null
//            if (address != null) {
//
//                //Lets take first possibility from the all possibilities.
//                try {
//                    Address location = address.get(0);
//                    latit=String.valueOf(location.getLatitude());
//                    longit=String.valueOf(location.getLongitude());
//
//                    //Animate and Zoon on that map location
//
//                } catch (IndexOutOfBoundsException er) {
//                    Toast.makeText(this, "Location isn't available", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public LatLng getLocationFromAddress(Context context, String strAddress) {
//
//        Geocoder coder = new Geocoder(context);
//        List<Address> address;
//        LatLng p1 = null;
//
//        try {
//            // May throw an IOException
//            address = coder.getFromLocationName(strAddress, 5);
//            if (address == null) {
//                return null;
//            }
//
//            Address location = address.get(0);
//            latit=String.valueOf(location.getLatitude());
//            longit=String.valueOf(location.getLongitude());
//
//            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
//
//        } catch (IOException ex) {
//
//            ex.printStackTrace();
//        }
//
//        return p1;
//    }

    private boolean checkPermissions(List<String> permissions) {
        List<String> permissionsNotGranted = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(BookServiceActivity.this, permission) != PackageManager.PERMISSION_GRANTED)
                permissionsNotGranted.add(permission);
        }
        if (!permissionsNotGranted.isEmpty()) {
            requestPermissions(permissionsNotGranted.toArray(new String[permissionsNotGranted.size()]), STORAGE_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_REQUEST_CODE: {
                Log.d(TAG, "onRequestPermissionsResult: called.....");
                boolean anyPermissionDenied = false;
                boolean neverAskAgainSelected = false;
                // Check if any permission asked has been denied
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        anyPermissionDenied = true;
                        //check if user select "never ask again" when denying any permission
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(BookServiceActivity.this, permissions[i])) {
                            neverAskAgainSelected = true;
                        }
                    }
                }
                if (!anyPermissionDenied) {
                    Log.d(TAG, "onRequestPermissionsResult: Every permissions granted...");
                    // All Permissions asked were granted! Yey!
                    // DO YOUR STUFF

                    takePhotoIntent();
                } else {
                    // the user has just denied one or all of the permissions
                    // use this message to explain why he needs to grant these permissions in order to proceed
                    if (neverAskAgainSelected) {
                        Log.d(TAG, "onRequestPermissionsResult: never ask again selected...");
                        //This message is displayed after the user has checked never ask again checkbox.
                        shouldShowSettings(iv_upload);

                    } else {
                        Log.d(TAG, "onRequestPermissionsResult: denied selected......");
                        //This message is displayed while the user hasn't checked never ask again checkbox.
                        CommonUtil.showSnackBar(BookServiceActivity.this, "Give Permissions to Upload Image");
                    }
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void shouldShowSettings(View v) {
        Snackbar.make(v, getString(R.string.grant_permission), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.grant_access), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, " snackbar onClick: starts");
                        //user has permanenty denied the permissions so take them to the settings
                        Log.d(TAG, "onClick: user has permanently denied the permission ...");
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        Log.d(TAG, " snackbar onClick: uri is = " + uri.toString());
                        intent.setData(uri);
                        startActivity(intent);
                        Log.d(TAG, " snackbar onClick: ends");
                    }
                }).show();
    }

    private void takePhotoIntent() {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File imageFile = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir);
            path = imageFile.getAbsolutePath();
            Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", imageFile);
            Intent intent = new CommonUtil().getPickIntent(getApplicationContext(), imageFileUri);
            startActivityForResult(intent, PHOTO_REQ);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
//        File file = new File(capture_dir);
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        path = capture_dir + System.currentTimeMillis() + ".jpg";
//        Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", new File(path));
//        Intent intent = new CommonUtil().getPickIntent(getApplicationContext(), imageFileUri);
//        startActivityForResult(intent, PHOTO_REQ);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult: called.....");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADDRESS_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                if (data != null) {

                    latit = data.getStringExtra(BOOKING_PLACE_LAT);
                    longit = data.getStringExtra(BOOKING_PLACE_LONGI);
                    mCityName = data.getStringExtra(BOOKING_PLACE_CITY);

                    Log.d(TAG, "onActivityResult:; ADDRESS = " + data.getStringExtra(SELECTED_ADDRESS) + "\n" +
                            "City => " + mCityName + " latitude=> " + latit + " longitude => " + longit);
                    tv_address.setText(data.getStringExtra(SELECTED_ADDRESS));
                    isDefaultAddress = false;
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult: RESULT CANCELED...");
                latit = "";
                longit = "";
                mCityName = "";
                tv_address.setText("");
            }

        } else if (requestCode == ENTER_ADDRESS_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                if (data != null) {
                    tv_address.setText(data.getStringExtra(SELECTED_ADDRESS));
                    isDefaultAddress = data.getBooleanExtra(DEFAULT_ADDRESS, isDefaultAddress);
                    mCityName = data.getStringExtra(BOOKING_PLACE_CITY);

                    Log.d(TAG, "onActivityResult: ADDRESS = " + data.getStringExtra(SELECTED_ADDRESS) +
                            ", IsDefaultAddress = " + data.getBooleanExtra(DEFAULT_ADDRESS, isDefaultAddress) + "\n" +
                            "City Name => " + data.getStringExtra(BOOKING_PLACE_CITY));
                    //isDefaultAddress = false;
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult: RESULT CANCELED...");
                tv_address.setText("");
                mCityName = "";
                latit = "";
                longit = "";
            }

        } else if (requestCode == PHOTO_REQ) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Log.d(TAG, "onActivityResult: DATA NOT NULL.. => " + data.getDataString());
                    if (data.getData() != null)
                        path = FilePath.getPath(BookServiceActivity.this, Uri.parse(data.getDataString()));

                } else {
                    Log.d(TAG, "onActivityResult: DATA NULL...");
                }
                compressAndSetImage(path);
            }
        }
    }

    public String getPickImageResultUriPath(Intent data) {
        if (data != null) {
            Log.d(TAG, "getPickImageResultUriPath: DATA => " + data.toString());
            return data.getData() != null ? data.getData().toString() : path;
        }
        return null;
    }


    private void compressAndSetImage(String imagePath) {
        Log.d(TAG, "compressAndSetImage: GIVEN_PATH==>" + imagePath);
//        Bitmap bm = BitmapFactory.decodeFile(imagePath);
//        Bitmap myBitmap = CommonUtil.setImageOrientation(bm, imagePath);
        // String finalpath = compressImage(myBitmap, myBitmap.getWidth(), myBitmap.getHeight());
        // Log.d(TAG, "compressAndSetImage: FINAL_IMAGE_PATH==>" + finalpath);
        mImageList.add(imagePath);
        rv_serviceImage.getAdapter().notifyDataSetChanged();

    }

    private String compressImage(Bitmap finalBitmap, int width, int height) {
        Log.d(TAG, "compressImage: starts.....");
        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
        File myDir = new File(capture_dir);
        myDir.mkdirs();
        File file = new File(myDir, System.currentTimeMillis() + ".jpg");
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);//	compressing to 50%
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }


    private boolean checkImageLoadSize(List<String> list) {
        if (!(list.size() < 2)) {
            CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.you_can_add_up_to_2_images));
            return false;
        }
        return true;
    }

    private boolean validateBooking() {
        if (!CommonUtil.blankValidation(mServiceId)) {
            CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.validate_service_id));
            return false;
        }
        if (!CommonUtil.blankValidation(mDate)) {
            CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.validate_date));
            return false;
        }
        if (!CommonUtil.blankValidation(mTiming)) {
            CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.validate_service_timing));
            return false;
        }
        if (!CommonUtil.blankValidation(tv_address.getText().toString().trim())) {
            CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.validate_booking_address));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_requirement.getText().toString())) {
            CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.validate_requirement));
            return false;
        }
        if (!(mImageList.size() == 2)) {
            CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.two_image_validation));
            return false;
        }

        if (CommonUtil.blankValidation(mCityName)) {
            Log.d(TAG, "validateBooking: CITY NAME FOUND");
            if (!(mCityName.toLowerCase().matches(HAIL_CITY_REGEX)) && !(mCityName.toLowerCase().matches(HAIL_CITY_ARABIC_REGEX)) &&
                    !(mCityName.toLowerCase().matches(HAIL_CITY_ARABIC_REGEX_2))) {
                CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.only_available_in_hail_city));
                return false;
            }
        } else {
            Log.d(TAG, "validateBooking: City NAME NOT FOUND");
            String address = tv_address.getText().toString().toLowerCase().trim();
            if (!(address.matches(HAIL_CITY_REGEX)) && !(address.matches(HAIL_CITY_ARABIC_REGEX)) &&
                    !(address.matches(HAIL_CITY_ARABIC_REGEX_2))) {
                CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.only_available_in_hail_city));
                return false;
            }
        }

        return true;
    }

    private void showBookingSuccessDialog(String bookingId) {
        bookingSucessDialog = new Dialog(BookServiceActivity.this, android.R.style.Theme_Black_NoTitleBar);
        bookingSucessDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bookingSucessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bookingSucessDialog.setContentView(R.layout.layout_query_sent);
        bookingSucessDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        bookingSucessDialog.setCanceledOnTouchOutside(true);

        ImageView iv_close = bookingSucessDialog.findViewById(R.id.iv_close);
        TextView tv_message = bookingSucessDialog.findViewById(R.id.tv_message);
        TextView tv_booking_id = bookingSucessDialog.findViewById(R.id.tv_booking_id);

        tv_message.setText(getString(R.string.booking_success_message));
        tv_booking_id.setText("#" + bookingId);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingSucessDialog.dismiss();
                startActivity(new Intent(BookServiceActivity.this, HomeActivity.class));
            }
        });

        bookingSucessDialog.show();
    }

    private void serviceCheckSlot() {
        {


            if (new InternetCheck(BookServiceActivity.this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(BookServiceActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialog = new DialogPopup(BookServiceActivity.this);
                dialog.showLoadingDialog(BookServiceActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = api_service.isSlotAvailable(TOKEN_KEY + token, mDate, mTiming);

                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialog.dismissLoadingDialog();
                            Toast.makeText(BookServiceActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(BookServiceActivity.this, BookServiceActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialog.dismissLoadingDialog();
                                    serviceBookService();
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialog.dismissLoadingDialog();
                                    Toast.makeText(BookServiceActivity.this, getString(R.string.time_slot_full), Toast.LENGTH_SHORT).show();
                                } else {
                                    dialog.dismissLoadingDialog();
//                                    CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        Toast.makeText(BookServiceActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceBookService() {
        {
            if (new InternetCheck(BookServiceActivity.this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(BookServiceActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialog = new DialogPopup(BookServiceActivity.this);
                dialog.showLoadingDialog(BookServiceActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ServiceBookingResponse> call = null;
                try {
                    call = api_service.bookService(TOKEN_KEY + token, getParam(), getImages("image_", mImageList));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                call.enqueue(new Callback<ServiceBookingResponse>() {
                    @Override
                    public void onResponse(Call<ServiceBookingResponse> call, Response<ServiceBookingResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialog.dismissLoadingDialog();
                            Toast.makeText(BookServiceActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(BookServiceActivity.this, BookServiceActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                ServiceBookingResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialog.dismissLoadingDialog();
                                    showBookingSuccessDialog(server_response.getBooking());

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialog.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(BookServiceActivity.this, response.message());
                                } else {
                                    dialog.dismissLoadingDialog();
                                    //       CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ServiceBookingResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(BookServiceActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private Map<String, RequestBody> getParam() {
        if (latit.equals("")) {
            latit = "0";
            longit = "0";
        }
        Map<String, RequestBody> map = new HashMap<>();
        map.put("service", RequestBody.create(MediaType.parse("text/plain"), mServiceId));
        map.put("date", RequestBody.create(MediaType.parse("text/plain"), mDate));
        map.put("time", RequestBody.create(MediaType.parse("text/plain"), mTiming));
        map.put("address", RequestBody.create(MediaType.parse("text/plain"), tv_address.getText().toString().trim()));
        map.put("requirement", RequestBody.create(MediaType.parse("text/plain"), edt_requirement.getText().toString().trim()));
        map.put("default_address", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(isDefaultAddress)));
        map.put("status", RequestBody.create(MediaType.parse("text/plain"), BOOKING_STATUS_STARTED));
        map.put("booking_lat", RequestBody.create(MediaType.parse("text/plain"), latit));
        map.put("booking_long", RequestBody.create(MediaType.parse("text/plain"), longit));

        map.put("total", RequestBody.create(MediaType.parse("text/plain"), mBasePrice));
        map.put("night_booking", RequestBody.create(MediaType.parse("text/plain"), mNightBooking));
        return map;
    }

    private MultipartBody.Part getServicePic(String imagePath) throws IOException {
        MultipartBody.Part part = null;
        if ((imagePath != null) && (imagePath.length() > 0)) {
            File file = new File(imagePath);
            RequestBody body = RequestBody.create(MediaType.parse("" + "*/*"), file);
            part = MultipartBody.Part.createFormData("image_1", file.getName(), body);
        }
        return part;
    }

    private MultipartBody.Part[] getImages(String key, List<String> imgList) throws IOException {
        RequestBody imagesBody = null;
        MultipartBody.Part imgpart[] = null;
        if ((imgList != null) && (imgList.size() > 0)) {
            imgpart = new MultipartBody.Part[imgList.size()];
            for (int i = 0; i < imgList.size(); i++) {
                File imgFile = new File(imgList.get(i));
                imagesBody = RequestBody.create(MediaType.parse("*/*"), imgFile);
                imgpart[i] = MultipartBody.Part.createFormData(key + (i + 1), imgFile.getName(), imagesBody);

            }
        }
        return imgpart;
    }

    @Override
    public void onTimingClicked(int position) {
        Log.d(TAG, "onTimingClicked: POSITION_CLICKED=> " + position + " TIMING = " + mTimingList.get(position).getTiming());
        mTiming = mTimingList.get(position).getTiming();
        if ((position == 3))
            mNightBooking = VALUE_TRUE;
        else
            mNightBooking = VALUE_FALSE;


    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
        Log.d(TAG, "onSelectedDayChange: DATE = " + year + "-" + (month + 1) + "-" + dayOfMonth);
        mDate = year + "-" + (month + 1) + "-" + dayOfMonth;

        try {
            Date dateSelected = new SimpleDateFormat("yyyy-MM-dd").parse(mDate);
            if (mTodayDate.compareTo(dateSelected) == 0)
                getTimeSlots(true);
            else
                getTimeSlots(false);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onRemove(int pos) {
        Log.d(TAG, "onRemove: REMOVE POSITION = " + pos);
        try {
            mImageList.remove(pos);
            rv_serviceImage.getAdapter().notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "onRemove: " + e.getMessage(), e);
        }
    }

    @Override
    public void onShowLargeImage(ImageView imageView, String path) {

    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called....");
        Intent intent = new Intent(BookServiceActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}