package com.demand.keheilan.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.GPSService;
import com.demand.keheilan.util.LocaleHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import static com.demand.keheilan.util.CommonUtil.checkGPS;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ARABIC;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ENGLISH;
import static com.demand.keheilan.util.MyAppConstants.ON_BOARDING_THIRD;

public class SplashActivity extends AppCompatActivity implements GPSService.GetLocationUpdate {
    private static final String TAG = "SplashActivity";
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    public static final int RESULT_PERMISSION_LOCATION = 1;
    private static final String[] PERMISSION_LOCATION = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setLanguage();
        checkLocationPermissions();
    }

    private void setLanguage() {
        if (SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.LANG_CODE).equals(LANGUAGE_CODE_ARABIC))
            LocaleHelper.setLocale(SplashActivity.this, LANGUAGE_CODE_ARABIC);
        else
            LocaleHelper.setLocale(SplashActivity.this, LANGUAGE_CODE_ENGLISH);

    }

    public static boolean hasAccessFineLocationPermissions(final Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;

    }

    public static void requestLocationPermissions(final Activity activity) {
        ActivityCompat.requestPermissions(activity, PERMISSION_LOCATION, RESULT_PERMISSION_LOCATION);
    }

    private void checkLocationPermissions() {
        if (SplashActivity.hasAccessFineLocationPermissions(SplashActivity.this)) {
            if (checkGPS(SplashActivity.this)) {
                new GPSService(SplashActivity.this, this::getLocationUpdate);
                splashTime();
            } else {
                buildAlertMessageNoGps(getString(R.string.enable_gps));
            }
        } else {
            SplashActivity.requestLocationPermissions(this);
        }
    }

    private void buildAlertMessageNoGps(String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //denied
                Log.e("denied", permission);
                showSettingLocation(this);
            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                    Log.e("allowed", permission);
                    new GPSService(SplashActivity.this, this);
                    splashTime();
                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                    Toast.makeText(SplashActivity.this, "Grant the Location permission to use this app.", Toast.LENGTH_SHORT).show();
                    // User selected the Never Ask Again Option
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 0);
                }
            }
        }
    }

    public void showSettingLocation(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage("Turn on your GPS and give Paginazula Driver access to your location.");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                checkLocationPermissions();
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        if (!(activity).isFinishing()) {
            alert.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_PERMISSION_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        new GPSService(SplashActivity.this, this);
                        splashTime();
                        break;
                    case Activity.RESULT_CANCELED:
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                        break;
                }
                break;
            case 0:
                checkLocationPermissions();
                break;
        }
    }

    private void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDeviceToken();
                String userId = SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.USER_ID);
                if (userId != null && userId.length() > 0) {
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                } else {
                    Intent intent = new Intent(SplashActivity.this, ChooseLanguageActivity.class);
                    intent.putExtra(CAME_FROM_SCREEN, ON_BOARDING_THIRD);
                    startActivity(intent);                }
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void getDeviceToken() {
        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "run: DEVICE_TOKEN Thread is Running.....");
                    FirebaseMessaging.getInstance().getToken().
                            addOnCompleteListener(new OnCompleteListener<String>() {
                                @Override
                                public void onComplete(@NonNull Task<String> task) {
                                    if (!task.isSuccessful()) {
                                        Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                        return;
                                    }
                                    String token = task.getResult();
                                    Log.d(TAG, "onComplete: DEVICE_TOKEN=> " + token);
                                    if (token == null)
                                        getDeviceToken();
                                    else
                                        SharedPreferenceWriter.getInstance(SplashActivity.this).writeStringValue(SPreferenceKey.DEVICE_TOKEN, token);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.run();
            }
        };
        thread.start();
    }

    @Override
    public void getLocationUpdate(Double latitude, Double longitude) {
        Log.d(TAG, "getLocationUpdate: called..");
    }

}