package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;

import static com.demand.keheilan.util.MyAppConstants.APP_URL;

public class ShareActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ShareActivity";

    private ImageView iv_back;
    private TextView tv_title, tv_url;
    private Button btn_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        initView();

        tv_title.setText(getString(R.string.share_odc));
        tv_url.setText(APP_URL);
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        btn_share = findViewById(R.id.btn_share);
        tv_url = findViewById(R.id.tv_url);

        iv_back.setOnClickListener(this);
        btn_share.setOnClickListener(this);
        tv_url.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.btn_share) {
            shareApp();
        } else if (id == R.id.tv_url) {
            copyToClipboard();
        }
    }

    private void copyToClipboard() {
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("Khailn live url", APP_URL);
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(this, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
    }

    private void shareApp() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Share App");
        shareIntent.putExtra(Intent.EXTRA_TEXT, APP_URL);
        startActivity(Intent.createChooser(shareIntent, "Share via"));
    }
}