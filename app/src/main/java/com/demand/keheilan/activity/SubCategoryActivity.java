package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.SubCategoryAdapter;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.SubCategoryResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.CATEGORY_ID;
import static com.demand.keheilan.util.MyAppConstants.CATEGORY_NAME;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.SUBCATEGORY_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.SUB_CATEGORY_ID;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class SubCategoryActivity extends AppCompatActivity implements SubCategoryAdapter.OnSubCategoryClick, View.OnClickListener,
             CommonUtil.LogoutUser{
    private static final String TAG = "SubCategoryActivity";
    private TextView tv_searchTitle;
    private ImageView iv_back, iv_search;
    private RecyclerView rv_subCategory;
    private EditText edt_search;

    private List<SubCategoryResponse.DataBean> mSubCategoryList;
    private List<SubCategoryResponse.DataBean> mSearchList;
    private String mCategoryId, mSearchText;
    private SearchSubCategory searchSubCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        initView();
        mSubCategoryList = new ArrayList<>();
        mSearchList = new ArrayList<>();
        Intent extraData = getIntent();
        if (extraData != null) {
            if (extraData.hasExtra(CATEGORY_ID)) {
                Log.d(TAG, "onCreate: CATEGORY ID RECEIVED => " + extraData.getStringExtra(CATEGORY_ID));
                mCategoryId = extraData.getStringExtra(CATEGORY_ID);
                tv_searchTitle.setText(extraData.getStringExtra(CATEGORY_NAME));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called..");
        if (mCategoryId != null && mCategoryId.length() > 0) {
            serviceSubCategory(mCategoryId);
        }
    }

    private void initView() {
        tv_searchTitle = findViewById(R.id.tv_searchTitle);
        rv_subCategory = findViewById(R.id.rv_subCategory);
        iv_back = findViewById(R.id.iv_back);
        edt_search = findViewById(R.id.edt_search);
        iv_search = findViewById(R.id.iv_search);

        iv_back.setOnClickListener(this);

        searchSubCategory = new SearchSubCategory();

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    CommonUtil.hideKeyBoard(SubCategoryActivity.this, edt_search);
                    if (mSearchText.length() > 0) {
                        startSearching();
                    }
                    return true;
                }
                return false;
            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mSearchText = s.toString().trim();
                if (mSearchText.length() == 0) {
                    iv_search.setVisibility(View.GONE);
                    setSubCategoryAdapter(mSubCategoryList);
                    CommonUtil.hideKeyBoard(SubCategoryActivity.this, edt_search);
                } else {
                    iv_search.setVisibility(View.GONE);
                    startSearching();
                }

            }
        });

    }

    private void startSearching() {
        if (searchSubCategory.getStatus() != AsyncTask.Status.RUNNING) {
            Log.d(TAG, "onCameraIdle: ASYNC Task is Not Running");
            searchSubCategory = new SearchSubCategory();
            searchSubCategory.execute();
        } else {
            Log.d(TAG, "onCameraIdle: ASYNC TASK RUNNING");
            searchSubCategory.cancel(true);
            searchSubCategory = new SearchSubCategory();
            searchSubCategory.execute();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        }
    }

    private void setSubCategoryAdapter(List<SubCategoryResponse.DataBean> subCategories) {
        rv_subCategory.setLayoutManager(new GridLayoutManager(this, 3));
        rv_subCategory.setAdapter(new SubCategoryAdapter(SubCategoryActivity.this, subCategories, this));
    }

    private void serviceSubCategory(String categoryId) {
        {
            if (new InternetCheck(this).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(SubCategoryActivity.this);
                dialogPopup.showLoadingDialog(SubCategoryActivity.this, "");
                String token = SharedPreferenceWriter.getInstance(SubCategoryActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SubCategoryResponse> call = api_service.subCategoryList(TOKEN_KEY + token, categoryId);
                call.enqueue(new Callback<SubCategoryResponse>() {
                    @Override
                    public void onResponse(Call<SubCategoryResponse> call, Response<SubCategoryResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(SubCategoryActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(SubCategoryActivity.this, SubCategoryActivity.this);
                        }else {
                            if (response.isSuccessful()) {
                                SubCategoryResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<SubCategoryResponse.DataBean> subCategories = (ArrayList<SubCategoryResponse.DataBean>) server_response.getData();
                                    if (subCategories != null && subCategories.size() > 0) {
                                        mSubCategoryList.clear();
                                        mSubCategoryList.addAll(subCategories);
                                    }
                                    setSubCategoryAdapter(mSubCategoryList);
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(SubCategoryActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<SubCategoryResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(SubCategoryActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(SubCategoryActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    @Override
    public void onSubCategoryClick(String subCategoryId) {
        Log.d(TAG, "onSubCategoryClick: called.. SUB CATEGORY ID=> " + subCategoryId);
        Intent intent = new Intent(SubCategoryActivity.this, RecentSearchesActivity.class);
        intent.putExtra(CAME_FROM_SCREEN, SUBCATEGORY_SCREEN);
        intent.putExtra(SUB_CATEGORY_ID, subCategoryId);
        startActivity(intent);
    }

    private class SearchSubCategory extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSearchList.clear();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setSubCategoryAdapter(mSearchList);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (SubCategoryResponse.DataBean subCategory : mSubCategoryList) {
                if (subCategory.getSub_category_name().toLowerCase().contains(mSearchText.toLowerCase()))
                    mSearchList.add(subCategory);
            }
            return null;
        }
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called...");
        Intent intent = new Intent(SubCategoryActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}