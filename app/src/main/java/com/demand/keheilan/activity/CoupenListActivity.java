package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.CouponListAdapter;
import com.demand.keheilan.responsemodel.CoupenListResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.RESULT;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class CoupenListActivity extends AppCompatActivity implements View.OnClickListener, CommonUtil.LogoutUser, CouponListAdapter.OnCouponClicked {
    private static final String TAG = "CoupenListActivity";
    private ImageView iv_back;
    private TextView tv_title, tv_couponCode, tv_noData;
    private Button btn_apply;
    private RecyclerView rv_couponList;

    private List<CoupenListResponse.Coupen> mCouponsList;
    private String mSelectedCouponId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupen_list);
        initView();
        tv_title.setText(getString(R.string.coupon_list));
        mCouponsList = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceCoupons();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_couponCode = findViewById(R.id.tv_couponCode);
        btn_apply = findViewById(R.id.btn_apply);
        rv_couponList = findViewById(R.id.rv_couponList);
        tv_noData = findViewById(R.id.tv_noData);

        iv_back.setOnClickListener(this);
        btn_apply.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        }else if (id == R.id.btn_apply){
            setResultAndFinish(mSelectedCouponId);
        }
    }

    private void serviceCoupons() {
        {
            if (new InternetCheck(CoupenListActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(CoupenListActivity.this);
                dialogPopup.showLoadingDialog(CoupenListActivity.this, "");
                String token = SharedPreferenceWriter.getInstance(CoupenListActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CoupenListResponse> call = api_service.coupons(TOKEN_KEY + token);
                call.enqueue(new Callback<CoupenListResponse>() {
                    @Override
                    public void onResponse(Call<CoupenListResponse> call, Response<CoupenListResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(CoupenListActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(CoupenListActivity.this, CoupenListActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CoupenListResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<CoupenListResponse.Coupen> coupons = (ArrayList<CoupenListResponse.Coupen>) server_response.getData();
                                    mCouponsList.clear();
                                    if (coupons != null && coupons.size() > 0) {
                                        tv_noData.setVisibility(View.GONE);
                                        mCouponsList.addAll(coupons);
                                    } else {
                                        tv_noData.setVisibility(View.VISIBLE);
                                        Log.d(TAG, "onResponse: COUPONS NOT FOUND");
                                    }
                                    setCouponAdapter();

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(CoupenListActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(CoupenListActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CoupenListResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(CoupenListActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(CoupenListActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void setCouponAdapter() {
        rv_couponList.setLayoutManager(new LinearLayoutManager(CoupenListActivity.this));
        rv_couponList.setAdapter(new CouponListAdapter(CoupenListActivity.this, mCouponsList, this));
    }

    @Override
    public void onCouponSelected(String couponId, String couponCode) {
        Log.d(TAG, "onCouponSelected: called..COUPON ID = " + couponId + " COUPON CODE = " + couponCode);
        mSelectedCouponId = couponId;
        tv_couponCode.setText(couponCode);

    }

    private void setResultAndFinish(String couponId) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(RESULT, couponId);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called..");
        Intent intent = new Intent(CoupenListActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}