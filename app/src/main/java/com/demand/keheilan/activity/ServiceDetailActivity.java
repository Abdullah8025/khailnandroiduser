package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.ServiceDetailResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.SERVICE_BASE_PRICE;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ID;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class ServiceDetailActivity extends AppCompatActivity implements View.OnClickListener, CommonUtil.LogoutUser {
    private static final String TAG = "ServiceDetailActivity";
    private ImageView iv_back, iv_background, iv_front;
    private Button btn_service;
    private RatingBar ratingBar_service;
    private TextView tv_serviceName, tv_review, tv_amount, tv_fieldOne, tv_fieldTwo, tv_fieldThree, tv_fieldFour, tv_seeMore;
    private String mServiceId, mBasePrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        initView();
        Intent extraData = getIntent();
        if (extraData != null) {
            if (extraData.hasExtra(SERVICE_ID)) {
                Log.d(TAG, "onCreate: SERVICE_ID=> " + extraData.getStringExtra(SERVICE_ID));
                mServiceId = extraData.getStringExtra(SERVICE_ID);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called..");
        if (mServiceId != null && mServiceId.length() > 0)
            serviceServiceDetail(mServiceId);
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        btn_service = findViewById(R.id.btn_service);
        iv_background = findViewById(R.id.iv_background);
        iv_front = findViewById(R.id.iv_front);
        ratingBar_service = findViewById(R.id.ratingBar_service);
        tv_serviceName = findViewById(R.id.tv_serviceName);
        tv_review = findViewById(R.id.tv_review);
        tv_amount = findViewById(R.id.tv_amount);
        tv_fieldOne = findViewById(R.id.tv_fieldOne);
        tv_fieldTwo = findViewById(R.id.tv_fieldTwo);
        tv_fieldThree = findViewById(R.id.tv_fieldThree);
        tv_fieldFour = findViewById(R.id.tv_fieldFour);
        tv_seeMore = findViewById(R.id.tv_seeMore);

        btn_service.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_seeMore.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.btn_service) {
            if (CommonUtil.isUserLoggedIn(ServiceDetailActivity.this))
                openBookService();
            else
                CommonUtil.showLoginAlert(ServiceDetailActivity.this, this);
        } else if (id == R.id.tv_seeMore) {
            openReviewActivity();
        }
    }

    private void openBookService() {
        Intent intent = new Intent(ServiceDetailActivity.this, BookServiceActivity.class);
        intent.putExtra(SERVICE_ID, mServiceId);
        intent.putExtra(SERVICE_BASE_PRICE, mBasePrice);
        startActivity(intent);
    }

    private void openReviewActivity() {
        Intent intent = new Intent(ServiceDetailActivity.this, ReviewRatingActivity.class);
        intent.putExtra(SERVICE_ID, mServiceId);
        startActivity(intent);
    }

    private void serviceServiceDetail(String serviceId) {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(ServiceDetailActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(ServiceDetailActivity.this);
                dialogPopup.showLoadingDialog(ServiceDetailActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ServiceDetailResponse> call = api_service.serviceDetail(TOKEN_KEY + token, serviceId);
                call.enqueue(new Callback<ServiceDetailResponse>() {
                    @Override
                    public void onResponse(Call<ServiceDetailResponse> call, Response<ServiceDetailResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(ServiceDetailActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(ServiceDetailActivity.this, ServiceDetailActivity.this);
                        }else {
                            if (response.isSuccessful()) {
                                ServiceDetailResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();

                                    setServiceDetailData(server_response);

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(ServiceDetailActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<ServiceDetailResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(ServiceDetailActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(ServiceDetailActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void setServiceDetailData(ServiceDetailResponse server_response) {
        Glide.with(ServiceDetailActivity.this).
                load(server_response.getImage_2())
                .placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(iv_background);

        Glide.with(ServiceDetailActivity.this).
                load(server_response.getImage_1())
                .placeholder(R.drawable.profile_placeholder).
                error(R.drawable.profile_placeholder).
                into(iv_front);
        tv_serviceName.setText(server_response.getService_name());
        tv_review.setText(getString(R.string.num_reviews, server_response.getRating_count()));
        ratingBar_service.setRating(server_response.getAverage_rating());
        tv_amount.setText(getString(R.string.num_amount, server_response.getBase_price()));
        tv_fieldOne.setText(server_response.getField_1());
        tv_fieldTwo.setText(server_response.getField_2());
        tv_fieldThree.setText(server_response.getField_3());
        tv_fieldFour.setText(server_response.getField_4());

        mBasePrice = server_response.getBase_price();

    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called....");
        Intent intent = new Intent(ServiceDetailActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}