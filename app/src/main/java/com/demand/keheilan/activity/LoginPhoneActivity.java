package com.demand.keheilan.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.model.ChangeLanguageModel;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.GuestLoginResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;
import com.hbb20.CountryCodePicker;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.CLICKED_ON_TAB;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ARABIC;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ENGLISH;
import static com.demand.keheilan.util.MyAppConstants.PRIVACY_POLICY_TAB;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;

import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TERMS_AND_CONDITIONS_TAB;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class LoginPhoneActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "LoginPhoneActivity";
    private ProgressBar progressBar_login;
    private ImageView imageView4;
    private TextView textView12, tv_skip,tv_link;
    private TextView btn_continue;
    private CountryCodePicker ccp;
    private EditText edt_number;
    private CheckBox checkBox;

    Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            progressBar_login.setProgress((Integer) msg.obj);
            if (100 == (Integer) msg.obj) {
                imageView4.setImageDrawable(ContextCompat.getDrawable(LoginPhoneActivity.this, R.drawable.bg_circle_yellow));
                textView12.setTextColor(ContextCompat.getColor(LoginPhoneActivity.this, R.color.black));

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_phone);
        initView();
        createSpan();
    }

    private void createSpan() {
        SpannableString spannableString = new SpannableString(getString(R.string.i_agree_to_the_terms_and_condition_and_privacy_policy));

        ClickableSpan termsClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                openWebViewActivity(TERMS_AND_CONDITIONS_TAB);
            }
        };

        ClickableSpan privacyClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                openWebViewActivity(PRIVACY_POLICY_TAB);
            }
        };

        if (SharedPreferenceWriter.getInstance(LoginPhoneActivity.this).getString(SPreferenceKey.LANG_CODE).equals(LANGUAGE_CODE_ENGLISH)) {
            spannableString.setSpan(termsClickableSpan, 15, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(privacyClickableSpan, 39, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(LoginPhoneActivity.this, R.color.color_blue)), 15, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(LoginPhoneActivity.this, R.color.color_blue)), 39, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        } else if (SharedPreferenceWriter.getInstance(LoginPhoneActivity.this).getString(SPreferenceKey.LANG_CODE).equals(LANGUAGE_CODE_ARABIC)) {
            spannableString.setSpan(termsClickableSpan, 10, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(privacyClickableSpan, 27, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(LoginPhoneActivity.this, R.color.color_blue)), 10, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(LoginPhoneActivity.this, R.color.color_blue)), 27, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        tv_link.setText(spannableString);
        tv_link.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: SIGNUP MODEL => " + SignUpModel.getInstance().toString());
        showProgress();

    }

    private void openWebViewActivity(String type) {
        Intent intent = new Intent(LoginPhoneActivity.this, WebViewActivity.class);
        if (type.equals(PRIVACY_POLICY_TAB))
            intent.putExtra(CLICKED_ON_TAB, PRIVACY_POLICY_TAB);
        else if (type.equals(TERMS_AND_CONDITIONS_TAB))
            intent.putExtra(CLICKED_ON_TAB, TERMS_AND_CONDITIONS_TAB);

        startActivity(intent);
    }

    private void showProgress() {
        Thread progressThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= 100; i++) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Message message = Message.obtain();
                    message.obj = i;
                    mHandler.sendMessage(message);
                }
            }
        });
        progressThread.start();

    }

    private void initView() {
        progressBar_login = findViewById(R.id.progressBar_login);
        imageView4 = findViewById(R.id.imageView4);
        textView12 = findViewById(R.id.textView12);
        tv_skip = findViewById(R.id.tv_skip);
        btn_continue = findViewById(R.id.btn_continue);
        ccp = findViewById(R.id.ccp);
        edt_number = findViewById(R.id.edt_number);
        checkBox = findViewById(R.id.checkBox);
        tv_link=findViewById(R.id.tv_link);

        btn_continue.setOnClickListener(this);
        tv_skip.setOnClickListener(this);


       // ccp.setAutoDetectedCountry(true);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_continue) {
            if (validatePhone()) {
                SignUpModel.getInstance().setCountryCode(ccp.getSelectedCountryCodeWithPlus());
                SignUpModel.getInstance().setPhoneNumber(edt_number.getText().toString().trim());
                startActivity(new Intent(LoginPhoneActivity.this, OtpVerificationActivity.class));
            }
        } else if (id == R.id.tv_skip) {
            serviceGuestLogin();
        }

    }

    private boolean validatePhone() {
        if (!CommonUtil.blankValidation(edt_number.getText().toString())) {
            CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.validate_number));
            return false;
        }
        if (edt_number.getText().toString().length() < 7 || edt_number.getText().toString().length() > 11) {
            CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.validate_number));
            return false;
        }
        if (!checkBox.isChecked()) {
            CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.validate_terms_conditions));
            return false;
        }
        return true;
    }

    private void serviceGuestLogin() {
        {
            if (new InternetCheck(LoginPhoneActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(LoginPhoneActivity.this);
                dialogPopup.showLoadingDialog(LoginPhoneActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<GuestLoginResponse> call = api_service.guestLogin();
                call.enqueue(new Callback<GuestLoginResponse>() {
                    @Override
                    public void onResponse(Call<GuestLoginResponse> call, Response<GuestLoginResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.session_expired));
                        } else {
                            if (response.isSuccessful()) {
                                GuestLoginResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();

                                    SharedPreferenceWriter.getInstance(LoginPhoneActivity.this).writeStringValue(SPreferenceKey.TOKEN, server_response.getToken());
                                    SharedPreferenceWriter.getInstance(LoginPhoneActivity.this).writeBooleanValue(SPreferenceKey.IS_GUEST_USER, true);
                                    serviceUpdateLanguage(server_response.getToken());

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GuestLoginResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceUpdateLanguage(String userToken) {
        {
            if (new InternetCheck(LoginPhoneActivity.this).isConnect()) {
                DialogPopup dialog = new DialogPopup(LoginPhoneActivity.this);
                dialog.showLoadingDialog(LoginPhoneActivity.this, "");
                String langCode=SharedPreferenceWriter.getInstance(LoginPhoneActivity.this).getString(SPreferenceKey.LANG_CODE);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();

                ChangeLanguageModel changeLanguageModel = new ChangeLanguageModel(langCode);

                Call<CommonResponse> call = api_service.updateLanguage(TOKEN_KEY + userToken, changeLanguageModel);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.isSuccessful()) {
                            CommonResponse server_response = response.body();
                            if (server_response.getStatus() == STATUS_SUCCESS) {
                                dialog.dismissLoadingDialog();
                                startActivity(new Intent(LoginPhoneActivity.this, HomeActivity.class));
                            } else if (server_response.getStatus() == STATUS_FAILURE) {
                                dialog.dismissLoadingDialog();
                                Log.d(TAG, "onResponse: " + response.message());
                            } else {
                                dialog.dismissLoadingDialog();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(LoginPhoneActivity.this, getString(R.string.internet_lost));
            }
        }
    }
}