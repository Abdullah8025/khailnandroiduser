package com.demand.keheilan.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.demand.keheilan.R;
import com.demand.keheilan.adapter.AllServicesAdapter;
import com.demand.keheilan.adapter.PastServiceAdapter;
import com.demand.keheilan.adapter.UpcomingServiceAdapter;
import com.demand.keheilan.fragment.BookingsFragment;
import com.demand.keheilan.fragment.HelpFragment;
import com.demand.keheilan.fragment.HomeFragment;
import com.demand.keheilan.fragment.MoreFragment;
import com.demand.keheilan.responsemodel.HomeApiResponse;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.SwitchFragment;


import static com.demand.keheilan.util.CommonUtil.showSnackBar;
import static com.demand.keheilan.util.MyAppConstants.ACTION_NOTIFICATION_TAP;
import static com.demand.keheilan.util.MyAppConstants.BASE_PRICE;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_FRAGMENT_TAG;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_ID;
import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.FIREBASE_MESSAGING_SERVICE;
import static com.demand.keheilan.util.MyAppConstants.HELP_FRAGMENT_TAG;
import static com.demand.keheilan.util.MyAppConstants.HOME_FRAGMENT_TAG;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_ITEM;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.MESSAGE_BODY;
import static com.demand.keheilan.util.MyAppConstants.MESSAGE_TITLE;
import static com.demand.keheilan.util.MyAppConstants.MORE_FRAGMENT_TAG;
import static com.demand.keheilan.util.MyAppConstants.OPEN_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.PAST_ITEM;
import static com.demand.keheilan.util.MyAppConstants.PAST_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.RESULT;
import static com.demand.keheilan.util.MyAppConstants.SELECTED_ADDRESS;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ACCEPTED;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_BASE_PRICE;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ID;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_REJECTED;
import static com.demand.keheilan.util.MyAppConstants.TOTAL_PRICE;
import static com.demand.keheilan.util.MyAppConstants.UPCOMING_ITEM;
import static com.demand.keheilan.util.MyAppConstants.UPCOMING_SERVICES;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, SwitchFragment.SetHeader,
        PastServiceAdapter.OnPastServiceClicked, AllServicesAdapter.OnAllServiceClick, UpcomingServiceAdapter.OnBookingClicked, CommonUtil.LogoutUser {
    private static final String TAG = "HomeActivity";
    private TextView tv_home_bottom, tv_booking_bottom, tv_help_bottom, tv_more_bottom;

    private HomeFragment mHomeFragment = new HomeFragment();
    private BookingsFragment mBookingsFragment = new BookingsFragment();
    private HelpFragment mHelpFragment = new HelpFragment();
    private MoreFragment mMoreFragment = new MoreFragment();

    private boolean mExit = false;
    private String mFragmentTag;
    public static final int UPCOMING_DETAIL_REQUEST_CODE = 2;
    public static final int RATE_REQUEST = 4;
    public static final int MAP_ADDRESS_REQUEST = 5;

    private final BroadcastReceiver mPushNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: called..");
            if (intent != null) {

//                Bundle bundle = intent.getExtras();
//                BookingsFragment bookingsFragment =new BookingsFragment();
//                bookingsFragment.setArguments(bundle);
//                loadFragment(bookingsFragment);


//                loadFragment(mBookingsFragment);
//                BookingsFragment bookingFragment = (BookingsFragment) getSupportFragmentManager().findFragmentById(R.id.replaceFrame);
//
//                if (bookingFragment != null){
//                    if (bundle.getString(OPEN_SERVICES).equals(UPCOMING_SERVICES)) {
//                        bookingFragment.refreshUpComingServices();
//                    } else if (bundle.getString(OPEN_SERVICES).equals(IN_PROCESS_SERVICES)) {
//                        bookingFragment.refreshProcessServices();
//                    } else if (bundle.getString(OPEN_SERVICES).equals(PAST_SERVICES)) {
//                        bookingFragment.refreshPastList();
//                    }
//                }else {
//                    Log.d(TAG, "onReceive: BOOKING FRAGMENT NULL..");
//                }

               
            } else {
                Log.d(TAG, "onReceive: INTENT DATA NULL ....");
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        loadFragment(mHomeFragment);

        if (getIntent() !=null && getIntent().hasExtra(FIREBASE_MESSAGING_SERVICE)){
            Log.d(TAG, "onCreate: PUSH NOTIFICATION DATA FOUND");
           Bundle bundle = getIntent().getExtras();
            mBookingsFragment.setArguments(bundle);
            loadFragment(mBookingsFragment);
        }else {
            Log.d(TAG, "onCreate: EXTRA DATA NULL");
            loadFragment(mHomeFragment);
        }

    }

    private void initView() {
        tv_home_bottom = findViewById(R.id.tv_home_bottom);
        tv_booking_bottom = findViewById(R.id.tv_booking_bottom);
        tv_help_bottom = findViewById(R.id.tv_help_bottom);
        tv_more_bottom = findViewById(R.id.tv_more_bottom);

        tv_home_bottom.setOnClickListener(this);
        tv_booking_bottom.setOnClickListener(this);
        tv_help_bottom.setOnClickListener(this);
        tv_more_bottom.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called...REGISTERING BROADCAST RECEIVER");
     //   registerReceiver(mPushNotificationReceiver, new IntentFilter(ACTION_NOTIFICATION_TAP));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_home_bottom) {
            loadFragment(mHomeFragment);
        } else if (id == R.id.tv_booking_bottom) {
            loadFragment(mBookingsFragment);
        } else if (id == R.id.tv_help_bottom) {
            loadFragment(mHelpFragment);
        } else if (id == R.id.tv_more_bottom) {
            loadFragment(mMoreFragment);
        }
    }

    private void loadFragment(Fragment fragment) {
        if (fragment instanceof HomeFragment) {
            mFragmentTag = HOME_FRAGMENT_TAG;
        } else if (fragment instanceof BookingsFragment) {
            mFragmentTag = BOOKING_FRAGMENT_TAG;
        } else if (fragment instanceof HelpFragment) {
            mFragmentTag = HELP_FRAGMENT_TAG;
        } else if (fragment instanceof MoreFragment) {
            mFragmentTag = MORE_FRAGMENT_TAG;
        }
        if (CommonUtil.isUserLoggedIn(HomeActivity.this)) {
            setBottom(mFragmentTag);
            SwitchFragment.replaceFragment(getSupportFragmentManager(), fragment, mFragmentTag, true, true, this);
        } else {
            if (mFragmentTag.equals(BOOKING_FRAGMENT_TAG) || mFragmentTag.equals(HELP_FRAGMENT_TAG)) {
                CommonUtil.showLoginAlert(HomeActivity.this, this);
            } else {
                setBottom(mFragmentTag);
                SwitchFragment.replaceFragment(getSupportFragmentManager(), fragment, mFragmentTag, true, true, this);
            }
        }

    }


    @Override
    public void setHeader(String fragmentTag) {

    }

    private void setBottom(String fragmentTag) {
        if (fragmentTag.equals(HOME_FRAGMENT_TAG)) {
            tv_home_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.home_new_y), null, null);
            tv_home_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.color_yellow));
        } else {
            tv_home_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.home_new_g), null, null);
            tv_home_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.white));
        }
        if (fragmentTag.equals(BOOKING_FRAGMENT_TAG)) {
            tv_booking_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.book_new_y), null, null);
            tv_booking_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.color_yellow));
        } else {
            tv_booking_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.book_new_g), null, null);
            tv_booking_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.white));
        }

        if (fragmentTag.equals(HELP_FRAGMENT_TAG)) {
            tv_help_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.help_new_y), null, null);
            tv_help_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.color_yellow));
        } else {
            tv_help_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.help_new_g), null, null);
            tv_help_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.white));
        }

        if (fragmentTag.equals(MORE_FRAGMENT_TAG)) {
            tv_more_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.more_new_y), null, null);
            tv_more_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.color_yellow));
        } else {
            tv_more_bottom.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(HomeActivity.this, R.drawable.more_new_g), null, null);
            tv_more_bottom.setTextColor(ContextCompat.getColor(HomeActivity.this, R.color.white));
        }


    }

    @Override
    public void onRatingClicked(String serviceId) {
        Log.d(TAG, "onRatingClicked: called.. SERVICE ID => " + serviceId);
        Intent intent = new Intent(HomeActivity.this, RateActivity.class);
        intent.putExtra(SERVICE_ID, serviceId);
        startActivityForResult(intent, RATE_REQUEST);
    }

    @Override
    public void onRebookingClicked(String serviceId, String basePrice) {
        Log.d(TAG, "onRebookingClicked: called...SERVICE ID = " + serviceId + " BASE PRICE = " + basePrice);
        openBookService(serviceId, basePrice);
    }


//    @Override
//    public void onServiceClick() {
//        Log.d(TAG, "onServiceClick: called...");
//        startActivity(new Intent(HomeActivity.this, ServiceDetailActivity.class));
//    }

    @Override
    public void onAllServiceClick(HomeApiResponse.ServicesBean allservice) {
        Log.d(TAG, "onAllServiceClick: ALL SERVICE ITEM => " + allservice.toString());
        openServiceDetail(allservice.getId());


    }

    @Override
    public void onUpcomingBookingClicked(String bookingId, String basePrice) {
        Log.d(TAG, "onUpcomingBookingClicked: Booiking Id = " + bookingId + " Base Price = " + basePrice);
        Intent intent = new Intent(HomeActivity.this, AcceptServiceActivity.class);
        intent.putExtra(CAME_FROM_SCREEN, UPCOMING_ITEM);
        intent.putExtra(BOOKING_ID, bookingId);
        intent.putExtra(BASE_PRICE, basePrice);
        startActivityForResult(intent, UPCOMING_DETAIL_REQUEST_CODE);
    }

    @Override
    public void onInProcessServiceClicked(String bookingId, String basePrice) {
        Log.d(TAG, "onInProcessServiceClicked: BOOKING ID" + bookingId + " Base Price = " + basePrice);
        Intent intent = new Intent(HomeActivity.this, AcceptServiceActivity.class);
        intent.putExtra(CAME_FROM_SCREEN, IN_PROCESS_ITEM);
        intent.putExtra(BOOKING_ID, bookingId);
        intent.putExtra(BASE_PRICE, basePrice);
        startActivity(intent);
    }

    @Override
    public void onPastServiceClicked(String bookingId, String price) {
        Log.d(TAG, "onPastServiceClicekd: BOOKING ID" + bookingId + " Base Price = " + price);

        Intent intent = new Intent(HomeActivity.this, AcceptServiceActivity.class);
        intent.putExtra(CAME_FROM_SCREEN, PAST_ITEM);
        intent.putExtra(BOOKING_ID, bookingId);
        intent.putExtra(TOTAL_PRICE, price);
        startActivity(intent);
    }

    //    @Override
//    public void onTopServiceClick(HomeApiResponse.TopServicesBean topServicesBean) {
//        Log.d(TAG, "onTopServiceClick: TOP SERVICE => " + topServicesBean.toString());
//        openServiceDetail(topServicesBean.getId());
//
//    }
//
//    @Override
//    public void onCategoryClick(String categoryId,String categoryName) {
//        Log.d(TAG, "onCategoryClick: called...CATEGORY_ID => " + categoryId);
//        Intent intent = new Intent(HomeActivity.this, SubCategoryActivity.class);
//        intent.putExtra(CATEGORY_ID, categoryId);
//        intent.putExtra(CATEGORY_NAME,categoryName);
//        startActivity(intent);
//    }

    private void openServiceDetail(String serviceId) {
        Intent intent = new Intent(HomeActivity.this, ServiceDetailActivity.class);
        intent.putExtra(SERVICE_ID, serviceId);
        startActivity(intent);
    }

    private void openBookService(String serviceId, String basePrice) {
        Intent intent = new Intent(HomeActivity.this, BookServiceActivity.class);
        intent.putExtra(SERVICE_ID, serviceId);
        intent.putExtra(SERVICE_BASE_PRICE, basePrice);
        startActivity(intent);
    }

    public void getMapAddress() {
        Intent intent = new Intent(HomeActivity.this, AddressPickActivity.class);
        startActivityForResult(intent, MAP_ADDRESS_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPCOMING_DETAIL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: RESULT = " + data.getStringExtra(RESULT));
                if (data.getStringExtra(RESULT).equals(SERVICE_ACCEPTED))
                    mBookingsFragment.refreshProcessServices();
                else if (data.getStringExtra(RESULT).equals(SERVICE_REJECTED))
                    mBookingsFragment.refreshUpComingServices();

            }
        }
        if (requestCode == RATE_REQUEST) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "onActivityResult: RATE REQUEST RESULT OK ");
                mBookingsFragment.refreshPastList();
            }
        }
        if (requestCode == MAP_ADDRESS_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Log.d(TAG, "onActivityResult: ADDRESS = " + data.getStringExtra(SELECTED_ADDRESS));
                    mHomeFragment.setHomeAddress(data.getStringExtra(SELECTED_ADDRESS));

                }
            }
        }

    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called..");
        Intent intent = new Intent(HomeActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown: back stack entry count is ==" + getSupportFragmentManager().getBackStackEntryCount());
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            SwitchFragment.replaceFragment(getSupportFragmentManager(), mHomeFragment, HOME_FRAGMENT_TAG, true, true, this);
            setBottom(HOME_FRAGMENT_TAG);
        } else {
            if (mExit) {
                finishAffinity(); // finish activity
            } else {
                showSnackBar(this, "Press Back again to Exit.");
                mExit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mExit = false;
                    }
                }, 3 * 1000);
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: called..");

//        if (mPushNotificationReceiver != null)
//            unregisterReceiver(mPushNotificationReceiver);


    }
}