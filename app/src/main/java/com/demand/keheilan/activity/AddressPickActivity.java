package com.demand.keheilan.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.AsyncGetLatLong;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.GPSService;
import com.demand.keheilan.util.OnLocationSelect;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.demand.keheilan.util.MyAppConstants.BOOKING_PLACE_CITY;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_PLACE_LAT;
import static com.demand.keheilan.util.MyAppConstants.BOOKING_PLACE_LONGI;
import static com.demand.keheilan.util.MyAppConstants.DEFAULT_ADDRESS;
import static com.demand.keheilan.util.MyAppConstants.SELECTED_ADDRESS;

public class AddressPickActivity extends AppCompatActivity implements OnMapReadyCallback, GPSService.GetLocationUpdate, View.OnClickListener {
    private static final String TAG = "AddressPickActivity";
    private GoogleMap mMap;
    Marker marker = null;
    Geocoder geocoder;
    private GetAddress getAddress;
    private TextView tv_showAddress, tv_searchAddress;
    private Button btn_save;
    private ImageView iv_back;

    private boolean isPlaceSearch = false;
    private String mAddress, mCityName;
    private String latss = "";
    private String longs = "";
    private LatLng latLng;

    private static int AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_pick);
        initView();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    private void initView() {
        tv_showAddress = findViewById(R.id.tv_showAddress);
        tv_searchAddress = findViewById(R.id.tv_searchAddress);
        btn_save = findViewById(R.id.btn_save);
        iv_back = findViewById(R.id.iv_back);

        tv_searchAddress.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tv_searchAddress) {
            Places.initialize(getApplicationContext(), getString(R.string.google_places_key));
            PlacesClient placesClient = Places.createClient(this);
            List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.ADDRESS);

            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                    .build(this);
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
        } else if (id == R.id.btn_save) {
            setResultAndFinish(true);
        } else if (id == R.id.iv_back) {
            setResultAndFinish(false);
        }
    }

    private void setResultAndFinish(boolean saveAddress) {
        Intent returnIntent = new Intent();
        if (saveAddress) {
            returnIntent.putExtra(SELECTED_ADDRESS, mAddress);
            returnIntent.putExtra(BOOKING_PLACE_LAT, latss);
            returnIntent.putExtra(BOOKING_PLACE_LONGI, longs);
            returnIntent.putExtra(BOOKING_PLACE_CITY,mCityName);
            setResult(RESULT_OK, returnIntent);
        } else {
            setResult(RESULT_CANCELED,returnIntent);
//            returnIntent.putExtra(SELECTED_ADDRESS, "");
//            returnIntent.putExtra(BOOKING_PLACE_LAT, "");
//            returnIntent.putExtra(BOOKING_PLACE_LONGI, "");
        }

        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            CommonUtil.hideKeyBoard(AddressPickActivity.this, tv_searchAddress);
            if (resultCode == RESULT_OK) {
                // isPlaceSearch = true;
                Place place = Autocomplete.getPlaceFromIntent(data);

                Log.d(TAG, "Place: " + place.getAddress() + ", " + place.getLatLng());
//                mAddress = place.getAddress();

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15.0f));


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                CommonUtil.showSnackBar(AddressPickActivity.this, getString(R.string.on_failure));
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult: called...");
                // The user canceled the operation.
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: called....");
        mMap = googleMap;

        String latitude = SharedPreferenceWriter.getInstance(AddressPickActivity.this).getString(SPreferenceKey.LATITUDE);
        String longitude = SharedPreferenceWriter.getInstance(AddressPickActivity.this).getString(SPreferenceKey.LONGITUDE);
        if (latitude.isEmpty()) {

        } else {
            LatLng currentLocation = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
            if (latitude != null && longitude != null) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15.0f));
                marker = mMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.address_pointer)));
            } else {
                new GPSService(AddressPickActivity.this, this);
            }
        }


        geocoder = new Geocoder(AddressPickActivity.this, Locale.getDefault());

        getAddress = new GetAddress();
        getAddress.execute();

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                marker.setPosition(mMap.getCameraPosition().target);
            }
        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Log.d(TAG, "onCameraIdle: called..");

                if (getAddress.getStatus() != AsyncTask.Status.RUNNING) {
                    Log.d(TAG, "onCameraIdle: ASYNC Task is Not Running");
                    getAddress = new GetAddress();
                    getAddress.execute();
                } else {
                    Log.d(TAG, "onCameraIdle: ASYNC TASK RUNNING");
                    getAddress.cancel(true);
                    getAddress = new GetAddress();
                    getAddress.execute();
                }

//                if (isPlaceSearch) {
//                    Log.d(TAG, "onCameraIdle: IS PLACE API SEARCH = TRUE");
//                    tv_showAddress.setText("");
//                } else {
//                    Log.d(TAG, "onCameraIdle: IS PLACE API SEARCH = FALSE");
//                    if (getAddress.getStatus() != AsyncTask.Status.RUNNING) {
//                        Log.d(TAG, "onCameraIdle: ASYNC Task is Not Running");
//                        getAddress = new GetAddress();
//                        getAddress.execute();
//                    } else {
//                        Log.d(TAG, "onCameraIdle: ASYNC TASK RUNNING");
//                        getAddress.cancel(true);
//                        getAddress = new GetAddress();
//                        getAddress.execute();
//                    }
//                }
//                isPlaceSearch = false;
            }
        });


    }


    @Override
    public void getLocationUpdate(Double latitude, Double longitude) {
        Log.d(TAG, "getLocationUpdate: called...");
        LatLng currentLocation = new LatLng(latitude, longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 12.0f));
        marker = mMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.address_pointer)));
    }

    private class GetAddress extends AsyncTask<Void, Void, String> {
        LatLng latLng;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "onPreExecute: called.. MARKER LATLNG = " + marker.getPosition());
            latLng = marker.getPosition();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "onPostExecute: SELECTED ADDRESS = " + s);
            mAddress = s;
            latss = String.valueOf(latLng.latitude);
            longs = String.valueOf(latLng.longitude);

            tv_showAddress.setText(s);
        }

        @Override
        protected String doInBackground(Void... voids) {
            String selectedAddress = "";

            try {
                List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                if (addresses != null && addresses.size() > 0) {
                    selectedAddress = addresses.get(0).getAddressLine(0);
                    mCityName = addresses.get(0).getLocality();
                    Log.d(TAG, "doInBackground: City => " + mCityName);

                } else {
                    selectedAddress = "";
                }


            } catch (IOException e) {
                e.printStackTrace();
                if (e.getMessage().equalsIgnoreCase("grpc failed")) {
                    CommonUtil.showSnackBar(AddressPickActivity.this, getString(R.string.enable_gps));
                }


            }
            return selectedAddress;
        }
    }
}