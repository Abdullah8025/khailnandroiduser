package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.AboutUsResponse;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.LoginResponse;
import com.demand.keheilan.responsemodel.PrivacyPolicyResponse;
import com.demand.keheilan.responsemodel.TermsCondtionsResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.ABOUT_US_TAB;
import static com.demand.keheilan.util.MyAppConstants.CLICKED_ON_TAB;
import static com.demand.keheilan.util.MyAppConstants.DEVICE_TYPE;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ARABIC;
import static com.demand.keheilan.util.MyAppConstants.PRIVACY_POLICY_TAB;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TERMS_AND_CONDITIONS_TAB;

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener, CommonUtil.LogoutUser {
    private static final String TAG = "WebViewActivity";
    private ImageView iv_back;
    private TextView tv_title, tv_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        initView();
        Intent data = getIntent();
        if (data != null) {
            Log.d(TAG, "onCreate: CAME_FROM_TAB=> " + data.getStringExtra(CLICKED_ON_TAB));
            if (data.getStringExtra(CLICKED_ON_TAB).equals(ABOUT_US_TAB)) {
                tv_title.setText(getString(R.string.about_ods));
                serviceAboutUs();
            }
            if (data.getStringExtra(CLICKED_ON_TAB).equals(PRIVACY_POLICY_TAB)) {
                tv_title.setText(getString(R.string.privacy_policy));
                servicePrivacyPolicy();
            }
            if (data.getStringExtra(CLICKED_ON_TAB).equals(TERMS_AND_CONDITIONS_TAB)) {
                tv_title.setText(getString(R.string.term_condition));
                serviceTermsConditions();
            }

        }

    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_content = findViewById(R.id.tv_content);

        iv_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        }
    }

    private void servicePrivacyPolicy() {
        {
            if (new InternetCheck(this).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(WebViewActivity.this);
                dialogPopup.showLoadingDialog(WebViewActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PrivacyPolicyResponse> call = api_service.privacyPolicy();
                call.enqueue(new Callback<PrivacyPolicyResponse>() {
                    @Override
                    public void onResponse(Call<PrivacyPolicyResponse> call, Response<PrivacyPolicyResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(WebViewActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(WebViewActivity.this, WebViewActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                PrivacyPolicyResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    if (SharedPreferenceWriter.getInstance(WebViewActivity.this).getString(SPreferenceKey.LANG_CODE).equals(LANGUAGE_CODE_ARABIC))
                                        setStaticContent(server_response.getPolicy_arabic());
                                    else
                                        setStaticContent(server_response.getPolicy());
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(WebViewActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PrivacyPolicyResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(WebViewActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(WebViewActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceAboutUs() {
        {
            if (new InternetCheck(this).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(WebViewActivity.this);
                dialogPopup.showLoadingDialog(WebViewActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<AboutUsResponse> call = api_service.aboutUs();
                call.enqueue(new Callback<AboutUsResponse>() {
                    @Override
                    public void onResponse(Call<AboutUsResponse> call, Response<AboutUsResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(WebViewActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(WebViewActivity.this, WebViewActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                AboutUsResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    if (SharedPreferenceWriter.getInstance(WebViewActivity.this).getString(SPreferenceKey.LANG_CODE).equals(LANGUAGE_CODE_ARABIC))
                                        setStaticContent(server_response.getContent_arabic());
                                    else
                                        setStaticContent(server_response.getContent());
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(WebViewActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AboutUsResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(WebViewActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(WebViewActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceTermsConditions() {
        {
            if (new InternetCheck(this).isConnect()) {
                Log.d(TAG, "serviceLogin: " + SignUpModel.getInstance().toString());
                DialogPopup dialogPopup = new DialogPopup(WebViewActivity.this);
                dialogPopup.showLoadingDialog(WebViewActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<TermsCondtionsResponse> call = api_service.termsConditions();
                call.enqueue(new Callback<TermsCondtionsResponse>() {
                    @Override
                    public void onResponse(Call<TermsCondtionsResponse> call, Response<TermsCondtionsResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(WebViewActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(WebViewActivity.this, WebViewActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                TermsCondtionsResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    if (SharedPreferenceWriter.getInstance(WebViewActivity.this).getString(SPreferenceKey.LANG_CODE).equals(LANGUAGE_CODE_ARABIC))
                                        setStaticContent(server_response.getTerms_arabic());
                                    else
                                        setStaticContent(server_response.getTerms());
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(WebViewActivity.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TermsCondtionsResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(WebViewActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(WebViewActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    private void setStaticContent(String content) {
        tv_content.setText(content);
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called....");
        Intent intent = new Intent(WebViewActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}