package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.demand.keheilan.R;

public class OnBoardingFirst extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "OnBoardingFirst";
    private TextView tv_skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding_first);
        initView();
    }

    private void initView() {
        tv_skip=findViewById(R.id.tv_skip);

        tv_skip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        if (id==R.id.tv_skip)
            startActivity(new Intent(OnBoardingFirst.this,OnBoardingSecond.class));
    }
}