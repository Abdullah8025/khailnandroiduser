package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.service.controls.templates.RangeTemplate;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.R;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.RESULT;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ACCEPTED;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_ID;
import static com.demand.keheilan.util.MyAppConstants.SERVICE_REJECTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_ACCEPTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_REJECTED;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class RateActivity extends AppCompatActivity implements View.OnClickListener,CommonUtil.LogoutUser {
    private static final String TAG = "RateActivity";
    private ImageView iv_back;
    private TextView tv_title;
    private EditText edt_review;
    private RatingBar ratingBar_service;
    private Button btn_submit;

    private String mRating, mServiceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        initView();
        tv_title.setText(getString(R.string.rate_service));
        Intent extraData = getIntent();
        if (extraData != null) {
            mServiceId = extraData.getStringExtra(SERVICE_ID);
            Log.d(TAG, "onCreate: SERVICE_ID = " + mServiceId);
        }
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        edt_review = findViewById(R.id.edt_review);
        ratingBar_service = findViewById(R.id.ratingBar_service);
        btn_submit = findViewById(R.id.btn_submit);

        iv_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.btn_submit) {
            if (validateRate())
                serviceRate(mServiceId);
        }
    }

    private boolean validateRate() {
        mRating = String.valueOf(ratingBar_service.getRating());

        if (!CommonUtil.blankValidation(mServiceId)) {
            CommonUtil.showSnackBar(RateActivity.this, getString(R.string.validate_service_id));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_review.getText().toString())) {
            CommonUtil.showSnackBar(RateActivity.this, getString(R.string.validate_review));
            return false;
        }
        if ((!CommonUtil.blankValidation(mRating)) || (mRating.equalsIgnoreCase("0.0"))) {
            CommonUtil.showSnackBar(RateActivity.this, getString(R.string.validate_rating));
            return false;
        }
        return true;
    }

    private void serviceRate(String serviceId) {
        {
            if (new InternetCheck(RateActivity.this).isConnect()) {
                DialogPopup dialogPopup = new DialogPopup(RateActivity.this);
                dialogPopup.showLoadingDialog(RateActivity.this, "");
                String token = SharedPreferenceWriter.getInstance(RateActivity.this).getString(SPreferenceKey.TOKEN);
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = api_service.rate(TOKEN_KEY + token, serviceId, edt_review.getText().toString().trim(), mRating);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(RateActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(RateActivity.this, RateActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    setResultAndFinish();
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(RateActivity.this, getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    CommonUtil.showSnackBar(RateActivity.this, getString(R.string.on_failure));
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(RateActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(RateActivity.this, getString(R.string.internet_lost));
            }
        }
    }

   private void setResultAndFinish(){
        Intent returnIntent =new Intent();
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called...");
        Intent intent = new Intent(RateActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}