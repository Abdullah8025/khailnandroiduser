package com.demand.keheilan.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.demand.keheilan.BuildConfig;
import com.demand.keheilan.R;
import com.demand.keheilan.adapter.CommonImageAdapter;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.BookedIdsResponse;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.LoginResponse;
import com.demand.keheilan.responsemodel.QueryServicesResponse;
import com.demand.keheilan.responsemodel.ServicesBean;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.FilePath;
import com.demand.keheilan.util.InternetCheck;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.CommonUtil.getPermissionList;
import static com.demand.keheilan.util.MyAppConstants.DEVICE_TYPE;
import static com.demand.keheilan.util.MyAppConstants.GENERAL_INQUIRY;
import static com.demand.keheilan.util.MyAppConstants.GENERAL_INQUIRY_TAB;
import static com.demand.keheilan.util.MyAppConstants.HELP_FRAGMENT_TAG;
import static com.demand.keheilan.util.MyAppConstants.HELP_TAB;
import static com.demand.keheilan.util.MyAppConstants.HELP_WITH_ORDER;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class GeneralInquiry extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, CommonImageAdapter.onImageClick,
        CommonUtil.LogoutUser {
    private static final String TAG = "GeneralInquiry";
    private ImageView iv_back, iv_uploadImage, iv_plus;
    private TextView tv_title, tv_select_heading, tv_select;
    private Button btn_send;
    private RecyclerView rv_queryImages;
    private Spinner spinnerQuery;
    private EditText edt_querySubject, edt_message;

    private boolean isGeneralEnquiry = false;
    private String mServiceId, mOrderId;
    private List<String> mServiceNameList;
    private List<ServicesBean> mServicesList;
    private List<String> mImageList;
    private List<String> mBookingIdsList;
    private static final int STORAGE_REQUEST_CODE = 14;
    private static final int PHOTO_REQ = 4;
    private String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_inquiry);
        initView();
        initialiseList();
        setImageAdapter();
        Intent data = getIntent();
        if (data != null) {
            if (data.hasExtra(HELP_TAB)) {
                Log.d(TAG, "onCreate: HELP_TAB_CLICKED=>" + data.getStringExtra(HELP_TAB));
                if (data.getStringExtra(HELP_TAB).equals(GENERAL_INQUIRY_TAB)) {
                    isGeneralEnquiry = true;
                    tv_title.setText(getString(R.string.general_inquary));
                    tv_select_heading.setText(getString(R.string.query_related_to));
                    tv_select.setHint(getString(R.string.select_query_topic));
                } else {
                    isGeneralEnquiry = false;
                    tv_title.setText(getString(R.string.help_with_order));
                    tv_select_heading.setText(getString(R.string.please_select_the_order));
                    tv_select.setHint(getString(R.string.please_select_the_order));
                }
            }
        }
    }

    private void initialiseList() {
        mServiceNameList = new ArrayList<>();
        mServicesList = new ArrayList<>();
        mImageList = new ArrayList<>();
        mBookingIdsList = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isGeneralEnquiry) {
            serviceAllServices();
        } else {
            serviceBookingIds();
        }
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_select_heading = findViewById(R.id.tv_select_heading);
        tv_select = findViewById(R.id.tv_select);
        btn_send = findViewById(R.id.btn_send);
        rv_queryImages = findViewById(R.id.rv_queryImages);
        spinnerQuery = findViewById(R.id.spinnerQuery);
        iv_uploadImage = findViewById(R.id.iv_uploadImage);
        edt_querySubject = findViewById(R.id.edt_querySubject);
        edt_message = findViewById(R.id.edt_message);
        iv_plus = findViewById(R.id.iv_plus);

        iv_back.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        tv_select.setOnClickListener(this);
        iv_plus.setOnClickListener(this);
        iv_uploadImage.setOnClickListener(this);
        spinnerQuery.setOnItemSelectedListener(this);

        iv_plus.setVisibility(View.GONE);
        iv_uploadImage.setVisibility(View.VISIBLE);

    }

    private void setImageAdapter() {
        rv_queryImages.setLayoutManager(new LinearLayoutManager(GeneralInquiry.this, RecyclerView.HORIZONTAL, false));
        rv_queryImages.setAdapter(new CommonImageAdapter(GeneralInquiry.this, mImageList, this));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.btn_send) {
            if (isGeneralEnquiry) {
                if (validateQuery(GENERAL_INQUIRY))
                    serviceQuery(GENERAL_INQUIRY);
            } else {
                if (validateQuery(HELP_WITH_ORDER))
                    serviceQuery(HELP_WITH_ORDER);
            }

        } else if (id == R.id.tv_select) {
            if (isGeneralEnquiry)
                CommonUtil.setSpinner(GeneralInquiry.this, mServiceNameList, spinnerQuery);
            else
                CommonUtil.setSpinner(GeneralInquiry.this, mBookingIdsList, spinnerQuery);
        } else if (id == R.id.iv_uploadImage) {
            if (checkPermissions(getPermissionList())) {
                if (checkImageLoadSize(mImageList))
                    takePhotoIntent();
            }
        } else if (id == R.id.iv_plus) {
            if (checkPermissions(getPermissionList())) {
                if (checkImageLoadSize(mImageList))
                    takePhotoIntent();
            }
        }
    }

    private boolean checkPermissions(List<String> permissions) {
        List<String> permissionsNotGranted = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(GeneralInquiry.this, permission) != PackageManager.PERMISSION_GRANTED)
                permissionsNotGranted.add(permission);
        }
        if (!permissionsNotGranted.isEmpty()) {
            requestPermissions(permissionsNotGranted.toArray(new String[permissionsNotGranted.size()]), STORAGE_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_REQUEST_CODE: {
                Log.d(TAG, "onRequestPermissionsResult: called.....");
                boolean anyPermissionDenied = false;
                boolean neverAskAgainSelected = false;
                // Check if any permission asked has been denied
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        anyPermissionDenied = true;
                        //check if user select "never ask again" when denying any permission
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(GeneralInquiry.this, permissions[i])) {
                            neverAskAgainSelected = true;
                        }
                    }
                }
                if (!anyPermissionDenied) {
                    Log.d(TAG, "onRequestPermissionsResult: Every permissions granted...");
                    // All Permissions asked were granted! Yey!
                    // DO YOUR STUFF

                    takePhotoIntent();
                } else {
                    // the user has just denied one or all of the permissions
                    // use this message to explain why he needs to grant these permissions in order to proceed
                    if (neverAskAgainSelected) {
                        Log.d(TAG, "onRequestPermissionsResult: never ask again selected...");
                        //This message is displayed after the user has checked never ask again checkbox.
                        shouldShowSettings(iv_uploadImage);

                    } else {
                        Log.d(TAG, "onRequestPermissionsResult: denied selected......");
                        //This message is displayed while the user hasn't checked never ask again checkbox.
                        CommonUtil.showSnackBar(GeneralInquiry.this, "Give Permissions to Upload Image");
                    }
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void shouldShowSettings(View v) {
        Snackbar.make(v, getString(R.string.grant_permission), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.grant_access), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, " snackbar onClick: starts");
                        //user has permanenty denied the permissions so take them to the settings
                        Log.d(TAG, "onClick: user has permanently denied the permission ...");
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        Log.d(TAG, " snackbar onClick: uri is = " + uri.toString());
                        intent.setData(uri);
                        startActivity(intent);
                        Log.d(TAG, " snackbar onClick: ends");
                    }
                }).show();
    }

    private void takePhotoIntent() {

        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File imageFile = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir);
            path = imageFile.getAbsolutePath();
            Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", imageFile);
            Intent intent = new CommonUtil().getPickIntent(getApplicationContext(), imageFileUri);
            startActivityForResult(intent, PHOTO_REQ);
        } catch (IOException e) {
            e.printStackTrace();
        }


//        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
//        File file = new File(capture_dir);
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        path = capture_dir + System.currentTimeMillis() + ".jpg";
//        Uri imageFileUri = FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()), BuildConfig.APPLICATION_ID + ".provider", new File(path));
//        Intent intent = new CommonUtil().getPickIntent(getApplicationContext(), imageFileUri);
//        startActivityForResult(intent, PHOTO_REQ);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PHOTO_REQ:
                    if (data != null) {
                        if (data.getData() != null)
                            path = FilePath.getPath(GeneralInquiry.this, Uri.parse(data.getDataString()));

                    }
                    compressAndSetImage(path);
                    break;
            }
        }
    }


    private void compressAndSetImage(String imagePath) {
        Log.d(TAG, "compressAndSetImage: GIVEN_PATH==>" + imagePath);
//        Bitmap bm = BitmapFactory.decodeFile(imagePath);
//        Bitmap myBitmap = CommonUtil.setImageOrientation(bm, imagePath);
//        String finalpath = compressImage(myBitmap, myBitmap.getWidth(), myBitmap.getHeight());
//        Log.d(TAG, "compressAndSetImage: FINAL_IMAGE_PATH==>" + finalpath);
        mImageList.add(imagePath);
        rv_queryImages.getAdapter().notifyDataSetChanged();
        iv_plus.setVisibility(View.VISIBLE);
        iv_uploadImage.setVisibility(View.GONE);

    }

    private String compressImage(Bitmap finalBitmap, int width, int height) {
        Log.d(TAG, "compressImage: starts.....");
        String capture_dir = Environment.getExternalStorageDirectory() + "/" + getString(R.string.directory_name) + "/Images/";
        File myDir = new File(capture_dir);
        myDir.mkdirs();
        File file = new File(myDir, System.currentTimeMillis() + ".jpg");
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);//	compressing to 50%
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }


    private boolean checkImageLoadSize(List<String> list) {
        if (!(list.size() < 1)) {
            CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.one_image_validation));
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            int parentId = parent.getId();
            if (parentId == R.id.spinnerQuery) {
                if (isGeneralEnquiry) {
                    tv_select.setText(mServiceNameList.get(position));
                    mServiceId = mServicesList.get(position - 1).getId();
                    Log.d(TAG, "onItemSelected: SERVICE NAME = " + mServicesList.get(position - 1).getService_name() + " SERVICE ID = " +
                            mServicesList.get(position - 1).getId());
                } else {
                    tv_select.setText(mBookingIdsList.get(position));
                    mOrderId = mBookingIdsList.get(position);
                }

            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private boolean validateQuery(String type) {
        if (type.equals(GENERAL_INQUIRY)) {
            if (!CommonUtil.blankValidation(mServiceId)) {
                CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.select_query));
                return false;
            }
        }
        if (type.equals(HELP_WITH_ORDER)) {
            if (!CommonUtil.blankValidation(mOrderId)) {
                CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.select_order));
                return false;
            }
        }

        if (!CommonUtil.blankValidation(edt_querySubject.getText().toString())) {
            CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.enter_subject));
            return false;
        }
        if (!CommonUtil.blankValidation(edt_message.getText().toString())) {
            CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.enter_message));
            return false;
        }
        if (mImageList == null || mImageList.size() == 0) {
            CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.upload_image_validation));
            return false;
        }

        return true;
    }

    private void showQuerySentDialog() {
        Dialog querySentDialog = new Dialog(GeneralInquiry.this, android.R.style.Theme_Black_NoTitleBar);
        querySentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        querySentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        querySentDialog.setContentView(R.layout.layout_query_sent);
        querySentDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        querySentDialog.setCanceledOnTouchOutside(true);

        ImageView iv_close = querySentDialog.findViewById(R.id.iv_close);
        TextView bookingIdTitle = querySentDialog.findViewById(R.id.textView30);
        TextView tv_booking_id = querySentDialog.findViewById(R.id.tv_booking_id);

        bookingIdTitle.setVisibility(View.GONE);
        tv_booking_id.setVisibility(View.GONE);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                querySentDialog.dismiss();
                finish();
            }
        });

        querySentDialog.show();
    }

    private void serviceAllServices() {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(GeneralInquiry.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(GeneralInquiry.this);
                dialogPopup.showLoadingDialog(GeneralInquiry.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<QueryServicesResponse> call = api_service.getServices(TOKEN_KEY + token);
                call.enqueue(new Callback<QueryServicesResponse>() {
                    @Override
                    public void onResponse(Call<QueryServicesResponse> call, Response<QueryServicesResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(GeneralInquiry.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(GeneralInquiry.this, GeneralInquiry.this);
                        } else {
                            if (response.isSuccessful()) {
                                QueryServicesResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<ServicesBean> services = (ArrayList<ServicesBean>) server_response.getServices();
                                    if (services != null && services.size() > 0) {
                                        mServicesList.clear();
                                        mServicesList.addAll(services);

                                        mServiceNameList.add("Select Service");
                                        for (ServicesBean servicesBean : services) {
                                            mServiceNameList.add(servicesBean.getService_name());
                                        }
                                    }

                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(GeneralInquiry.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<QueryServicesResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.internet_lost));
            }
        }
    }

    private void serviceBookingIds() {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(GeneralInquiry.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(GeneralInquiry.this);
                dialogPopup.showLoadingDialog(GeneralInquiry.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<BookedIdsResponse> call = api_service.bookedIds(TOKEN_KEY + token);
                call.enqueue(new Callback<BookedIdsResponse>() {
                    @Override
                    public void onResponse(Call<BookedIdsResponse> call, Response<BookedIdsResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(GeneralInquiry.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(GeneralInquiry.this, GeneralInquiry.this);
                        } else {
                            if (response.isSuccessful()) {
                                BookedIdsResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    ArrayList<BookedIdsResponse.BookingIdBean> bookingIds = (ArrayList<BookedIdsResponse.BookingIdBean>) server_response.getData();
                                    if (bookingIds != null && bookingIds.size() > 0) {
                                        mBookingIdsList.clear();
                                        mBookingIdsList.add("Select Order");
                                        for (BookedIdsResponse.BookingIdBean bookingID : bookingIds) {
                                            mBookingIdsList.add(bookingID.getId());
                                        }
                                    }


                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(GeneralInquiry.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<BookedIdsResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.internet_lost));
            }
        }
    }

    private MultipartBody.Part[] getImages(String key, List<String> imgList) throws IOException {
        RequestBody imagesBody = null;
        MultipartBody.Part imgpart[] = null;
        if ((imgList != null) && (imgList.size() > 0)) {
            imgpart = new MultipartBody.Part[imgList.size()];
            for (int i = 0; i < imgList.size(); i++) {
                File imgFile = new File(imgList.get(i));
                imagesBody = RequestBody.create(MediaType.parse("*/*"), imgFile);
                imgpart[i] = MultipartBody.Part.createFormData(key + (i + 1), imgFile.getName(), imagesBody);

            }
        }
        return imgpart;
    }

    private void serviceQuery(String type) {
        {
            if (new InternetCheck(this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(GeneralInquiry.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialogPopup = new DialogPopup(GeneralInquiry.this);
                dialogPopup.showLoadingDialog(GeneralInquiry.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CommonResponse> call = null;
                try {
                    if (type.equals(GENERAL_INQUIRY))
                        call = api_service.generalEnquiry(TOKEN_KEY + token, getGeneralEnquiryParam(), getImages("image_", mImageList));
                    else if (type.equals(HELP_WITH_ORDER)) {
                        call = api_service.helpWithOrder(TOKEN_KEY + token, getHelpParam(), getImages("image_", mImageList));
                    } else
                        throw new IllegalArgumentException(TAG + " query must be of type GENERAL ENQUIRY or HELP ORDER");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialogPopup.dismissLoadingDialog();
                            Toast.makeText(GeneralInquiry.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(GeneralInquiry.this, GeneralInquiry.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialogPopup.dismissLoadingDialog();
                                    showQuerySentDialog();
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialogPopup.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + getString(R.string.on_failure));
                                } else {
                                    dialogPopup.dismissLoadingDialog();
                                    Toast.makeText(GeneralInquiry.this, getString(R.string.on_failure), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialogPopup.dismissLoadingDialog();
                        CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(GeneralInquiry.this, getString(R.string.internet_lost));
            }
        }
    }

    private Map<String, RequestBody> getGeneralEnquiryParam() {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("service", RequestBody.create(MediaType.parse("text/plain"), mServiceId));
        map.put("subject", RequestBody.create(MediaType.parse("text/plain"), edt_querySubject.getText().toString().trim()));
        map.put("message", RequestBody.create(MediaType.parse("text/plain"), edt_message.getText().toString().trim()));
        return map;
    }

    private Map<String, RequestBody> getHelpParam() {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("order", RequestBody.create(MediaType.parse("text/plain"), mOrderId));
        map.put("subject", RequestBody.create(MediaType.parse("text/plain"), edt_querySubject.getText().toString().trim()));
        map.put("message", RequestBody.create(MediaType.parse("text/plain"), edt_message.getText().toString().trim()));
        return map;
    }

    @Override
    public void onRemove(int pos) {
        Log.d(TAG, "onRemove: REMOVE POSITION = " + pos);
        try {
            mImageList.remove(pos);
            rv_queryImages.getAdapter().notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "onRemove: " + e.getMessage(), e);
        }

        if (mImageList.size() == 0) {
            iv_uploadImage.setVisibility(View.VISIBLE);
            iv_plus.setVisibility(View.GONE);
        } else {
            iv_uploadImage.setVisibility(View.GONE);
            iv_plus.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onShowLargeImage(ImageView imageView, String path) {

    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called..");
        Intent intent = new Intent(GeneralInquiry.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}