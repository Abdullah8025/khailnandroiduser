package com.demand.keheilan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.demand.keheilan.R;
import com.demand.keheilan.model.ChangeLanguageModel;
import com.demand.keheilan.model.SignUpModel;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.ProfilePicResponse;
import com.demand.keheilan.retrofit.ApiInterface;
import com.demand.keheilan.retrofit.RetrofitInit;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.demand.keheilan.util.CommonUtil;
import com.demand.keheilan.util.DialogPopup;
import com.demand.keheilan.util.InternetCheck;
import com.demand.keheilan.util.LocaleHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.demand.keheilan.util.MyAppConstants.CAME_FROM_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ARABIC;
import static com.demand.keheilan.util.MyAppConstants.LANGUAGE_CODE_ENGLISH;
import static com.demand.keheilan.util.MyAppConstants.MORE_SCREEN;
import static com.demand.keheilan.util.MyAppConstants.ON_BOARDING_THIRD;
import static com.demand.keheilan.util.MyAppConstants.STATUS_FAILURE;
import static com.demand.keheilan.util.MyAppConstants.STATUS_SUCCESS;
import static com.demand.keheilan.util.MyAppConstants.STATUS_UNAUTHORIZED;
import static com.demand.keheilan.util.MyAppConstants.TOKEN_KEY;

public class ChooseLanguageActivity extends AppCompatActivity implements View.OnClickListener, CommonUtil.LogoutUser {
    private static final String TAG = "ChooseLanguageActivity";
    private Button btn_continue;
    private LinearLayout header;
    private ConstraintLayout const_tab, const_englishLang, const_arabicLang;
    private ImageView iv_back;
    private TextView tv_title, tv_dummyTextEng, tv_engLang, tv_dummyTextArabic, tv_ArabicLang;
    private boolean isFinishEnabled = false;
    private String mSelectedLangCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);
        initView();
        Intent data = getIntent();
        if (data != null) {
            Log.d(TAG, "onCreate: CAME_FROM_SCREEN=> " + data.getStringExtra(CAME_FROM_SCREEN));
            if (data.hasExtra(CAME_FROM_SCREEN)) {
                if (data.getStringExtra(CAME_FROM_SCREEN).equals(ON_BOARDING_THIRD)) {
                    header.setVisibility(View.GONE);
                    const_tab.setVisibility(View.VISIBLE);
                    btn_continue.setText(getString(R.string.btn_continue));
                    isFinishEnabled = false;
                    mSelectedLangCode = LANGUAGE_CODE_ENGLISH;
                }
                if (data.getStringExtra(CAME_FROM_SCREEN).equals(MORE_SCREEN)) {
                    header.setVisibility(View.VISIBLE);
                    tv_title.setText(getString(R.string.change_language));
                    const_tab.setVisibility(View.GONE);
                    btn_continue.setText(getString(R.string.change));
                    isFinishEnabled = true;
                }

            } else {
                isFinishEnabled = false;
            }
        } else {
            isFinishEnabled = false;
        }
    }

    private void initView() {
        btn_continue = findViewById(R.id.btn_continue);
        header = findViewById(R.id.header);
        const_tab = findViewById(R.id.const_tab);
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        const_englishLang = findViewById(R.id.const_englishLang);
        const_arabicLang = findViewById(R.id.const_arabicLang);
        tv_dummyTextEng = findViewById(R.id.tv_dummyTextEng);
        tv_engLang = findViewById(R.id.tv_engLang);
        tv_dummyTextArabic = findViewById(R.id.tv_dummyTextArabic);
        tv_ArabicLang = findViewById(R.id.tv_ArabicLang);

        btn_continue.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_title.setOnClickListener(this);
        const_englishLang.setOnClickListener(this);
        const_arabicLang.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSelectedLangCode = LANGUAGE_CODE_ENGLISH;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_continue) {
            if (isFinishEnabled) {
                serviceUpdateLanguage(mSelectedLangCode);
            } else {
                CommonUtil.hideKeyBoard(ChooseLanguageActivity.this, btn_continue);
                if (validateLanguage()) {
                    saveLangCode(mSelectedLangCode);
                    setLanguage();
                    SignUpModel.getInstance().setLangCode(mSelectedLangCode);
                    startActivity(new Intent(ChooseLanguageActivity.this, LoginPhoneActivity.class));
                }
            }
        } else if (id == R.id.iv_back) {
            finish();
        } else if (id == R.id.const_englishLang) {
            mSelectedLangCode = LANGUAGE_CODE_ENGLISH;
            setTabBackground(true, false);
        } else if (id == R.id.const_arabicLang) {
            mSelectedLangCode = LANGUAGE_CODE_ARABIC;
            setTabBackground(false, true);
        }

    }

    private void setLanguage() {
        if (SharedPreferenceWriter.getInstance(ChooseLanguageActivity.this).getString(SPreferenceKey.LANG_CODE).equals(LANGUAGE_CODE_ARABIC))
            LocaleHelper.setLocale(ChooseLanguageActivity.this, LANGUAGE_CODE_ARABIC);
        else
            LocaleHelper.setLocale(ChooseLanguageActivity.this, LANGUAGE_CODE_ENGLISH);

    }

    private void saveLangCode(String langCode) {
        SharedPreferenceWriter.getInstance(ChooseLanguageActivity.this).writeStringValue(SPreferenceKey.LANG_CODE, langCode);
    }

    private boolean validateLanguage() {
        if (mSelectedLangCode == null || mSelectedLangCode.length() == 0) {
            CommonUtil.showSnackBar(ChooseLanguageActivity.this, getString(R.string.validate_language));
            return false;
        }
        return true;
    }

    private void setTabBackground(boolean isEnglishTabSelected, boolean isArabicTabSelected) {
        if (isEnglishTabSelected) {
            const_englishLang.setBackground(ContextCompat.getDrawable(ChooseLanguageActivity.this, R.drawable.bg_yellow_corner_8));
            tv_dummyTextEng.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.white));
            tv_engLang.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.white));
        } else {
            const_englishLang.setBackground(ContextCompat.getDrawable(ChooseLanguageActivity.this, R.drawable.bg_white_corner_8));
            tv_dummyTextEng.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.color_light_grey));
            tv_engLang.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.color_light_grey));
        }

        if (isArabicTabSelected) {
            const_arabicLang.setBackground(ContextCompat.getDrawable(ChooseLanguageActivity.this, R.drawable.bg_yellow_corner_8));
            tv_dummyTextArabic.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.white));
            tv_ArabicLang.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.white));
        } else {
            const_arabicLang.setBackground(ContextCompat.getDrawable(ChooseLanguageActivity.this, R.drawable.bg_white_corner_8));
            tv_dummyTextArabic.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.color_light_grey));
            tv_ArabicLang.setTextColor(ContextCompat.getColor(ChooseLanguageActivity.this, R.color.color_light_grey));
        }
    }

    private void changeLanguage(String langCode) {
        Log.d(TAG, "changeLanguage: LANG_CODE=>" + langCode);
        LocaleHelper.setLocale(ChooseLanguageActivity.this, langCode);
        SharedPreferenceWriter.getInstance(ChooseLanguageActivity.this).writeStringValue(SPreferenceKey.LANG_CODE, langCode);
        CommonUtil.startActivity(ChooseLanguageActivity.this, HomeActivity.class);
    }

    private void serviceUpdateLanguage(String selectedLang) {
        {
            if (new InternetCheck(ChooseLanguageActivity.this).isConnect()) {
                String token = SharedPreferenceWriter.getInstance(ChooseLanguageActivity.this).getString(SPreferenceKey.TOKEN);
                DialogPopup dialog = new DialogPopup(ChooseLanguageActivity.this);
                dialog.showLoadingDialog(ChooseLanguageActivity.this, "");
                ApiInterface api_service = RetrofitInit.getConnect().createConnection();

                ChangeLanguageModel changeLanguageModel = new ChangeLanguageModel(selectedLang);

                Call<CommonResponse> call = api_service.updateLanguage(TOKEN_KEY + token, changeLanguageModel);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.code() == STATUS_UNAUTHORIZED) {
                            dialog.dismissLoadingDialog();
                            Toast.makeText(ChooseLanguageActivity.this, getString(R.string.session_expired), Toast.LENGTH_SHORT).show();
                            CommonUtil.clearSavedData(ChooseLanguageActivity.this, ChooseLanguageActivity.this);
                        } else {
                            if (response.isSuccessful()) {
                                CommonResponse server_response = response.body();
                                if (server_response.getStatus() == STATUS_SUCCESS) {
                                    dialog.dismissLoadingDialog();
                                    Toast.makeText(ChooseLanguageActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                                    changeLanguage(selectedLang);
                                    finish();
                                } else if (server_response.getStatus() == STATUS_FAILURE) {
                                    dialog.dismissLoadingDialog();
                                    Log.d(TAG, "onResponse: " + response.message());
                                } else {
                                    dialog.dismissLoadingDialog();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        CommonUtil.showSnackBar(ChooseLanguageActivity.this, getString(R.string.on_failure));
                        Log.e("tag", t.getMessage(), t);
                    }
                });
            } else {
                CommonUtil.showSnackBar(ChooseLanguageActivity.this, getString(R.string.internet_lost));
            }
        }
    }

    @Override
    public void logoutAndFinish() {
        Log.d(TAG, "logoutAndFinish: called...");
        Intent intent = new Intent(ChooseLanguageActivity.this, LoginPhoneActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}