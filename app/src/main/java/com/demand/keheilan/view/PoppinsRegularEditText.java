package com.demand.keheilan.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

/**
 * Created by mahesh on 27/1/17.
 * SanFranciscoDisplay-Regular font TextView
 */

public class PoppinsRegularEditText extends AppCompatEditText {
    public PoppinsRegularEditText(Context context) {
        super(context);
        createFont();
    }

    public PoppinsRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        createFont();
    }

    public PoppinsRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createFont();
    }

    public void createFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/Poppins_Regular.ttf");
        setTypeface(font);
    }

}
