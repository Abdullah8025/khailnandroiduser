package com.demand.keheilan.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public class PoppinsSemiBoldTextView extends AppCompatTextView {
    public PoppinsSemiBoldTextView(Context context) {
        super(context);
        createFont();
    }

    public PoppinsSemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createFont();
    }

    public PoppinsSemiBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createFont();
    }

    public void createFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "font/Poppins_SemiBold.ttf");
        setTypeface(font);
    }

}
