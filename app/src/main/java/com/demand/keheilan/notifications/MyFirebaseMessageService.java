package com.demand.keheilan.notifications;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.demand.keheilan.activity.HomeActivity;
import com.demand.keheilan.sharedpreference.SPreferenceKey;
import com.demand.keheilan.sharedpreference.SharedPreferenceWriter;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.demand.keheilan.util.App.PUSH_NOTIFICATION;
import static com.demand.keheilan.util.MyAppConstants.ACTION_NOTIFICATION_TAP;
import static com.demand.keheilan.util.MyAppConstants.FIREBASE_MESSAGE;
import static com.demand.keheilan.util.MyAppConstants.FIREBASE_MESSAGING_SERVICE;
import static com.demand.keheilan.util.MyAppConstants.IN_PROCESS_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.OPEN_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.PAST_SERVICES;
import static com.demand.keheilan.util.MyAppConstants.UPCOMING_SERVICES;

public class MyFirebaseMessageService extends FirebaseMessagingService {
    String TAG = MyFirebaseMessageService.class.getSimpleName();
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived: message recieved from=> " + remoteMessage.getFrom());
        Log.e(TAG, "MESSAGE_RECEIVED:" + remoteMessage.getData().toString());
        String title = null, body = null, type = null;
        if (remoteMessage.getData().size() > 0) {
            title = remoteMessage.getData().get("title");
            body = remoteMessage.getData().get("body");
            type = remoteMessage.getData().get("type");

            Intent homeActivityIntent = new Intent(getApplicationContext(), HomeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(FIREBASE_MESSAGING_SERVICE, FIREBASE_MESSAGE);
            bundle.putString(OPEN_SERVICES, notiType(type));
            homeActivityIntent.putExtras(bundle);
            showNotificationMessage(getApplicationContext(), title, body, "", homeActivityIntent);
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

        }else {
            Log.d(TAG, "onMessageReceived: DATA NULL....");
        }

//        Map<String, String> data = remoteMessage.getData();
//        JSONObject jsonObject = new JSONObject(data);
//
//        try {
//            title = jsonObject.getString("title");
//            body = jsonObject.getString("message");
//            type = jsonObject.getString("type");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

       

//        notiType(type);
//
//        if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//            Intent pushNotification = new Intent(PUSH_NOTIFICATION);
//            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//            Intent homeActivityIntent = new Intent(getApplicationContext(), HomeActivity.class);
//            showNotificationMessage(getApplicationContext(), title, body, "", homeActivityIntent);
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();
//        } else {
//            Intent homeActivityIntent = new Intent(getApplicationContext(), HomeActivity.class);
//            showNotificationMessage(getApplicationContext(), title, body, "", homeActivityIntent);
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();
//        }


    }

    private String notiType(String type) {
        if (type.equalsIgnoreCase(UPCOMING_SERVICES)) {
            return UPCOMING_SERVICES;
//            Bundle bundle = new Bundle();
//            bundle.putString(OPEN_SERVICES, UPCOMING_SERVICES);
//            Intent intent = new Intent(ACTION_NOTIFICATION_TAP);
//            intent.putExtras(bundle);
//            sendBroadcast(intent);
        } else if (type.equalsIgnoreCase(IN_PROCESS_SERVICES)) {
            return IN_PROCESS_SERVICES;
//            Bundle bundle = new Bundle();
//            bundle.putString(OPEN_SERVICES, IN_PROCESS_SERVICES);
//            Intent intent = new Intent(ACTION_NOTIFICATION_TAP);
//            intent.putExtras(bundle);
//            sendBroadcast(intent);

        } else if (type.equalsIgnoreCase(PAST_SERVICES)) {
            return PAST_SERVICES;
//            Bundle bundle = new Bundle();
//            bundle.putString(OPEN_SERVICES, PAST_SERVICES);
//            Intent intent = new Intent(ACTION_NOTIFICATION_TAP);
//            intent.putExtras(bundle);
//            sendBroadcast(intent);

        }
        return IN_PROCESS_SERVICES;
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
        Log.d(TAG, "onDeletedMessages: called..");
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.d(TAG, "onNewToken: called..NEW_DEVICE_TOKEN=> " + s);
        SharedPreferenceWriter.getInstance(this).writeStringValue(SPreferenceKey.DEVICE_TOKEN, s);
    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }
}
