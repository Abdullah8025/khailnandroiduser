package com.demand.keheilan.retrofit;

import com.demand.keheilan.model.ChangeLanguageModel;
import com.demand.keheilan.model.UpdateStatusModel;
import com.demand.keheilan.responsemodel.AboutUsResponse;
import com.demand.keheilan.responsemodel.BookedIdsResponse;
import com.demand.keheilan.responsemodel.BookedServicesResponse;
import com.demand.keheilan.responsemodel.BookingDetailResponse;
import com.demand.keheilan.responsemodel.CoupenListResponse;
import com.demand.keheilan.responsemodel.CustomerTaxResponse;
import com.demand.keheilan.responsemodel.GuestLoginResponse;
import com.demand.keheilan.responsemodel.NotificationListResponse;
import com.demand.keheilan.responsemodel.PastServicesResponse;
import com.demand.keheilan.responsemodel.PrivacyPolicyResponse;
import com.demand.keheilan.responsemodel.QueryServicesResponse;
import com.demand.keheilan.responsemodel.ReviewsResponse;
import com.demand.keheilan.responsemodel.SearchServiceResponse;
import com.demand.keheilan.responsemodel.CheckUserExistResponse;
import com.demand.keheilan.responsemodel.CommonResponse;
import com.demand.keheilan.responsemodel.HomeApiResponse;
import com.demand.keheilan.responsemodel.LoginResponse;
import com.demand.keheilan.responsemodel.ProfilePicResponse;
import com.demand.keheilan.responsemodel.RecentSearchResponse;
import com.demand.keheilan.responsemodel.ServiceBookingResponse;
import com.demand.keheilan.responsemodel.ServiceDetailResponse;
import com.demand.keheilan.responsemodel.ServicesBean;
import com.demand.keheilan.responsemodel.SignUpResponse;
import com.demand.keheilan.responsemodel.SubCategoryResponse;
import com.demand.keheilan.responsemodel.SubCategoryServices;
import com.demand.keheilan.responsemodel.TermsCondtionsResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/check-user-exists/")
    Call<CheckUserExistResponse> checkPhoneNumber(@Field("country_code") String countryCode,
                                                  @Field("phone_number") String phoneNumber);

    @FormUrlEncoded
    @POST("api/user-login/")
    Call<LoginResponse> login(@Field("phone_number") String phoneNumber,
                              @Field("device_token") String deviceToken,
                              @Field("country_code") String countryCode,
                              @Field("language") String language,
                              @Field("device_type") String deviceType,
                              @Field("lang") String longitude,
                              @Field("lat") String latitude);

    @FormUrlEncoded
    @POST("api/user-create/")
    Call<SignUpResponse> signUp(@Field("full_name") String fullName,
                                @Field("country_code") String countryCode,
                                @Field("phone_number") String phoneNumber,
                                @Field("referral_code") String referralCode,
                                @Field("device_token") String deviceToken,
                                @Field("language") String language,
                                @Field("device_type") String deviceType,
                                @Field("lat") String latitude,
                                @Field("lang") String longitude);

    @Multipart
    @PATCH("api/update-user-profile-pic/")
    Call<CommonResponse> updateProfilePic(@Header("Authorization") String token,
                                          @Part MultipartBody.Part profilePic);

    @GET("api/get-user-profile-pic/")
    Call<ProfilePicResponse> getProfilePic(@Header("Authorization") String token);

    @GET("api/home-api/")
    Call<HomeApiResponse> homeList(@Header("Authorization") String token);

    @GET("api/get-service-detail/")
    Call<ServiceDetailResponse> serviceDetail(@Header("Authorization") String token,
                                              @Query("id") String serviceId);

    @GET("api/get-saved-searches/")
    Call<RecentSearchResponse> recentSearches(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api/save-search/")
    Call<CommonResponse> saveSearch(@Header("Authorization") String token,
                                    @Field("searched_value") String searchedValue);

    @GET("api/search-services/")
    Call<SearchServiceResponse> searchService(@Header("Authorization") String token,
                                              @Query("search") String search);

    @GET("api/get-subcategories/")
    Call<SubCategoryResponse> subCategoryList(@Header("Authorization") String token,
                                              @Query("category") String categoryId);

    @GET("api/get-services/")
    Call<SubCategoryServices> getServices(@Header("Authorization") String token,
                                          @Query("sub_category") String subCategoryId);

    @GET("api/privacy-policy/")
    Call<PrivacyPolicyResponse> privacyPolicy();

    @GET("api/terms-and-condition/")
    Call<TermsCondtionsResponse> termsConditions();

    @GET("api/about-us/")
    Call<AboutUsResponse> aboutUs();

    @GET("api/services/")
    Call<QueryServicesResponse> getServices(@Header("Authorization") String token);

    @Multipart
    @POST("api/general-inquiry/")
    Call<CommonResponse> generalEnquiry(@Header("Authorization") String token,
                                        @PartMap Map<String, RequestBody> part,
                                        @Part MultipartBody.Part enquiryImages[]);

    @Multipart
    @POST("api/help-with-order/")
    Call<CommonResponse> helpWithOrder(@Header("Authorization") String token,
                                       @PartMap Map<String, RequestBody> part,
                                       @Part MultipartBody.Part orderImages[]);

    @Multipart
    @POST("api/provider-register-from-user-app/")
    Call<CommonResponse> registerProvider(@Header("Authorization") String token,
                                          @PartMap Map<String, RequestBody> part,
                                          @Part MultipartBody.Part providerPic);

    @GET("api/get-user-notification-list/")
    Call<NotificationListResponse> notifications(@Header("Authorization") String token);

    @Multipart
    @POST("api/book-service/")
    Call<ServiceBookingResponse> bookService(@Header("Authorization") String token,
                                             @PartMap Map<String, RequestBody> partmap,
                                             @Part MultipartBody.Part serviceImage[]);

    @GET("api/upcoming-bookings/")
    Call<BookedServicesResponse> upcomingBookings(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api/booking-detail/")
    Call<BookingDetailResponse> bookingDetail(@Header("Authorization") String token,
                                              @Field("id") String bookingId);

    @FormUrlEncoded
    @POST("api/update-order/")
    Call<CommonResponse> updateServiceStatus(@Header("Authorization") String token,
                                             @Field("id") String bookingId,
                                             @Field("status") String status);

    @GET("api/ongoing-bookings/")
    Call<BookedServicesResponse> inProcessServices(@Header("Authorization") String token);

    @GET("api/past-bookings/")
    Call<PastServicesResponse> pastServices(@Header("Authorization") String token);

    @GET("api/get-users-bookings-ids/")
    Call<BookedIdsResponse> bookedIds(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api/ratings-and-review/")
    Call<CommonResponse> rate(@Header("Authorization") String token,
                              @Field("id") String serviceId,
                              @Field("review") String review,
                              @Field("rating") String rating);

    @GET("api/service-ratings-and-review/")
    Call<ReviewsResponse> reviews(@Header("Authorization") String token,
                                  @Query("service_id") String serviceId);

    @GET("api/coupon-list/")
    Call<CoupenListResponse> coupons(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("api/apply-coupon/")
    Call<CommonResponse> applyCoupon(@Header("Authorization") String token,
                                     @Field("coupon") String couponId,
                                     @Field("order") String bookingId);

    @FormUrlEncoded
    @POST("api/remove-coupon/")
    Call<CommonResponse> removeCoupon(@Header("Authorization") String token,
                                      @Field("order") String bookingId);

    @GET("api/guest-user/")
    Call<GuestLoginResponse> guestLogin();

    @PATCH("api/update-user-language/")
    Call<CommonResponse> updateLanguage(@Header("Authorization") String token,
                                        @Body ChangeLanguageModel changeLanguageModel);

    @FormUrlEncoded
    @POST("api/check-number-of-bookings/")
    Call<CommonResponse> isSlotAvailable(@Header("Authorization") String token,
                                         @Field("date") String bookingData,
                                         @Field("time") String time);

    @GET("api/get-gst-value/")
    Call<CustomerTaxResponse> getCustomerTax();


}
