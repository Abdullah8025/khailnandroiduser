package com.demand.keheilan.sharedpreference;


public class SPreferenceKey {


    public static final String LATITUDE ="latitude" ;
    public static final String LONGITUDE ="longitude" ;
    public static final String PHONE_NUMBER="phone_number";
    public static final String COUNTRY_CODE="country_code";
    public static final String TOKEN="token";
    public static final String USER_ID="id";
    public static final String FULL_NAME="full_name";
    public static final String PROFILE_PIC="profile_pic";
    public static final String DEVICE_TYPE="device_type";
    public static final String DEVICE_TOKEN="device_token";
    public static final String IS_USER_LOGIN="is_user_login";
    public static final String USER_HOME_ADDRESS="user_home_address";
    public static final String IS_GUEST_USER="guest_user";
    public static final String LANG_CODE="lang_code";


}